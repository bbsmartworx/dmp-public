#! /usr/bin/env python

"""
This script uses the public API to read the latest settings (both Reported and Desired),
for one or multiple sections,
from multiple devices.

February 2023
"""
import os
from json import dumps
from argparse import ArgumentParser
from os.path import splitext, basename
from logging import getLogger
from sys import exit
from dmp.ApiConsumer import ApiConsumer
from utils.get import (
    get_id,
    get_data, 
    get_sections, 
    get_mac_address, 
    get_name,
    get_in_sync,
    get_desired_conf, 
    get_reported_conf,     
    search
)
from utils.logging_conf import set_log_lvl, configure_logging
from utils.services import get_my_devices, log_response, find_fw_ids


def parse_args():
    """Parse command-line arguments"""
    parser = ArgumentParser(
        description="Read settings for one section from all devices"
    )

    # Positional arguments:

    parser.add_argument("devices", help="CSV file of devices", type=str, default="ALL")

    parser.add_argument("section", help="Section name", type=str, default="snmp")

    # Optional arguments:

    parser.add_argument(
        "-host",
        help="URL of the API gateway. \
                Default = 'https://gateway.wadmp.com'",
        type=str,
        default="https://gateway.wadmp.com",
    )

    parser.add_argument(
        "-username",
        help="Username. \
                Check the code for the default!",
        type=str,
        default="email address",
    )

    parser.add_argument(
        "-password",
        help="Password. \
                Check the code for the default!",
        type=str,
        default="password",
    )

    parser.add_argument(
        "-console_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    parser.add_argument(
        "-file_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    args = parser.parse_args()

    return args


def main(args):
    api = ApiConsumer(args.host)
    api.login(args.username, args.password)
    
    console_log_lvl    = set_log_lvl(args.console_log_level)
    file_log_lvl       = set_log_lvl(args.file_log_level)
    script_name        = splitext(basename(__file__))[0]
    configure_logging(script_name, console_log_lvl, file_log_lvl)
    
    logger = getLogger(script_name)

    my_devices = get_my_devices(api, logger, args.devices)
    get_report(api, logger, my_devices)

def make_get_settings_dir(logger):
    try:
        os.mkdir("get_settings")
        logger.debug("Created the 'get_settings' directory.")
    except FileExistsError:
        logger.debug("The 'get_settings' directory already exists")

def make_device_folder(logger, device):
    # Windows doesn't allow colons (':') in filenames
    device_folder = os.path.join(
        "get_settings", get_mac_address(device).replace(":", "-")
    )
    try:
        os.mkdir(device_folder)
    except FileExistsError:
        logger.error(f"The sub-directory {device_folder} already exists!")
        logger.error(
            "Please delete or move the contents of the 'get_settings' folder before trying again"
        )
        exit(1)
    return device_folder

def get_all_app_sections(api, logger, fw_app_id, fw_app_version_id):
    response = api.application.get_app_version(fw_app_id, fw_app_version_id)
    log_response(logger, response, message="Failed to get firmware version details!")
    all_sections = get_sections(response)
    if not all_sections:
        logger.error("No application sections found!") 
    return all_sections

def get_all_settings(api, logger, device, fw_app_version_id, all_sections):
    for section in all_sections:
        logger.info(f"Section {get_name(section)}")

        response = api.application.get_section(get_mac_address(device), fw_app_version_id, get_id(section))
        log_response(logger, response, message=f"Failed to get app section details! For device: {get_mac_address(device)}.")
        settings = get_data(response)
        logger.info(dumps(settings, indent=4, sort_keys=True) + "\n")
    return settings

def write_settings_to_file(logger, settings, device_folder, section):
    logger.info(dumps(settings, indent=4, sort_keys=True) + "\n")
    desired_file = os.path.join(
        device_folder, f"{get_name(section)}_desired.ini"
    )
    reported_file = os.path.join(
        device_folder, f"{get_name(section)}_reported.ini"
    )
    if settings[
        "desired_configuration"
    ]:  # Can't write 'null' or 'None':
        with open(desired_file, "x") as f:
            f.write(get_desired_conf(settings))
    if settings[
        "reported_configuration"
    ]:  # Can't write 'null' or 'None':
        with open(reported_file, "x") as f:
            f.write(get_reported_conf(settings))               

def report_in_numbers(logger, device_count, device_with_specified_section_count, reported_count, desired_count, in_sync_count):
    if args.section != "ALL":
        logger.info(
            f"{device_count} devices in total, of which {device_with_specified_section_count} have the {args.section} section"
        )
        logger.info(
            f"Of those {device_with_specified_section_count}:\n"
            f"    {reported_count} have a reported state,\n"
            f"    {desired_count} have a desired state,\n"
            f"    {in_sync_count} are in sync.\n"
        )

def save_settings_from_specified_section_if_exist(
        api,
        logger,
        section, device, 
        fw_app_version_id, 
        device_folder,
        device_with_specified_section_count, 
        in_sync_count, 
        desired_count, 
        reported_count
    ):
    if section:
        device_with_specified_section_count += 1
        response = api.device.get_section(get_mac_address(device), fw_app_version_id, get_id(section))
        log_response(logger, response, message=f"Failed to get app section details! For device: {get_mac_address(device)}.")
        if settings := get_data(response):
            write_settings_to_file(logger, settings, device_folder, section) 
            if get_in_sync(settings): in_sync_count += 1
            if get_desired_conf(settings): desired_count += 1
            if get_reported_conf(settings): reported_count += 1
    else:
        logger.warning(
            f"This device does not have a section called {args.section}"
        )      

def get_report(api, logger, my_devices):
    device_count    = 0
    device_with_specified_section_count = 0
    in_sync_count   = 0
    desired_count   = 0
    reported_count  = 0    

    for device in my_devices:
        device_count += 1
        logger.info(f"Device {get_mac_address(device)}")
        fw_app_id, fw_app_version_id = find_fw_ids(api, logger, get_mac_address(device))
        if fw_app_id and fw_app_version_id:
            logger.info(
                f"Firmware application ID {fw_app_id}, application version ID {fw_app_version_id}"
            )
            make_get_settings_dir(logger)
            device_folder = make_device_folder(logger, device)
            all_sections = get_all_app_sections(api, logger, fw_app_id, fw_app_version_id)
            if args.section == "ALL":
                for section in all_sections:
                    settings = get_all_settings(api, logger, device, fw_app_version_id, all_sections)
                    if settings:
                        write_settings_to_file(logger, settings, device_folder, section)
            else:  # Only one section was specified on the command line
                section = search(all_sections, args.section)
                save_settings_from_specified_section_if_exist(
                    api,
                    logger, 
                    section,
                    device, 
                    fw_app_version_id, 
                    device_folder, 
                    device_with_specified_section_count, 
                    in_sync_count, 
                    desired_count, 
                    reported_count
                )
        else:
            logger.error("No firmware application found!")
    report_in_numbers(
        logger, 
        device_count, 
        device_with_specified_section_count, 
        reported_count, 
        desired_count, 
        in_sync_count
    )


if __name__ == "__main__":
    args = parse_args()
    main(args)
