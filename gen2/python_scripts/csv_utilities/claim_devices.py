#!/usr/bin/env python

"""
Python script which uses the WADMP public API to claim a number of devices to a company.

February 2023
"""

from argparse import ArgumentParser
from os.path import splitext, basename
from csv import reader
from sys import exit
from logging import getLogger
from dmp.ApiConsumer import ApiConsumer
from utils.get import get_id_by_name
from utils.logging_conf import set_log_lvl, configure_logging
from utils.services import log_response


def parse_args():
    """Parse command-line arguments
    """
    parser = ArgumentParser(description="Claim devices to a company on WA/DMP")

    # Positional arguments:

    parser.add_argument("CSVfile", help="Path to CSV file.", type=str)

    parser.add_argument(
        "Company",
        help="Company name. \
                Check the code for the default!",
        type=str,
        default="Company X",
    )

    # Optional arguments:

    parser.add_argument(
        "-host",
        help="URL of the API gateway. \
                Default = 'https://gateway.wadmp.com'",
        type=str,
        default="https://gateway.wadmp.com",
    )

    parser.add_argument(
        "-username",
        help="Username. \
                Check the code for the default!",
        type=str,
        default="email",
    )

    parser.add_argument(
        "-password",
        help="Password. \
                Check the code for the default!",
        type=str,
        default="password",
    )

    parser.add_argument(
        "-console_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    parser.add_argument(
        "-file_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    return parser.parse_args()


def main(args):
    api = ApiConsumer(args.host)
    api.login(args.username, args.password)
    
    console_log_lvl    = set_log_lvl(args.console_log_level)
    file_log_lvl       = set_log_lvl(args.file_log_level)
    script_name        = splitext(basename(__file__))[0]
    configure_logging(script_name, console_log_lvl, file_log_lvl)
    
    logger = getLogger(script_name) 
    company_id = wanted_company_id(api, logger, args)
    claim_wanted_devices(api, logger, args, company_id)

def wanted_company_id(api, logger, args):
    company_id = None
    response = api.company.get_companies()
    log_response(logger, response, message="Failed to retrieve the list of Companies!")
    company_id = get_id_by_name(response, args.Company)

    if company_id:
        logger.info(f"Company {args.Company} has ID {company_id}")
    else:
        logger.error(f"Company {args.Company} not found!")
        exit()
    return company_id

def claim_wanted_devices(api, logger, args, company_id):
    with open(args.CSVfile, encoding="UTF-8", newline="") as csvfile:
        csvreader = reader(csvfile, delimiter=",")
        next(csvreader)  # Skip the first row
        for row in csvreader:
            logger.debug(row)

            alias, serial_number, order_code, mac, imei, requested_type = row
            logger.info(f"Alias {alias}")
            logger.info(f"Serial Number {serial_number}")
            logger.info(f"Order Code {order_code}")
            logger.info(f"MAC {mac}")
            logger.info(f"IMEI {imei}")
            logger.info(f"Type {requested_type}\n")

            device = {
                "alias":            alias,
                "serial_number":    serial_number,
                "mac_address":      mac,
                "imei":             imei,
                "company_id":       company_id,
            }
            response = api.device.claim(device) 
            log_response(logger, response, message=f"Failed to claim device {mac} !")

if __name__ == "__main__":
    args = parse_args()
    main(args)
