"""
This script uses the public API to read the applications (both Firmware and User Modules),
from multiple devices.

February 2023
"""

from argparse import ArgumentParser
from os.path import splitext, basename
from logging import getLogger
import pandas as pd
from dmp.ApiConsumer import ApiConsumer
from utils.get import (
    get_data, 
    has_failed, 
    get_app_name_from_app_version, 
    get_version_from_app_version, 
    is_unknown_from_app_version, 
    is_pinned, 
    is_unknown_from_app_version, 
    get_alias, 
    get_serial_number, 
    get_order_code, 
    get_mac_address, 
    get_imei, 
    get_device_type_description
)
from utils.logging_conf import set_log_lvl, configure_logging
from utils.services import get_my_devices, log_response


def parse_args():
    """Parse command-line arguments"""
    parser = ArgumentParser(description="Read applications from all devices")

    # Positional arguments:

    parser.add_argument("devices", help="CSV file of devices", type=str, default="ALL")

    # Optional arguments:

    parser.add_argument(
        "-csv_file",
        help="Name of saved file. \
                Default = 'get_apps.csv'",
        type=str,
        default="get_apps.csv",
    )

    parser.add_argument(
        "-host",
        help="URL of the API gateway. \
                Default = 'https://gateway.wadmp.com'",
        type=str,
        default="https://gateway.wadmp.com",
    )

    parser.add_argument(
        "-username",
        help="Username. \
                Check the code for the default!",
        type=str,
        default="email address",
    )

    parser.add_argument(
        "-password",
        help="Password. \
                Check the code for the default!",
        type=str,
        default="password",
    )

    parser.add_argument(
        "-console_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    parser.add_argument(
        "-file_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    args = parser.parse_args()

    return args

def main(args):
    api = ApiConsumer(args.host)
    api.login(args.username, args.password)
    
    console_log_lvl    = set_log_lvl(args.console_log_level)
    file_log_lvl       = set_log_lvl(args.file_log_level)
    script_name        = splitext(basename(__file__))[0]
    configure_logging(script_name, console_log_lvl, file_log_lvl)
    
    logger = getLogger(script_name)

    my_devices = get_my_devices(api, logger, args.devices)
    table = make_pandas_dataframe(api, logger, args.devices, my_devices)

    columns = table.columns.tolist()
    new_columns = sort_columns(columns)
    table = table[new_columns]

    table.to_csv(args.csv_file, index=False)
    logger.info(f"Saved to {args.csv_file}.")

def delete_if_failed(logger, mac, apps):
    for i, app in enumerate(apps):
        if has_failed(app):
            logger.warning(
                f"{mac}: app {get_app_name_from_app_version(app)}, {get_version_from_app_version(app)} failed to install - ignoring"
            )
            del apps[i]  # We ignore any apps that failed to install  
    return apps

def select_firmwares_and_user_modules(logger, apps):
    firmwares = []
    user_modules = []
    for app in apps:
        if is_unknown_from_app_version(app):
            firmwares.append(app)
        else:
            user_modules.append(app)                
    if firmwares:
        logger.info(
            f"{len(firmwares)} firmwares, {len(user_modules)} User Modules"
        )
    else:
        logger.warning(
            f"No firmware application found, {len(user_modules)} User Modules"
        )

def select_user_modules(apps):
    user_modules = []
    for app in apps:
        if not is_unknown_from_app_version(app):
            user_modules.append(app)
    return user_modules       

def select_firmwares(logger, apps, user_modules):
    firmwares = []
    for app in apps:
        if is_unknown_from_app_version(app):
            firmwares.append(app)
    if firmwares:
        logger.info(
            f"{len(firmwares)} firmwares, {len(user_modules)} User Modules."
        )
    else:
        logger.warning(
            f"No firmware application found, {len(user_modules)} User Modules."
        ) 
    return firmwares      

def get_device_info(device, firmwares, user_modules):
    data = []
    device_info = {
        "Alias":        get_alias(device),
        "Serial No":    get_serial_number(device),
        "Order Code":   get_order_code(device),
        "MAC":          get_mac_address(device),
        "IMEI":         get_imei(device),
        "Type":         get_device_type_description(device),
    }

    for app in firmwares:
        # Note that we prefix each app name with "FW" so that we can group them later
        device_info[
            f"FW {get_app_name_from_app_version(app)}, {get_version_from_app_version(app)}"
        ] = summarise_app(app)

    for app in user_modules:
        device_info[
            f"{get_app_name_from_app_version(app)}, {get_version_from_app_version(app)}"
        ] = summarise_app(app)

    data.append(device_info)
    return data                    

def make_pandas_dataframe(api, logger, device, my_devices):
    for device in my_devices:
        mac = get_mac_address(device)
        logger.info(f"Device {mac}")
        response = api.device.get_apps(mac)
        log_response(logger, response, message=f"Failed to get applications details for device: {mac}")
        if apps := get_data(response):
            logger.info(f"{len(apps)} applications found")

            apps = delete_if_failed(logger, mac, apps)
            user_modules = select_user_modules(apps)
            firmwares = select_firmwares(logger, apps, user_modules)

        else:
            logger.error("No applications found!")
            continue 

        data = get_device_info(device, firmwares, user_modules)    

    table = pd.DataFrame(data)
    return table

def summarise_app(app):
    """Return a string the summarises the important information about the app"""
    state = app["state"]
    if state != "Installed":
        state = state.upper()

    pin_status = "Pinned" if is_pinned(app) else "NOT PINNED"

    known_status = "UNKNOWN" if is_unknown_from_app_version(app) else "Known"

    return f"{state}, {pin_status}, {known_status}" 

def sort_columns(columns):
    """Re-order a list of strings.
    Specifically, we want to keep all the Firmware apps together, before the User Modules.

    The first 6 positions are fixed. For example:
        0   Alias                       Alias
        1   Serial No                   Serial No
        2   Order Code                  Order Code
        3   MAC                         MAC
        4   IMEI                        IMEI
        5   Type                        Type
        6   FW ICR-321x, 6.2.5          FW ICR-321x, 6.2.5
        7   wadmp_client, 2.0.5         FW ICR-321x, 6.2.3
        8   ipsec_tools, 1.0.1          wadmp_client, 2.0.5
        9   zebra, 1.7.0                ipsec_tools, 1.0.1
        10  bgp, 1.7.0                  zebra, 1.7.0
        11  nhrp, 1.1.0                 bgp, 1.7.0
        12  netflow, 1.0.0              nhrp, 1.1.0
        13  scepClient, 2.0.0           netflow, 1.0.0
        14  FW ICR-321x, 6.2.3          scepClient, 2.0.0
        15  pinger, 2.3.3               pinger, 2.3.3

    """
    order = []
    first_fw_found = None

    for i, name in enumerate(columns):
        if name.startswith("FW "):
            if not first_fw_found:
                first_fw_found = True
                fw_index = i
                order.append(i)
            else:
                order.insert(fw_index + 1, i)
                fw_index += 1
        else:
            order.append(i)

    new_list = [columns[i] for i in order]

    return new_list               


if __name__ == "__main__":
    args = parse_args()
    main(args)
