#! /usr/bin/env python

"""
This script uses the public API to change the Desired settings for one section of the firmware,
for all devices.

February 2023
"""

from os.path import splitext, basename
from logging import getLogger
from argparse import ArgumentParser
from dmp.ApiConsumer import ApiConsumer
from utils.get import (get_sections, get_id_by_name_generally, get_mac_address)
from utils.logging_conf import set_log_lvl, configure_logging
from utils.services import get_my_devices, log_response, find_fw_ids


def parse_args():
    """Parse command-line arguments"""
    parser = ArgumentParser(
        description="Change settings for one section for all devices"
    )

    # Positional arguments:

    parser.add_argument("devices", help="CSV file of devices", type=str, default="ALL")

    parser.add_argument("section", help="Section name", type=str, default="snmp")

    parser.add_argument(
        "INIfile", help="Path to INI file", type=str, default="snmp.ini"
    )

    # Optional arguments:

    parser.add_argument(
        "-host",
        help="URL of the API gateway. \
                Default = 'https://gateway.wadmp.com'",
        type=str,
        default="https://gateway.wadmp.com",
    )

    parser.add_argument(
        "-username",
        help="Username. \
                Check the code for the default!",
        type=str,
        default="email address",
    )

    parser.add_argument(
        "-password",
        help="Password. \
                Check the code for the default!",
        type=str,
        default="password",
    )

    parser.add_argument(
        "-console_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    parser.add_argument(
        "-file_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    args = parser.parse_args()

    return args

def main(args):
    api = ApiConsumer(args.host)
    api.login(args.username, args.password)
    
    console_log_lvl    = set_log_lvl(args.console_log_level)
    file_log_lvl       = set_log_lvl(args.file_log_level)
    script_name        = splitext(basename(__file__))[0]
    configure_logging(script_name, console_log_lvl, file_log_lvl)
    
    logger = getLogger(script_name) 

    my_devices = get_my_devices(api, logger, args.devices) 

    desired_settings = open(args.INIfile, "r").read()
    logger.info(f"Desired State:\n{desired_settings}")
    logger.info(f"i.e. {len(desired_settings.splitlines())} individual parameters.\n") 

    change_settings_and_get_report(api, logger, my_devices, desired_settings)         

def change_settings_and_get_report(api, logger, my_devices, desired_settings):
    device_count = 0
    device_with_specified_section_count = 0

    for device in my_devices:
        device_count += 1
        logger.info(f"Device {get_mac_address(device)}")

        fw_app_id, fw_app_version_id = find_fw_ids(api, logger, get_mac_address(device))
        if fw_app_id and fw_app_version_id:
            section_id = find_section_and_change_settings(api, logger, fw_app_id, fw_app_version_id)
            device_with_specified_section_count = change_settings(
                api, 
                args, 
                logger, 
                device_with_specified_section_count, 
                section_id, 
                fw_app_version_id, 
                desired_settings, 
                device
            )

        else:
            logger.info("No firmware application found\n")            

    logger.info(
        f"{device_count} devices in total, of which {device_with_specified_section_count} have the {args.section} section"
    )
    logger.info(
        f"We changed the Desired State of {device_with_specified_section_count} devices."
    )

def find_section_and_change_settings(
        api, 
        logger,
        fw_app_id, 
        fw_app_version_id
    ):
    logger.info(
        f"Firmware application ID {fw_app_id}, application version ID {fw_app_version_id}"
    )

    response = api.application.get_app_version(fw_app_id, fw_app_version_id)
    log_response(logger, response, message="Failed to get the version details!")
    section_id = get_id_by_name_generally(get_sections(response), args.section)
    logger.info(f"Application section ID {section_id}\n")
    return section_id

def change_settings(
        api, 
        args, 
        logger, 
        device_with_specified_section_count, 
        section_id, 
        fw_app_version_id, 
        desired_settings, 
        device
    ):
    if section_id:
        device_with_specified_section_count += 1
        response = api.device.update_settings_section(get_mac_address(device), fw_app_version_id, section_id, desired_settings)
        log_response(logger, response, message="Failed to put the section settings!")
    else:
        logger.info(f"No {args.section} section found\n") 
    return device_with_specified_section_count           

if __name__ == "__main__":
    args = parse_args()
    main(args)
