"""
This script uses the public API to create a CSV file with a list of your devices.
Arguments to the script can be used to filter the devices.

February 2023
"""

from argparse import ArgumentParser
from os.path import splitext, basename
from csv import writer
from logging import getLogger
from enum import Enum
from dmp.ApiConsumer import ApiConsumer
from utils.logging_conf import set_log_lvl, configure_logging
from utils.services import log_response
from utils.get import (
    get_id_by_name, 
    get_data, 
    get_id, 
    get_total_items, 
    get_alias,
    get_serial_number, 
    get_order_code, 
    get_mac_address, 
    get_imei, 
    get_device_type_name
)


class GetValuesNames(Enum):
    company = "company", 
    group   = "group", 
    tag     = "tag", 
    type    = "type", 

def parse_args():
    """Parse command-line arguments"""
    parser = ArgumentParser(
        description="Get a list of devices, save in CSV file."
    )

    # Positional arguments:
    # None

    # Optional arguments:

    parser.add_argument(
        "-csv_file",
        help="Name of saved file. \
                Default = 'get_devices.csv'",
        type=str,
        default="get_devices.csv",
    )

    parser.add_argument(
        "-mac_or_serial_nbr",
        help="MAC Address or Serial Number of an individual device",
        type=str,
    )

    parser.add_argument(
        "-alias",
        help="Alias (name) of an individual device",
        type=str,
    )

    parser.add_argument(
        "-companies",
        help="Names of Companies to which devices belong",
        nargs="*",
        type=str,
    )

    parser.add_argument(
        "-groups",
        help="Names of Groups to which devices belong",
        nargs="*",
        type=str,
    )

    parser.add_argument(
        "-tags",
        help="Names of Tags which have been applied to devices",
        nargs="*",
        type=str,
    )

    parser.add_argument(
        "-types",
        help="List of Device Types",
        nargs="*",
        type=str,
    )

    parser.add_argument(
        "-is_online",
        help="isOnline?",
        choices=[True, False, None],
        type=str,
        default=None,
    )

    parser.add_argument(
        "-in_sync",
        help="inSync?",
        choices=[True, False, None],
        type=str,
        default=None,
    )

    parser.add_argument(
        "-host",
        help="URL of the API gateway. \
                Default = 'https://gateway.wadmp.com'",
        type=str,
        default="https://gateway.wadmp.com",
    )

    parser.add_argument(
        "-username",
        help="Username. \
                Check the code for the default!",
        type=str,
        default="email address",
    )

    parser.add_argument(
        "-password",
        help="Password. \
                Check the code for the default!",
        type=str,
        default="password",
    )

    parser.add_argument(
        "-console_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    parser.add_argument(
        "-file_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    args = parser.parse_args()

    return args


def main(args): 
    api = ApiConsumer(args.host)
    api.login(args.username, args.password)

    console_log_lvl = set_log_lvl(args.console_log_level)
    file_log_lvl    = set_log_lvl(args.file_log_level)

    script_name = splitext(basename(__file__))[0]
    configure_logging(script_name, console_log_lvl, file_log_lvl)        
    logger = getLogger(script_name)

    my_devices = get_my_devices(api, args, logger)
    save_to_file(args, logger, my_devices)

def get_ids_by_names(logger, name, list_of_names, api_call_function, *, key_name="name"):
    if list_of_names:
        logger.debug(f"Finding {name} IDs ...")
        ids = []
        for wanted_name in list_of_names:
            if id := get_id_by_name(api_call_function, wanted_name, key_name):
                logger.debug(f"{wanted_name} = {id}")
                ids.append(id)
            else:
                logger.warning(f"{wanted_name} not found!")
        if not ids:
            raise Exception(f"{name}'s were specified, but no ids were found.")
        return ids

    else:
        return []  

def get_type_ids_by_names(api, args, logger):
    args.types
    response = api.family.get_families()
    log_response(logger, response, message="Failed to get device family details.")
    families = get_data(response)
    ids = []
    for family in families:
        response = api.type.get_all_types_in_family(get_id(family))
        log_response(logger, response, message="Failed to get device types.")
        wanted_ids = get_ids_by_names(
            logger, 
            GetValuesNames.type.value[0], 
            args.types,
            response, 
            key_name="type_id"                
        )
        ids += wanted_ids
    return ids

def get_companies(api, logger):
    response = api.company.get_companies()
    log_response(logger, response, message="Failed to get companies details.")
    companies = get_ids_by_names(
        logger, 
        GetValuesNames.company.value[0], 
        args.companies, 
        response
    )
    return companies

def get_groups(api, logger):
    response = api.device_group.get_device_groups()
    log_response(logger, response, message="Failed to get device groups details.")    
    groups = get_ids_by_names(
        logger, 
        GetValuesNames.group.value[0], 
        args.groups,
        response
    )
    return groups

def get_tags(api, logger):
    response = api.device_tag.get_tags()
    log_response(logger, response, message="Failed to get device tags details.")      
    tags = get_ids_by_names(
        logger, 
        GetValuesNames.tag.value[0], 
        args.tags, 
        response
    )
    return tags

def get_my_devices(api, args, logger):
    companies   = get_companies(api, logger)
    groups      = get_groups(api, logger)
    types       = get_type_ids_by_names(api, args, logger)
    tags        = get_tags(api, logger)
    devices_model = {
        "name" :        args.alias,
        "companies":    companies, 
        "isOnline":     args.is_online,
        "groups":       groups, 
        "types":        types, 
        "tags":         tags,
        "inSync":       args.in_sync,
        "search":       args.mac_or_serial_nbr 
    }
    logger.info(f"Getting the list of matching devices ...")
    response = api.device.get_list_of_devices(devices_model)
    log_response(logger, response, message="Failed to get list of devices.")
    logger.info(f"Found {get_total_items(response)} devices in total.\n")
    return get_data(response)

def save_to_file(args, logger, my_devices):
    with open(args.csv_file, "w", encoding="UTF-8", newline="") as csvfile:
            csvwriter = writer(csvfile, delimiter=",")
            csvwriter.writerow(["Alias", "Serial No", "Order Code", "MAC", "IMEI", "Type"])
            for device in my_devices:
                csvwriter.writerow(
                    [
                        get_alias(device),
                        get_serial_number(device),
                        get_order_code(device),
                        get_mac_address(device),
                        get_imei(device),
                        get_device_type_name(device)
                    ]
                )

    logger.info(f"Saved to {args.csv_file}.") 

if __name__ == "__main__":
    args = parse_args()
    main(args)    