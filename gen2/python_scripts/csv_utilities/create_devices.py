#!/usr/bin/env python

"""
Python script which uses the WADMP public API to create a number of devices.

February 2023
"""

from argparse import ArgumentParser
from os.path import splitext, basename
from csv import reader
from logging import getLogger
from dmp.ApiConsumer import ApiConsumer
from utils.get import get_data, get_id, get_name, get_type_id
from utils.logging_conf import set_log_lvl, configure_logging
from utils.services import log_response


def parse_args():
    """Parse command-line arguments
    """
    parser = ArgumentParser(description="Create devices on WA/DMP")

    # Positional arguments:

    parser.add_argument("CSVfile", help="Path to CSV file.", type=str)

    # Optional arguments:

    parser.add_argument(
        "-host",
        help="URL of the WADMP server's API gateway. \
                                Check the code for the default!",
        type=str,
        default="https://gateway.wadmp.com",
    )

    parser.add_argument(
        "-username",
        help="Username. \
                                Check the code for the default!",
        type=str,
        default="email",
    )

    parser.add_argument(
        "-password",
        help="Password. \
                                Check the code for the default!",
        type=str,
        default="password",
    )

    parser.add_argument(
        "-console_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    parser.add_argument(
        "-file_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    return parser.parse_args()

def main(args):
    api = ApiConsumer(args.host)
    api.login(args.username, args.password)
    
    console_log_lvl    = set_log_lvl(args.console_log_level)
    file_log_lvl       = set_log_lvl(args.file_log_level)
    script_name        = splitext(basename(__file__))[0]
    configure_logging(script_name, console_log_lvl, file_log_lvl)
    
    logger = getLogger(script_name)

    all_types = all_device_types(api, logger)
    create_wanted_devices(api, logger, args, all_types)


def all_device_types(api, logger):
    # Create a list of all device types on the system
    all_types = []
    response = api.family.get_families()
    log_response(logger, response, message="Failed to retrieve the list of Families!")
    families = get_data(response)
    for family in families:
        family_id = get_id(family)
        response = api.type.get_all_types_in_family(family_id)
        log_response(logger, response, message="Failed to retrieve the list of Types!")
        types = get_data(response)
        all_types.extend(types)
    return all_types

def create_wanted_devices(api, logger, args, all_types):
    with open(args.CSVfile, encoding="UTF-8", newline="") as csvfile:
        csvreader = reader(csvfile, delimiter=",")
        next(csvreader)  # Skip the first row
        for row in csvreader:
            logger.debug(row)

            alias, serial_number, order_code, mac, imei, requested_type = row
            logger.info(f"Alias {alias}")
            logger.info(f"Serial Number {serial_number}")
            logger.info(f"Order Code {order_code}")
            logger.info(f"MAC {mac}")
            logger.info(f"IMEI {imei}")
            logger.info(f"Type {requested_type}\n")

            type_id = get_wanted_type(logger, all_types, requested_type)
            device = {
                "alias": alias,
                "serial_number": serial_number,
                "order_code": order_code,
                "mac_address": mac,
                "device_type_id": type_id,
                "imei": imei,
            }
            response = api.device.create(device)
            log_response(logger, response, message=f"Failed to create device {mac} !")

def get_wanted_type(logger, all_types, requested_type):
    type_id = None
    for existing_type in all_types:
        if get_name(existing_type) == requested_type:
            type_id = get_type_id(existing_type)
            break

    if type_id:
        logger.debug(f"Device type {requested_type} has ID {type_id}")
    else:
        logger.error(f"Device type {requested_type} not found!")
    return type_id

if __name__ == "__main__":
    args = parse_args()
    main(args)
