#! /usr/bin/env python

"""
This script uses the public API to install or upgrade an application, on multiple devices.
An "application" may be Firmware or a User Module.

February 2023
"""

from argparse import ArgumentParser
from os.path import splitext, basename
from logging import getLogger
from sys import exit
from dmp.ApiConsumer import ApiConsumer
from utils.get import (
    get_id, 
    get_data, 
    get_versions, 
    get_name, 
    get_version, 
    is_firmware, 
    is_unknown, 
    get_mac_address,
    get_state_of_app_on_device, 
)
from utils.logging_conf import set_log_lvl, configure_logging
from utils.services import get_my_devices, log_response


def parse_args():
    """Parse command-line arguments"""
    parser = ArgumentParser(description="Install an app. on multiple devices")

    # Positional arguments:

    parser.add_argument("devices", help="CSV file of devices", type=str, default="ALL")

    parser.add_argument("appName", help="App. name", type=str, default="wadmp_client")

    parser.add_argument(
        "appVersion", help="App. version number", type=str, default="2.0.5"
    )

    # Optional arguments:

    parser.add_argument(
        "-host",
        help="URL of the API gateway. \
                Default = 'https://gateway.wadmp.com'",
        type=str,
        default="https://gateway.wadmp.com",
    )

    parser.add_argument(
        "-username",
        help="Username. \
                Check the code for the default!",
        type=str,
        default="email address",
    )

    parser.add_argument(
        "-password",
        help="Password. \
                Check the code for the default!",
        type=str,
        default="password",
    )

    parser.add_argument(
        "-console_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    parser.add_argument(
        "-file_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    # If --replace_pending_installs is NOT given on the command line, it defaults to False.
    parser.add_argument(
        "--replace_pending_installs",
        help="If a device already has an upgrade-downgrade pending for this app, replace it",
        action="store_true",
    )

    args = parser.parse_args()

    return args


def main(args):
    api = ApiConsumer(args.host)
    api.login(args.username, args.password)
    
    console_log_lvl    = set_log_lvl(args.console_log_level)
    file_log_lvl       = set_log_lvl(args.file_log_level)
    script_name        = splitext(basename(__file__))[0]
    configure_logging(script_name, console_log_lvl, file_log_lvl)
    
    logger          = getLogger(script_name)  
    app_version_id  = get_wanted_app(api, args, logger)
    my_devices      = get_my_devices(api, logger, args.devices)

    install_app_to_devices(api, logger, args, my_devices, app_version_id)    


def get_wanted_app(api, args, logger):
    app_id          = None
    app_version_id  = None
    response        = api.application.get_apps(args.appName)
    log_response(logger, response, message="Failed to retrieve the list of Applications!")
    matching_apps = get_data(response) 

    if len(matching_apps) == 0:
        logger.error(f"No app found with name '{args.appName}'")
        exit(1)

    elif len(matching_apps) == 1:
        matching_apps = matching_apps[0]
        if is_unknown(matching_apps) is False:
            app_id = get_app_id_from_one_found_app(logger, args, matching_apps)
            app_version_id = get_app_version_id_from_one_found_app(logger, matching_apps)
    else:
        logger.warning(
            f"{len(matching_apps)} apps found with a name matching '{args.appName}'! Proceeding with caution ..."
        )
        app_id, app_version_id = get_ids_from_many_apps(logger, args, matching_apps)
    check_if_ids_exist(logger, args, app_id, app_version_id)
    return app_version_id

def get_app_id_from_one_found_app(logger, args, matching_apps):
    app_id = get_id(matching_apps)
    logger.info(f"App name {args.appName} has app ID {app_id}")
    if is_firmware(matching_apps) is True:
        logger.info("This application is FIRMWARE")
    return app_id

def get_app_version_id_from_one_found_app(logger, matching_apps):
    available_versions = get_versions(matching_apps)
    for available_version in available_versions:
        if get_version(available_version) == args.appVersion:
            app_version_id = available_version["id"]
            break
    if app_version_id:
        logger.info(
            f"{args.appName} version {args.appVersion} has app version ID {app_version_id}"
        )
    else:
        logger.error(
            f"No match found for {args.appVersion}. Please double-check!"
        )
        exit(1)
    return app_version_id

def get_ids_from_many_apps(logger, args, matching_apps):
    for matching_app in matching_apps:
        logger.info(f"Considering {get_name(matching_app)} ...")
        if get_name(matching_app) == args.appName:
            app_id          = get_app_id_from_matching_app(logger, args, matching_app)
            app_version_id  = get_app_version_id_from_matching_app(logger, args, matching_app)
            break
    return app_id, app_version_id

def get_app_id_from_matching_app(logger, args, matching_app):
    logger.info(f"{args.appName} is an EXACT match")
    app_id = get_id(matching_app)
    logger.info(f"App name {args.appName} has app ID {app_id}")
    if is_firmware(matching_app) is True:
        logger.info("This application is FIRMWARE")
    return app_id

def get_app_version_id_from_matching_app(logger, args, matching_app):
    available_versions = get_versions(matching_app)
    for available_version in available_versions:
        if get_version(available_version) == args.appVersion:
            app_version_id = get_id(available_version)
            break
    if app_version_id:
        logger.info(
            f"{args.appName} version {args.appVersion} has app version ID {app_version_id}"
        )
    return app_version_id

def check_if_ids_exist(logger, args, app_id, app_version_id):
    if app_id:
        if app_version_id:
            logger.info(
                f"Proceeding with app ID {app_id} and app version ID {app_version_id}"
            )
        else:
            logger.error(
                f"No match found for {args.appVersion}. Please double-check!"
            )
            exit(1)
    else:
        logger.error(
            f"No exact match found for {args.appName}. Please double-check the spelling, upper/lower-case, etc."
        )
        exit(1)  

def install_app_to_devices(api, logger, args, my_devices, app_version_id):
    device_count = 0
    installed_device_count = 0

    for device in my_devices:
        device_count += 1
        logger.info(f"Device {get_mac_address(device)}")

        response = api.device.install_app(get_mac_address(device), app_version_id)
        if response.status_code == 200:
            installed_device_count += 1
        elif response.status_code == 400:
            try_reinstall_pending_app(api, logger, device, app_version_id)
        else:
            logger.error(
                f"   Failed to install {args.appName} on {get_mac_address(device)}! With response code: {response.status_code}"
            ) 
    logger.info(
        f"{device_count} devices in total, and we installed {args.appName} version {args.appVersion} on {installed_device_count}."
    ) 

def try_reinstall_pending_app(api, logger, device, app_version_id):
    response = get_data(api.device.get_appversion(get_mac_address(device), app_version_id)) 
    if get_state_of_app_on_device(response) == "Pending":
        if args.replace_pending_installs:
            logger.debug("Attempting to remove the pending installation ...")
            logger.info(
                f"   Removing pending installation {app_version_id}"
            )
            response = api.device.remove_app(get_mac_address(device), app_version_id)
            if response.status_code == 200:
                # Try to install again
                response = api.device.install_app(get_mac_address(device), app_version_id)
                if response.status_code == 200:
                    installed_device_count += 1 
    else:
        logger.error(
            f"   Failed to find a pending installation of {args.appName} on {get_mac_address(device)}!"
        )    


if __name__ == "__main__":
    args = parse_args()
    main(args)
