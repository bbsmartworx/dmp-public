import json
from csv import reader
from requests import Response
from utils.get import get_data, is_firmware_from_app_version, get_app_id_from_version, get_app_version_id


def find_fw_ids(api, logger, mac):
    """For the given device, find the app ID and the app version ID for the firmware app."""
    fw_app_id = None
    fw_app_version_id = None
    response = api.device.get_apps(mac)
    log_response(logger, response, message="Failed to retrieve the list of Applications!")
    if apps := get_data(response):
        for app in apps:
            if is_firmware_from_app_version(app):
                fw_app_id = get_app_id_from_version(app)
                fw_app_version_id = get_app_version_id(app)
                break

    return fw_app_id, fw_app_version_id  

def get_my_devices(api, logger, devices):
    if devices == "ALL":
        logger.info("Getting a list of ALL your devices ...")
        response = api.device.get_list_of_devices()
        log_response(logger, response, message="Failed to retrieve device details!")
        my_devices = get_data(response)
        logger.info(f"You have {len(my_devices)} devices in total.\n")
    else:
        logger.info("Opening CSV file of devices ...")
        with open(devices, encoding="UTF-8", newline="") as csvfile:
            csvreader = reader(csvfile, delimiter=",")
            next(csvreader)  # Skip the first row
            my_devices = []
            for row in csvreader:
                logger.debug(row)
                alias, serial_number, order_code, mac, imei, device_type = row
                device = {
                    "alias": alias,
                    "serial_number": serial_number,
                    "order_code": order_code,
                    "mac_address": mac,
                    "imei": imei,
                    "device_type": {"description": device_type},
                }
                my_devices.append(device)
        logger.info(f"File contains {len(my_devices)} devices in total.\n")
    return my_devices

def log_response(logger, response: Response, *, message="Failed to retrieve data!"):
    logger.debug(f"Sending request.")
    logger.debug(response.status_code)
    try:
        logger.debug(json.dumps(response.json(), indent=4, sort_keys=True))
    except ValueError:
        logger.debug(response.text)
    if response.status_code == 200:
        try:
            response.json()
        except json.decoder.JSONDecodeError as err:
            logger.error(f"Problem decoding JSON!\n{err}")
        except KeyError as err:
            logger.error(f"Didn't find what we expected in the JSON response!\n{err}")    
    else:
        logger.error(
            f"{message} Response code: {response.status_code}"
        )
        try:
            logger.error(f"{response.json()['message']}")
        except json.decoder.JSONDecodeError as err:
            logger.error(f"Problem decoding JSON!\n{err}")
        except KeyError as err:
            logger.error(response.json())

