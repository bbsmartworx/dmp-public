from logging import config, Formatter, CRITICAL, ERROR, WARNING, INFO, DEBUG
from time import gmtime


class UTCFormatter(Formatter):
    """Allows us to configure the logging timestamps to use UTC.

    We could have used an external JSON or YAML file to hold the configuration for logging.config.dictConfig(),
    but there is no way to configure timestamps via a file!
    """

    converter = gmtime

def set_log_lvl(lvl):
    # A log message will only be emitted if the message 
    # level is greater than or equal to the configured level of the logger.
    LOG_LEVELS = {
        "critical": CRITICAL,
        "error":    ERROR,
        "warning":  WARNING,
        "info":     INFO,
        "debug":    DEBUG,
    }

    return LOG_LEVELS[lvl]

def configure_logging(name, console_loglevel, file_loglevel):
    """We use a dictionary to configure the Python logging module."""

    LOG_CONFIG = {
        "version": 1,
        "disable_existing_loggers": False,
        "loggers": {name: {"level": "DEBUG", "handlers": ["console", "file"]}},
        "handlers": {
            "console": {
                "level": console_loglevel,
                "class": "logging.StreamHandler",
                "formatter": "console_format",
            },
            "file": {
                "level": file_loglevel,
                "class": "logging.handlers.RotatingFileHandler",
                "formatter": "file_format",
                "filename": f"{name}.log",
                "mode": "w",
            },
        },
        "formatters": {
            "console_format": {"format": "%(name)s - %(levelname)s - %(message)s"},
            "file_format": {
                "()": UTCFormatter,
                "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s",
            },
        },
    }

    config.dictConfig(LOG_CONFIG)
