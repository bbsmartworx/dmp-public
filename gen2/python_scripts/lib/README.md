# Preconditions
1) Specific version of python build library is installed depending on the python version. Check pyproject.toml file for more details.

# Building DMP package
1) In command line move to python_scripts/lib directory
2) python3 -m build

# Installing DMP package
1) In command line move to python_scripts directory
2) pip3 install ./lib/dist/dmp-1.0.0.tar.gz