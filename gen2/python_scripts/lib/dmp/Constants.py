# Definitions of constants
BASE_URL_LOCALHOST = 'http://localhost:8082'
BASE_URL_DEV = 'https://gateway.dev.wadmp.com'
BASE_URL_PROD = 'https://gateway.wadmp.com'
BASE_URL_STAGING = 'https://gateway.staging.wadmp.com'