import requests
import time
import random as rdm
import string as str
from .RequestSession import RequestSession
from dmp.Models import *

class ApiClients:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_api_client_by_id(self, api_client_id):
        url = f"{self.base_url_api}/api-clients/{api_client_id}"
        response = self.session.get(url)
        return response

    def edit_client(self, api_client_id, model: UpdateAPIClientModel):
        url = f"{self.base_url_api}/api-clients/{api_client_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_by_id(self, api_client_id):
        url = f"{self.base_url_api}/api-clients/{api_client_id}"
        response = self.session.delete(url)
        return response

    def get_clients(self):
        url = f"{self.base_url_api}/api-clients"
        response = self.session.get(url)
        return response

    def create(self, model: CreateAPIClientModel):
        url = f"{self.base_url_api}/api-clients"
        response = self.session.post(url, json=model)
        return response

class ApiBilling:
    def __init__(self, base_url_api, session):
            self.session = session
            self.base_url_api = base_url_api

    def get_billing_job_by_id(self, job_id):
        url = f"{self.base_url_api}/billing/schedule/{job_id}"
        response = self.session.get(url)
        return response

    def delete_billing_job_by_id(self, job_id):
        url = f"{self.base_url_api}/billing/schedule/{job_id}"
        response = self.session.delete(url)
        return response

    def get_invoice_by_id(self, invoice_id):
        url = f"{self.base_url_api}/billing/{invoice_id}"
        response = self.session.get(url)
        return response

    def get_list_of_invoices(self, company_id=None):
        url = f"{self.base_url_api}/billing"
        if company_id:
            url += f"?companyId={company_id}"
        response = self.session.get(url)
        return response

    def get_list_of_billing_jobs(self):
        url = f"{self.base_url_api}/billing/schedule"
        response = self.session.get(url)
        return response

    def schedules_billing_job(self,  start_date=None, end_date=None, schedule_next=None):
        url = f"{self.base_url_api}/billing/schedule"
        if any:
            url += "?"
        if start_date:
            url += f"startDate={start_date}&"
        if end_date:
            url += f"endDate={end_date}&"
        if schedule_next:
            url += f"scheduleNext={schedule_next}&"
        response = self.session.post(url[:-1])
        return response

    def generate_new_invoice(self, model: GenerateInvoiceTestModel):
        url = f"{self.base_url_api}/billing/test"
        response = self.session.post(url, json=model)
        return response

    def generate_new_invoice_create_sap(self, model: GenerateSapInvoiceModel):
        url = f"{self.base_url_api}/billing/sap"
        response = self.session.post(url, json=model)
        return response

class ApiDeviceGroups:
    def __init__(self, base_url_api, session):
            self.session = session
            self.base_url_api = base_url_api

    def get_device_group_by_id(self, dev_g_id):
        url = f"{self.base_url_api}/management/device-groups/{dev_g_id}"
        response = self.session.get(url)
        return response

    def edit_device_group(self, dev_g_id, model: UpdateDeviceGroupModel):
        url = f"{self.base_url_api}/management/device-groups/{dev_g_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_device_group(self, dev_g_id):
        url = f"{self.base_url_api}/management/device-groups/{dev_g_id}"
        response = self.session.delete(url)
        return response

    def add_devices_to_device_group(self, dev_g_id, devices):
        url = f"{self.base_url_api}/management/device-groups/{dev_g_id}/devices"
        response = self.session.post(url, json=devices)
        return response

    def delete_devices(self, dev_g_id, devices):
        url = f"{self.base_url_api}/management/device-groups/{dev_g_id}/devices"
        response = self.session.delete(url, json=devices)
        return response

    def get_device_groups(self, name=None, company_ids=None):
        url = f"{self.base_url_api}/management/device-groups"
        query = {
            'name': name,
            'company_id': company_ids
        }
        response = self.session.get(url, params=query)
        return response

    def create_new_device_group(self, model: CreateDeviceGroupModel):
        url = f"{self.base_url_api}/management/device-groups"
        response = self.session.post(url, json=model)
        return response

class ApiTypes:
    def __init__(self, base_url_api, session):
            self.session = session
            self.base_url_api = base_url_api

    def get_by_id(self, type_id):
        url = f"{self.base_url_api}/identity/device-families/device-types/{type_id}"
        response = self.session.get(url)
        return response

    def get_all_types_in_family(self, family_id):
        url = f"{self.base_url_api}/identity/device-families/{family_id}/device-types"
        response = self.session.get(url)
        return response

    def create(self, family_id, model: CreateTypeModel): 
        url = f"{self.base_url_api}/identity/device-families/{family_id}/device-types"
        response = self.session.post(url, json=model)
        return response

class ApiFiles:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_by_id(self, file_id):
        url = f"{self.base_url_api}/files/{file_id}"
        response = self.session.get(url)
        return response

    def update_by_id(self, file_id, name, source_file_path):
        url = f"{self.base_url_api}/files/{file_id}"
        query = {'name': name}
        data = open(source_file_path, 'rb').read()
        response = self.session.put(url, params=query, data=data)
        return response

    def delete(self, file_id):
        url = f"{self.base_url_api}/files/{file_id}"
        response = self.session.delete(url)
        return response

    def get_files(self, name=None):
        url = f"{self.base_url_api}/files"
        query = dict()
        if name:
            query['FileName'] = name
        response = self.session.get(url, params=query)
        return response

    def upload_file(self, name, source_file_path):
        url = f"{self.base_url_api}/files"
        query = {'name': name}
        data = open(source_file_path, 'rb').read()
        response = self.session.post(url, params=query, data=data)
        return response

class ApiAuditing:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_audit_logs(self, model=None):
        url = f"{self.base_url_api}/auditing"
        response = self.session.get(url, params=model)
        return response

    def get_all_action_types(self):
        url = f"{self.base_url_api}/auditing/action-types"
        response = self.session.get(url)
        return response

class ApiDeviceTags:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_tags_by_id(self, tag_id):
        url = f"{self.base_url_api}/management/device-tags/{tag_id}"
        response = self.session.get(url)
        return response

    def edit_tags_details(self, tag_id, model: UpdateDeviceTagModel):
        url = f"{self.base_url_api}/management/device-tags/{tag_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_by_id(self, tag_id):
        url = f"{self.base_url_api}/management/device-tags/{tag_id}"
        response = self.session.delete(url)
        return response

    def assign_tag(self, tag_id, macs): # 'macs' is list of device MACs
        url = f"{self.base_url_api}/management/device-tags/{tag_id}/devices"
        response = self.session.post(url, json=macs)
        return response

    def delete_list_of_devices(self, tag_id, macs): # macs is list of device MACs
        url = f"{self.base_url_api}/management/device-tags/{tag_id}/devices"
        response = self.session.delete(url, json=macs)
        return response

    def get_tags(self, name = None, company_ids=None):
        url = f"{self.base_url_api}/management/device-tags"
        query = {
            'name': name,
            'company_id': company_ids
        }
        response = self.session.get(url, params=query)
        return response

    def create(self, model: CreateDeviceTagModel):
        url = f"{self.base_url_api}/management/device-tags"
        response = self.session.post(url, json=model)
        return response

class ApiFamilies:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_by_id(self, family_id):
        url = f"{self.base_url_api}/identity/device-families/{family_id}"
        response = self.session.get(url)
        return response

    def get_families(self, name=None, types=True):
        url = f"{self.base_url_api}/identity/device-families"
        query = dict()
        if name:
            query['name'] = name
        if types:
            query['includeTypes'] = types
        response = self.session.get(url, params=query)
        return response

    def create(self, model: CreateFamilyModel):
        url = f"{self.base_url_api}/identity/device-families"
        response = self.session.post(url, json=model)
        return response

class ApiPlaybooks:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_by_id(self, playbook_id):
        url = f"{self.base_url_api}/management/playbooks/{playbook_id}"
        response = self.session.get(url)
        return response

    # The input dictionary 'model' may contain keys:
    #   id, name, company_id, device_type_id
    # actions: id, type, settings_group_id, install_application_id,
    #   appplication_version_section_id, individual_setting_key, order
    # device_settings: device, setting_value
    # devices
    def update_playbook(self, playbook_id, model):
        url = f"{self.base_url_api}/management/playbooks/{playbook_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_playbook(self, playbook_id):
        url = f"{self.base_url_api}/management/playbooks/{playbook_id}"
        response = self.session.delete(url)
        return response

    def get_playbooks(self, states=['Draft', 'Scheduled', 'Running', 'Completed', \
                                    'Failed', 'Incomplete', 'Aborted', 'CompletedWithFailure', \
                                    'Deleted'], page_size = 9999, page = 1):
        url = f"{self.base_url_api}/management/playbooks?page={page}&pageSize={page_size}"
        for state in states:
            url += f'&states={state}'
        response = self.session.get(url)
        return response

    # The input dictionary 'model' may contain keys:
    #   name, company_id, device_type_id
    # actions: type, settings_group_id, install_application_version_id,
    #   application_version_section_id, individual_setting_key, order
    # device_settings: device, setting_value
    # devices
    def create(self, model):
        url = f"{self.base_url_api}/management/playbooks"
        response = self.session.post(url, json=model)
        return response

    def get_playbook_details_for_device(self, playbook_id, dev_mac):
        url = f"{self.base_url_api}/management/playbooks/{playbook_id}/devices/{dev_mac}"
        response = self.session.get(url)
        return response

    def schedule(self, playbook_id): # NOW (add time parameter in the future)
        url = f"{self.base_url_api}/management/playbooks/{playbook_id}/schedule"
        response = self.session.post(url)
        return response

    def unschedule(self, playbook_id):
        url = f"{self.base_url_api}/management/playbooks/{playbook_id}/unschedule"
        response = self.session.post(url)
        return response

    def aborts_running_playbook(self, playbook_id):
        url = f"{self.base_url_api}/management/playbooks/{playbook_id}/abort"
        response = self.session.post(url)
        return response

class ApiSyncEngine:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_sync_engine(self, company_id):
        url = f"{self.base_url_api}/syncengine/config"
        if company_id:
            url += f"?companyId={company_id}"
        response = self.session.get(url)
        return response

    def update_config(self, company_id, sync_type="Custom", retry_interval=0, retry_attempts=3):
        url = f"{self.base_url_api}/syncengine/config"
        model = {
            "sync_type": sync_type,
            "retry_interval": retry_interval,
            "retry_attempts": retry_attempts,
            "company_id": company_id
        }
        response = self.session.put(url, json=model)
        return response

class ApiCompanies:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_by_id(self, company_id):
        url = f"{self.base_url_api}/companies/{company_id}"
        response = self.session.get(url)
        return response

    def update_by_id(self, company_id, model: UpdateCompanyModel):
        url = f"{self.base_url_api}/companies/{company_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_by_id(self, company_id):
        url = f"{self.base_url_api}/companies/{company_id}"
        response = self.session.delete(url)
        return response

    def get_companies(self, model: GetCompanyModel):
        url = f"{self.base_url_api}/companies"
        query = RequestSession.make_query({
            'name': model.name,
            'sort': model.sort,
            'sortOrder': model.sort_order
        })
        response = self.session.get(url, params=query)
        return response

    def create(self, model: CreateCompanyModel):
        url = f"{self.base_url_api}/companies"
        response = self.session.post(url, json=model)
        return response

    def send_req_to_upgrade_to_premium(self, company_id, erp_id=None):
        url = f"{self.base_url_api}/companies/{company_id}/upgrade/request"
        if erp_id:
            url += f"?erpid={erp_id}"
        response = self.session.post(url)
        return response

    def set_to_premium_by_id(self, company_id, model: UpgradeCompanyPremium):
        url = f"{self.base_url_api}/companies/{company_id}/upgrade/premium"
        query = RequestSession.make_query({
            'ErpId': model.erpid,
            'OrgId': model.orgid,
            'CrmId': model.crmid,
            'BillingMethod': model.billing_method,
            'Currency': model.currency,
            'PoNumber': model.po_number,
            'OrderType': model.order_type,
            'DistributionChannel': model.distribution_channel,
            'Division': model.division,
            'PartNumber': model.part_number,
        })
        response = self.session.put(url, params=query)
        return response

    def set_to_trial_by_id(self, company_id):
        url = f"{self.base_url_api}/companies/{company_id}/upgrade/trial"
        response = self.session.put(url)
        return response

    def set_to_free_by_id(self, company_id):
        url = f"{self.base_url_api}/companies/{company_id}/upgrade/free"
        response = self.session.put(url)
        return response


class ApiAlertHistory:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_history(self, model: GetAlertHistoryModel):
        url = f"{self.base_url_api}/alert-history"
        query = RequestSession.make_query({
            'page': model.page,
            'pageSize': model.pagesize,
            'isAcked': model.is_acked,
            'alertIds': model.alert_ids,
            'companyIds': model.company_ids,
            'deviceIds': model.device_ids
        })
        response = self.session.get(url, params=query)
        return response

    def ack_records(self, ids):
        url = f"{self.base_url_api}/alert-history/ack?"
        for id in ids:
            url += f'id={id}&'
        response = self.session.put(url[:-1])
        return response

    def unack_records(self, ids):
        url = f"{self.base_url_api}/alert-history/unack?"
        for id in ids:
            url += f'id={id}&'
        response = self.session.put(url[:-1])
        return response

class ApiAlerts:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_alerts(self, model: GetAlertModel):
        url = f"{self.base_url_api}/alerts"
        query = RequestSession.make_query({
            'page': model.page,
            'pageSize': model.pagesize,
            'name': model.name,
            'companyIds': model.company_ids,
            'deviceMacAddresses': model.device_mac_addresses,
            'alertSortingType': model.sort,
            'sortingOrder': model.sort_order
        })
        response = self.session.get(url, params=query)
        return response

    def get_by_id(self, alert_id):
        url = f"{self.base_url_api}/alerts/{alert_id}"
        response = self.session.get(url)
        return response

    def delete_by_id(self, alert_id):
        url = f"{self.base_url_api}/alerts/{alert_id}"
        response = self.session.delete(url)
        return response

    # The input dictionary 'model' may contain keys:
    #   name, company_id, mac_address, is_enabled, cooldown, period
    #   critical_device_count, critical_repetitions, alertable_field_id,
    #   alert_operator_id, operand1, operand2
    def create(self, model: CreateAlertModel):
        url = f"{self.base_url_api}/alerts"
        response = self.session.post(url, json=model)
        return response

    def update_by_id(self, alert_id, model: UpdateAlertModel):
        url = f"{self.base_url_api}/alerts/{alert_id}"
        response = self.session.put(url, json=model)
        return response

class ApiAlertEndpoints:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_endpoints(self, model: GetEndpointModel):
        url = f"{self.base_url_api}/alert-endpoints"
        query = RequestSession.make_query({
            'page': model.page,
            'pageSize': model.pagesize,
            'name': model.name,
            'companyIdsFilter': model.company_ids,
        })
        response = self.session.get(url, params=query)
        return response

    def get_by_id(self, endpoint_id):
        url = f"{self.base_url_api}/alert-endpoints/{endpoint_id}"
        response = self.session.get(url)
        return response

    def delete_by_id(self, endpoint_id):
        url = f"{self.base_url_api}/alert-endpoints/{endpoint_id}"
        response = self.session.delete(url)
        return response

    # The input dictionary 'endpoint_dict' may contain keys:
    # name, endpoint_type ("Email"), recipient (string),
    # company_id (int), is_enabled (bool)
    def create(self, model: CreateAlertEndpointModel):
        url = f"{self.base_url_api}/alert-endpoints"
        response = self.session.post(url, json=model)
        return response

    def update_by_id(self, endpoint_id, model: UpdateAlertEndpointModel):
        url = f"{self.base_url_api}/alert-endpoints/{endpoint_id}"
        response = self.session.put(url, json=model)
        return response

    def assign_endpoints(self, alert_id, endpoint_ids):
        url = f"{self.base_url_api}/alerts/{alert_id}/assign-endpoints?"
        for id in endpoint_ids:
            url += f'alertEndpointIds={id}&'
        response = self.session.put(url[:-1])
        return response

class ApiAlertableFields:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_fields(self, model: GetAlertableFieldsModel):
        url = f"{self.base_url_api}/alertable-fields"
        query = RequestSession.make_query({
            'page': model.page,
            'pageSize': model.pagesize,
            'name': model.name,
            'isDeviceOriented': model.is_device_oriented,
            'sourceType': model.source_type,
            'dataType': model.data_type
        })
        response = self.session.get(url, params=query)
        return response

class ApiApplications:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_app_by_id(self, app_id):
        url = f"{self.base_url_api}/applications/{app_id}"
        response = self.session.get(url)
        return response

    def edit_app(self, app_id, model: UpdateApplicationModel):
        url = f"{self.base_url_api}/applications/{app_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_by_id(self, app_id):
        url = f"{self.base_url_api}/applications/{app_id}"
        response = self.session.delete(url)
        return response

    def get_app_version(self, app_id, app_version_id):
        url = f"{self.base_url_api}/applications/{app_id}/versions/{app_version_id}"
        response = self.session.get(url)
        return response

    def edit_app_version(self, application_id, version_id, model: UpdateApplicationVersionModel):
        url = f"{self.base_url_api}/applications/{application_id}/versions/{version_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_app_version(self, app_id, app_version_id):
        url = f"{self.base_url_api}/applications/{app_id}/versions/{app_version_id}"
        response = self.session.delete(url)
        return response

    def get_app_version_section_def_by_id(self, app_id, app_version_id, app_section_id):
        url = f"{self.base_url_api}/applications/{app_id}/versions/{app_version_id}/sections/{app_section_id}"
        response = self.session.get(url)
        return response

    def edit_app_version_section(self, app_id, version_id, section_id, data: UpdateApplicationVersionSectionModel):
        url = f"{self.base_url_api}/applications/{app_id}/versions/{version_id}/sections/{section_id}"
        response = self.session.put(url, json=data)
        return response

    def delete_section_from_app_version(self, app_id, app_version_id, app_section_id):
        url = f"{self.base_url_api}/applications/{app_id}/versions/{app_version_id}/sections/{app_section_id}"
        response = self.session.delete(url)
        return response

    def get_version_of_dmp_app(self):
        url = f"{self.base_url_api}/dmp/version"
        response = self.session.get(url)
        return response

    def get_apps(self, name = None, device_type_ids=None):
        url = f"{self.base_url_api}/applications"
        query = RequestSession.make_query({
            'name': name,
            'deviceTypeIds': device_type_ids
        })
        response = self.session.get(url, params=query)
        return response

    def create_new_app_def(self, model: CreateApplicationModel):
        url = f"{self.base_url_api}/applications"
        response = self.session.post(url, json=model)
        return response

    def get_versions_from_app(self, app_id, device_type_ids=[]):
        url = f"{self.base_url_api}/applications/{app_id}/versions"
        response = self.session.get(url, json=device_type_ids)
        return response

    def create_new_app_version_def(self, app_id, model: CreateApplicationVersionModel):
        url = f"{self.base_url_api}/applications/{app_id}/versions"
        response = self.session.post(url, json=model)
        return response

    def create_new_section_def(self, app_id, version_id, model):
        url = f"{self.base_url_api}/applications/{app_id}/versions/{version_id}/sections"
        response = self.session.post(url, json=model)
        return response

    def update_list_of_device_types_by_app_id(self, app_id, version_id, file_id, device_types_ids=[]):
        url = f"{self.base_url_api}/applications/{app_id}/versions/{version_id}/file/{file_id}/device-types"
        response = self.session.put(url, json=device_types_ids)
        return response

class ApiDevices:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def _generate_password(self):
        characters = list(str.ascii_letters + str.digits + "!@#$%^&*()")
        rdm.shuffle(characters)
        password = []
        for i in range(8):
            password.append(rdm.choice(characters))
        rdm.shuffle(password)
        new_password = "".join(password)
        return new_password

    def create(self, model: CreateDeviceModel):
        url = f"{self.base_url_api}/identity/devices"
        response = self.session.post(url, json=model)
        return response

    def claim(self, model: ClaimDeviceModel):
        url = f"{self.base_url_api}/identity/devices/claim"
        response = self.session.post(url, json=model)
        return response

    def release(self, model: ReleaseDeviceModel):
        url = f"{self.base_url_api}/identity/devices/release"
        response = self.session.post(url, json=model)
        return response

    def delete_by_mac(self, mac):
        url = f"{self.base_url_api}/identity/devices/{mac}"
        response = self.session.delete(url)
        return response

    def update_by_mac(self, mac):
        url = f"{self.base_url_api}/management/devices/{mac}"
        response = self.session.put(url)
        return response

    def get_by_mac(self, mac):
        url = f"{self.base_url_api}/management/devices/{mac}"
        response = self.session.get(url)
        return response

    def get_apps(self, mac):
        url = f"{self.base_url_api}/management/devices/{mac}/apps"
        response = self.session.get(url)
        return response

    def get_settings(self, mac, app_version_id):
        url = f"{self.base_url_api}/management/devices/{mac}/apps/{app_version_id}/settings"
        response = self.session.get(url)
        return response

    def update_settings(self, mac, app_version_id, data):
        url = f"{self.base_url_api}/management/devices/{mac}/apps/{app_version_id}/settings"
        response = self.session.put(url, json=data)
        return response

    def install_app(self, mac, app_version_id):
        url = f"{self.base_url_api}/management/devices/{mac}/apps/{app_version_id}"
        response = self.session.post(url)
        return response

    def remove_app(self, mac, app_version_id):
        url = f"{self.base_url_api}/management/devices/{mac}/apps/{app_version_id}"
        response = self.session.delete(url)
        return response

    def get_appversion(self, mac, app_version_id):
        url = f"{self.base_url_api}/management/devices/{mac}/apps/{app_version_id}"
        response = self.session.get(url)
        return response

    def get_section(self, mac, app_version_id, section_id):
        url = f"{self.base_url_api}/management/devices/{mac}/apps/{app_version_id}/settings/{section_id}"
        response = self.session.get(url)
        return response

    def get_commands(self, mac):
        url = f"{self.base_url_api}/management/devices/{mac}/commands"
        response = self.session.get(url)
        return response

    def post_reboot(self, mac):
        url = f"{self.base_url_api}/management/devices/{mac}/commands/reboot"
        response = self.session.post(url)
        return response
        
    def update_settings_section(self, mac, app_version_id, section_id, model: WriteApplicationSettingsModel):
        url = f"{self.base_url_api}/management/devices/{mac}/apps/{app_version_id}/settings/{section_id}"
        response = self.session.put(url, json=model)
        return response

    def edit_device(self, mac, new_mac, alias):
        url = f"{self.base_url_api}/identity/devices/{mac}"
        model = {
                "mac_address": new_mac,
                "alias": alias
                }
        response = self.session.put(url, json=model)
        return response

    def get_mngt_server_conf_for_device(self, mac):
        url = f"{self.base_url_api}/management/devices/{mac}/bootstrap-server"
        response = self.session.get(url)
        return response

    def get_device_cmd_detail_by_id(self, mac, cmd_id):
        url = f"{self.base_url_api}/management/devices/{mac}/commands/{cmd_id}"
        response = self.session.get(url)
        return response

    # The input dictionary "data" may contain keys:
    # name, companies, unclaimed, isOnline, groups, types,
    # tags, includeInstallations, inSync, search, sort,
    # sortOrder, page, pageSize
    def get_list_of_devices(self, data=None):
        url = f"{self.base_url_api}/management/devices"
        response = self.session.get(url, params=data)
        return response

    def get_devices_passw_by_mac(self, mac):
        url = f"{self.base_url_api}/management/devices/{mac}/retrieve-password"
        response = self.session.get(url)
        return response

    def post_cmd_to_trigger_bootstrap(self, mac):
        url = f"{self.base_url_api}/management/devices/{mac}/commands/trigger-bootstrap"
        response = self.session.post(url)
        return response

    def change_passw_by_mac(self, mac, user="root", password=None):
        url = f"{self.base_url_api}/management/devices/{mac}/commands/change-password?user={user}"
        if password:
            url += f"&password={password}"
        else:
            auto_gen_passw = self._generate_password(self)
            url += f"&password={auto_gen_passw}"
        response = self.session.post(url)
        return response

    def aborts_all_playbooks_by_mac(self, mac):
        url = f"{self.base_url_api}/management/devices/{mac}/abort-playbooks"
        response = self.session.post(url)
        return response

    def pins_unpins_app(self, mac, app_vers_id, is_pinned=False):
        url = f"{self.base_url_api}/management/devices/{mac}/apps/{app_vers_id}/pin"
        query = {'isPinned': is_pinned}
        response = self.session.put(url, params=query)
        return response

    def set_management_server(self, mac, server_id):
        url = f"{self.base_url_api}/management/devices/{mac}/bootstrap-server/{server_id}"
        response = self.session.put(url)
        return response

    def get_data_from_spec_device(self, q, epoch, statement_id, error, name, tags, columns, values):
        url = f"{self.base_url_api}/monitoring/devices/query?Q={q}&Epoch={epoch}"
        model = {
                "results": [
                    {
                    "statement_id": statement_id,
                    "error": error,
                    "series": [
                        {
                        "name": name,
                        "tags": tags,
                        "columns": [
                            columns
                        ],
                        "values": [
                            [
                            values
                            ]
                        ]
                        }
                    ]
                    }
                ]
                }
        response = self.session.get(url, json=model)
        return response

    def get_data_per_company(self, companies):
        url = f"{self.base_url_api}/monitoring/devices/dashboard-statistics"
        if companies:
            url += "?"
        for company in companies:
            url += f"Companies={company}&"
        response = self.session.get(url[:-1])
        return response

class ApiUsers:
    def __init__(self, base_url_api, session):
            self.session = session
            self.base_url_api = base_url_api

    def get_by_id(self, user_id):
        url = f"{self.base_url_api}/users/{user_id}"
        response = self.session.get(url)
        return response

    def delete_by_id(self, user_id):
        url = f"{self.base_url_api}/users/{user_id}"
        response = self.session.delete(url)
        return response

    def get_users(self, model: GetUserModel):
        url = f"{self.base_url_api}/users"
        query = RequestSession.make_query({
            'search': model.search,
            'name': model.name,
            'companies': model.companies,
            'email': model.email,
            'sort': model.sort,
            'sortOrder': model.sort_order
        })
        response = self.session.get(url, params=query)
        return response

    def create(self, model: CreateUserModel):
        url = f"{self.base_url_api}/users"
        response = self.session.post(url, json=model)
        return response

    def resend_user_confirmation_email_by_id(self, user_id):
        url = f"{self.base_url_api}/users/{user_id}/resend-confirmation-email"
        response = self.session.post(url)
        return response

    def update_user_profile_by_id(self, user_id, model: UpdateUserProfileModel):
        url = f"{self.base_url_api}/users/{user_id}/profile"
        response = self.session.put(url, json=model)
        return response

    def update_user_eula_confirmation(self, user_id):
        url = f"{self.base_url_api}/users/{user_id}/eula-confirmation"
        response = self.session.put(url)
        return response

    def update_user_company(self, user_id, model: UpdateUserCompanyModel):
        url = f"{self.base_url_api}/users/{user_id}/companies"
        response = self.session.put(url, json=model)
        return response

class ApiBootstrap:
    def __init__(self, base_url_api, session):
            self.session = session
            self.base_url_api = base_url_api

    def get_by_id(self, server_id):
        url = f"{self.base_url_api}/bootstrap/{server_id}"
        response = self.session.get(url)
        return response

    def update_bs_server(self, server_id, model: DetailManagementServerModel):
        url = f"{self.base_url_api}/bootstrap/{server_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_by_id(self, server_id):
        url = f"{self.base_url_api}/bootstrap/{server_id}"
        response = self.session.delete(url)
        return response

    def get_bs_servers(self, model: GetManagementServerModel):
        url = f"{self.base_url_api}/bootstrap"
        query = RequestSession.make_query({
            'Name': model.name,
            'Address': model.address
        })
        response = self.session.get(url, params=query)
        return response

    def create(self, model: CreateManagementServerModel):
        url = f"{self.base_url_api}/bootstrap"
        response = self.session.post(url, json=model)
        return response

    def get_certs(self):
        url = f"{self.base_url_api}/certs/"
        response = self.session.get(url)
        return response

    def update_certs(self, json):
        url = f"{self.base_url_api}/certs/"
        response = self.session.put(url, json=json)
        return response

#################################################
# Class for obtaining authorization token.
class ApiAuth:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_auth = base_url_api

    # Log into the the system and return authorization token.
    def authorize(self, username, password):
        url = f"{self.base_url_auth}/auth/connect/token"
        credentials = {'username': username,
                       'password': password,
                       'client_id': 'python',
                       'grant_type': 'password'}
        response = self.session.post(url, data=credentials)
        # How to get token from the response object:
        #      response.json()["access_token"]
        return response

#################################################
# This class can be used for sending API requests
# and parsing response.
class ApiConsumer:
    def __init__(self, base_url="https://gateway.dev.wadmp.com"):
        self.session          = RequestSession(base_url)   # For sending API requests.
        self.base_url_auth    = f"{base_url}/public" # Where authorization and weird stuff lies.
        self.base_url_api     = f"{base_url}/api"    # Where API lies.
        self.auth_token       = None                 # Will be configured when login() method is called.
        # SubAPIs:
        self._auth            = ApiAuth(self.base_url_auth, self.session)
        self.device           = ApiDevices(self.base_url_api, self.session)
        self.application      = ApiApplications(self.base_url_api, self.session)
        self.sync_engine      = ApiSyncEngine(self.base_url_api, self.session)
        self.company          = ApiCompanies(self.base_url_api, self.session)
        self.playbook         = ApiPlaybooks(self.base_url_api, self.session)
        self.family           = ApiFamilies(self.base_url_api, self.session)
        self.device_tag       = ApiDeviceTags(self.base_url_api, self.session)
        self.file             = ApiFiles(self.base_url_api, self.session)
        self.alert            = ApiAlerts(self.base_url_api, self.session)
        self.alert_endpoint   = ApiAlertEndpoints(self.base_url_api, self.session)
        self.alertable_field  = ApiAlertableFields(self.base_url_api, self.session)
        self.alert_history    = ApiAlertHistory(self.base_url_api, self.session)
        self.type             = ApiTypes(self.base_url_api, self.session)
        self.user             = ApiUsers(self.base_url_api, self.session)
        self.auditing         = ApiAuditing(self.base_url_api, self.session)
        self.device_group     = ApiDeviceGroups(self.base_url_api, self.session)
        self.billing          = ApiBilling(self.base_url_api, self.session)
        self.api_client       = ApiClients(self.base_url_api, self.session)
        self.bootstrap        = ApiBootstrap(self.base_url_api, self.session)

    def set_header(self, header):
        '''This method replaces header with the one given. It is used by tests
           that sign-in with multiple users and then switch between them for
           particular requests by replacing the header.'''
        self.session.clear_headers()
        self.session.set_header(header)

    def login(self, username, password):
        response = self._auth.authorize(username, password)
        if response.status_code != requests.codes['ok']:
            print(response.json())
            if response.status_code == 400: # Invalid username/password
                raise Exception("Invalid username or password.")
            elif response.status_code == 405: # 'Not Allowed'
                # Note: response.json() throws an exception when err code is 405. So we must not use it here.
                raise Exception(f"Invalid URL '{self._auth.base_url_auth}'.")
            else:
                raise Exception(f"Authorization failed: '{str(response.status_code)} -> {str(response.json())}")
        self.auth_token = response.json()["access_token"]
        self.session.headers.update({'Authorization': f'Bearer {self.auth_token}'})
        # The following sleep is a workaround from GEN2-2332. Its
        # purpose is to avoid Unauthorized error when using this
        # script against an installation running on a localhost.
        # There must always be some delay between creation of JWT
        # and usage of that token. We were unable to configure this
        # delay on the server side, so we are doing it here.
        time.sleep(0.5)