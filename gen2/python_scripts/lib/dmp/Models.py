import json

class PayloadBase(dict):
    def __init__(self, **kwargs):
        for arg, value in kwargs.items():
            self[arg] = value

    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def __setattr__(self, name, value):
        self[name] = value

    def json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

# models
class GetCompanyModel(PayloadBase):
    def __init__(self, name=None, sort=None, sort_order=None):
        super().__init__(
            name = name,
            sort = sort,
            sort_order = sort_order
        )

class CreateCompanyModel(PayloadBase):
    def __init__(self, name=None, parent_id=None, address=None, country_iso=None, contact_phone_number=None, contact_name=None, contact_email=None,
                 is2_fa_enabled=False, type='Trial'):
        super().__init__(
            name = name,
            parent_id = parent_id,
            address = address,
            country_iso = country_iso,
            contact_phone_number = contact_phone_number,
            contact_name = contact_name,
            contact_email = contact_email,
            is2_fa_enabled = is2_fa_enabled,
            type = type
        )

class UpdateCompanyModel(PayloadBase):
    def __init__(self, id=None, name=None, parent_id=None, address=None, country_iso=None, currency=None, contact_phone_number=None, contact_name=None,
                 contact_email=None, is2_fa_enabled=None, type=None):
        super().__init__(
            id = id,
            name = name,
            parent_id = parent_id,
            address = address,
            country_iso = country_iso,
            currency = currency,
            contact_phone_number = contact_phone_number,
            contact_name = contact_name,
            contact_email = contact_email,
            is2_fa_enabled = is2_fa_enabled,
            type = type
        )

class UpgradeCompanyPremium(PayloadBase):
    def __init__(self, erpid=None, orgid=None, crmid=None, billing_method=None, currency=None, po_number=None, order_type=None, distribution_channel=None,
                 division=None, part_number=None):
        super().__init__(
            erpid = erpid,
            orgid = orgid,
            crmid = crmid,
            billing_method = billing_method,
            currency = currency,
            po_number = po_number,
            order_type = order_type,
            distribution_channel = distribution_channel,
            division = division,
            part_number = part_number
        )

class DetailCompanyModel(PayloadBase):
    def __init__(self, id=None, name=None, parent_id=None, address=None, country_iso=None, currency=None, contact_phone_number=None,
                 contact_name=None, contact_email=None, is_trial=None, type=None):
        super().__init__(
            id = id,
            name = name,
            parent_id = parent_id,
            address = address,
            country_iso = country_iso,
            currency = currency,
            contact_phone_number = contact_phone_number,
            contact_name = contact_name,
            contact_email = contact_email,
            is_trial = is_trial,
            type = type
        )

class GetUserModel(PayloadBase):
    def __init__(self, search=None, name=None, companies=None, email=None, sort=None, sort_order=None):
        super().__init__(
            search = search,
            name = name,
            companies = companies,
            email = email,
            sort = sort,
            sort_order = sort_order,
        )

class DetailUserCompanyModel(PayloadBase):
    def __init__(self, company=None, permissions=None):
        super().__init__(
            company = company,
            permissions = permissions
        )


class UpdateUserCompanyModel(PayloadBase):
    def __init__(self, id=None, permissions=None, is_company_admin=False, is_service_account=False):
        super().__init__(
            id = id,
            permissions = permissions,
            is_company_admin = is_company_admin,
            is_service_account = is_service_account
        )


class UpdateUserCompaniesModel(PayloadBase):
    def __init__(self, id=None, companies=None):
        super().__init__(
            id = id,
            companies = companies
        )

class CreateUserModel(PayloadBase):
    def __init__(self, first_name=None, last_name=None, email=None, company_id=None, permissions=None):
        super().__init__(
            first_name = first_name,
            last_name = last_name,
            email = email,
            company_id = company_id,
            permissions = permissions
        )

class UpdateUserProfileModel(PayloadBase):
    def __init__(self, id=None, first_name=None, last_name=None):
        super().__init__(
            id = id,
            first_name = first_name,
            last_name = last_name
        )

class UserDetailsModel(PayloadBase):
    def __init__(self, first_name=None, last_name=None, user_id=None, email=None, company_id=None, name=None,
                 permissions=None, status=200):
        super().__init__(
            first_name = first_name,
            last_name = last_name,
            user_id = user_id,
            email = email,
            company_id = company_id,
            name = name,
            permissions = permissions,
            status = status
        )

    def fill(self, d):
        result = json.loads(json.dumps(d, default=str))
        self.first_name = result['first_name']
        self.last_name = result['last_name']
        self.user_id = result['id']
        self.email = result['email']
        self.company_id = result['companies'][0]['company']['id']
        self.name = result['companies'][0]['company']['name']
        self.permissions = result['companies'][0]['permissions']
        self.status = 200


class DetailUserModel(PayloadBase):
    def __init__(self, id=None, first_name=None, last_name=None, email=None, is_email_confirmed=None,
                 companies=None, creation_date=None):
        super().__init__(
            id = id,
            first_name = first_name,
            last_name = last_name,
            email = email,
            is_email_confirmed = is_email_confirmed,
            companies = companies,
            creation_date = creation_date
        )

class CreateApplicationModel(PayloadBase):
    def __init__(self, name=None, display_name=None, description=None,
                 is_global=True, is_firmware=False, is_aggregate=False, companies=None):
        super().__init__(
            name = name,
            display_name = display_name,
            description = description,
            is_global = is_global,
            is_firmware = is_firmware,
            is_aggregate = is_aggregate,
            companies = companies
        )

class DetailApplicationCompanyModel(PayloadBase):
    def __init__(self, id=None):
        super().__init__(
            id = id
        )

class UpdateApplicationModel(PayloadBase):
    def __init__(self, id=None, name=None, display_name=None, description=None,
                 is_global=True, is_firmware=False, is_aggregate=False, companies=None):
        super().__init__(
            id = id,
            name = name,
            display_name = display_name,
            description = description,
            is_global = is_global,
            is_firmware = is_firmware,
            is_aggregate = is_aggregate,
            companies = companies
        )

class CreateApplicationVersionModel(PayloadBase):
    def __init__(self, application_id=None, version=None):
        super().__init__(
            application_id = application_id,
            version = version
        )

class ApplicationVersionModel(PayloadBase):
    def __init__(self, application_id=None, application_version_id=None, version_id=None, state=None):
        super().__init__(
            application_id = application_id,
            application_version_id = application_version_id,
            version_id = version_id,
            state = state
        )

class UpdateApplicationVersionModel(PayloadBase):
    def __init__(self, application_id=None, version_id=None, version=None):
        super().__init__(
            application_id = application_id,
            version_id = version_id,
            version = version
        )

class CreateApplicationVersionSectionModel(PayloadBase):
    def __init__(self, version_id=None, name=None, display_name=None, config_type=None, metadata=None, model=None, ui_model=None, mergeable=True):
        super().__init__(
            version_id = version_id,
            name = name,
            display_name = display_name,
            config_type = config_type,
            metadata = metadata,
            model = model,
            ui_model = ui_model,
            mergeable = mergeable
        )

class CreateDeviceModel(PayloadBase):
    def __init__(self, serial_number=None, mac_address=None, device_type_id=None, order_code=None, imei=None):
        super().__init__(
            serial_number = serial_number,
            mac_address = mac_address,
            device_type_id = device_type_id,
            order_code = order_code,
            imei = imei
        )

class UpdateDeviceModel(PayloadBase):
    def __init__(self, mac_address=None, alias=None):
        super().__init__(
            mac_address = mac_address,
            alias = alias
        )

class ClaimDeviceModel(PayloadBase):
    def __init__(self, alias=None, serial_number=None, mac_address=None, imei=None, company_id=None):
        super().__init__(
            alias = alias,
            serial_number = serial_number,
            mac_address = mac_address,
            imei = imei,
            company_id = company_id
        )

class ReleaseDeviceModel(PayloadBase):
    def __init__(self, serial_number=None, mac_address=None, imei=None):
        super().__init__(
            serial_number = serial_number,
            mac_address = mac_address,
            imei = imei
        )

class GetManagementServerModel(PayloadBase):
    def __init__(self, name=None, address=None):
        super().__init__(
            name = name,
            address = address
        )

class CreateManagementServerModel(PayloadBase):
    def __init__(self, name=None, address=None, port=None, company_id=None):
        super().__init__(
            name = name,
            address = address,
            port = port,
            company_id = company_id
        )

class DetailManagementServerModel(PayloadBase):
    def __init__(self, id=None, name=None, address=None, port=None, company_id=None):
        super().__init__(
            id = id,
            name = name,
            address = address,
            port = port,
            company_id = company_id
        )

class TlsTerminationCertsModel(PayloadBase):
    def __init__(self, certs=None):
        super().__init__(
            certs = certs
        )

class WriteApplicationSettingsModel(PayloadBase):
    def __init__(self, section_id=None, set_config=None):
        super().__init__(
            section_id = section_id,
            set_config = set_config
        )

class CreateFamilyModel(PayloadBase):
    def __init__(self, name=None, description=None):
        super().__init__(
            name = name,
            description = description
        )

class CreateTypeModel(PayloadBase):
    def __init__(self, name=None, description=None):
        super().__init__(
            name = name,
            description = description
        )

class DetailApplicationModel(PayloadBase):
    def __init__(self, id=None, name=None, display_name=None, version=None,
                 is_global=True, companies=None, parent_id=None, sections=None):
        super().__init__(
            id = id,
            name = name,
            display_name = display_name,
            version = version,
            is_global = is_global,
            companies = companies,
            parent_id = parent_id,
            sections = sections
        )

class DetailApplicationSectionModel(PayloadBase):
    def __init__(self, id=None, application_id=None, application_version_id=None, name=None, display_name=None,
                 config_type=None, metadata=None, model=None, ui_model=None):
        super().__init__(
            id = id,
            application_id = application_id,
            application_version_id = application_version_id,
            name = name,
            display_name = display_name,
            config_type = config_type,
            metadata = metadata,
            model = model,
            ui_model = ui_model
        )

class UpdateApplicationVersionSectionModel(PayloadBase):
    def __init__(self, section_id=None, version_id=None, name=None, display_name=None,
                 config_type=None, metadata=None, model=None, ui_model=None, mergeable=True):
        super().__init__(
            section_id = section_id,
            version_id = version_id,
            name = name,
            display_name = display_name,
            config_type = config_type,
            metadata = metadata,
            model = model,
            ui_model = ui_model,
            mergeable = mergeable
        )

class CreateAPIClientModel(PayloadBase):
    def __init__(self, name=None, grant_type=None, redirect_uri=None, post_logout_redirect_uri=None, allowed_cors_uri=None, company_id=None):
        super().__init__(
            name = name,
            grant_type = grant_type,
            redirect_uri = redirect_uri,
            post_logout_redirect_uri = post_logout_redirect_uri,
            allowed_cors_uri = allowed_cors_uri,
            company_id = company_id
        )

class UpdateAPIClientModel(PayloadBase):
    def __init__(self, id= None, name=None, grant_type=None, redirect_uri=None, post_logout_redirect_uri=None,
                 allowed_cors_uri=None, company_id=None):
        super().__init__(
            id = id,
            name = name,
            grant_type = grant_type,
            redirect_uri = redirect_uri,
            post_logout_redirect_uri = post_logout_redirect_uri,
            allowed_cors_uri = allowed_cors_uri,
            company_id = company_id
        )

class CreateDeviceGroupModel(PayloadBase):
    def __init__(self, name=None, company_id=None):
        super().__init__(
            name = name,
            company_id = company_id
        )

class UpdateDeviceGroupModel(PayloadBase):
    def __init__(self, id=None, name=None, company_id=None):
        super().__init__(
            id = id,
            name = name,
            company_id = company_id
        )

class CreateDeviceTagModel(PayloadBase):
    def __init__(self, name=None, company_id=None):
        super().__init__(
            name = name,
            company_id = company_id
        )

class UpdateDeviceTagModel(PayloadBase):
    def __init__(self, id=None, name=None, company_id=None):
        super().__init__(
            id = id,
            name = name,
            company_id = company_id
        )

class DetailSyncEngineConfigModel(PayloadBase):
    def __init__(self, sync_type=None, retry_interval=None, retry_attempts=None, company_id=None):
        super().__init__(
            sync_type = sync_type,
            retry_interval = retry_interval,
            retry_attempts = retry_attempts,
            company_id = company_id
        )

class CreateSettingsGroupModel(PayloadBase):
    def __init__(self, name=None, application_version_id=None, company_id=None,
                 ignore_devices_with_different_versions=True, sections=None):
        super().__init__(
            name = name,
            application_version_id = application_version_id,
            company_id = company_id,
            ignore_devices_with_different_versions = ignore_devices_with_different_versions,
            sections = sections
        )

class UpdateSettingsGroupModel(PayloadBase):
    def __init__(self, id=None, name=None, application_version_id=None, company_id=None,
                 ignore_devices_with_different_versions=True, sections=None):
        super().__init__(
            id = id,
            name = name,
            application_version_id = application_version_id,
            company_id = company_id,
            ignore_devices_with_different_versions = ignore_devices_with_different_versions,
            sections = sections
        )

class CreatePlaybookModel(PayloadBase):
    def __init__(self, name=None, company_id=None, device_type_id=None, actions=None, devices=None):
        super().__init__(
            name = name,
            company_id = company_id,
            device_type_id = device_type_id,
            actions = actions,
            devices = devices
        )

class CreatePlaybookActionModel(PayloadBase):
    def __init__(self, type=None, settings_group_id=None, install_application_version_id=None,
                 application_version_section_id=None, individual_setting_key=None, order=None, device_settings=None):
        super().__init__(
            type = type,
            settings_group_id = settings_group_id,
            install_application_version_id = install_application_version_id,
            application_version_section_id = application_version_section_id,
            individual_setting_key = individual_setting_key,
            order = order,
            device_settings = device_settings
        )

class CreatePlaybookActionDeviceSettingModel(PayloadBase):
    def __init__(self, device=None, setting_value=None):
        super().__init__(
            device = device,
            setting_value = setting_value
        )

class CreateLicenseKeyModel(PayloadBase):
    def __init__(self, expiration_date=None, devices_allowed=None, default_user_permissions=None):
        super().__init__(
            expiration_date = expiration_date,
            devices_allowed = devices_allowed,
            default_user_permissions = default_user_permissions
        )

class UpdateLicenseKeyModel(PayloadBase):
    def __init__(self, key=None, expiration_date=None, devices_allowed=None, default_user_permissions=None):
        super().__init__(
            key = key,
            expiration_date = expiration_date,
            devices_allowed = devices_allowed,
            default_user_permissions = default_user_permissions
        )

class ScheduledBillingJobModel(PayloadBase):
    def __init__(self, start_date=None, end_date=None, schedule_next=False):
        super().__init__(
            start_date = start_date,
            end_date = end_date,
            schedule_next = schedule_next
        )

class GenerateSapInvoiceModel(PayloadBase):
    def __init__(self, start_date=None, end_date=None, company_id=None):
        super().__init__(
            start_date = start_date,
            end_date = end_date,
            company_id = company_id
        )

class GenerateInvoiceTestModel(PayloadBase):
    def __init__(self, start_date=None, end_date=None, rate_per_device_period=None, company_id=None, rate_unit=None):
        super().__init__(
            start_date = start_date,
            end_date = end_date,
            rate_per_device_period = rate_per_device_period,
            company_id = company_id,
            rate_unit = rate_unit
        )

class GetEndpointModel(PayloadBase):
    def __init__(self, page=None, pagesize=None, name=None, company_ids=None):
        super().__init__(
                page=page,
                pagesize=pagesize,
                name=name,
                company_ids=company_ids
        )

class CreateAlertEndpointModel(PayloadBase):
    def __init__(self, name=None, endpoint_type=None, recipient=None,company_id=None, is_enabled=None):
        super().__init__(
            name = name,
            endpoint_type = endpoint_type,
            recipient = recipient,
            company_id = company_id,
            is_enabled = is_enabled
        )

class UpdateAlertEndpointModel(PayloadBase):
    def __init__(self, id=None, name=None, endpoint_type=None, recipient=None,company_id=None, is_enabled=None):
        super().__init__(
            id = id,
            name = name,
            endpoint_type = endpoint_type,
            recipient = recipient,
            company_id = company_id,
            is_enabled = is_enabled
        )

class GetAlertModel(PayloadBase):
    def __init__(self, page=None, pagesize=None, name=None, company_ids=None,
                   device_mac_addresses=None, sort=None, sort_order=None):
        super().__init__(
                page=page,
                pagesize=pagesize,
                name=name,
                company_ids=company_ids,
                device_mac_addresses=device_mac_addresses,
                sort=sort,
                sort_order=sort_order
        )

class GetAlertableFieldsModel(PayloadBase):
    def __init__(self, page=None, pagesize=None, name=None, is_device_oriented=None,
                   source_type=None, data_type=None):
        super().__init__(
                page=page,
                pagesize=pagesize,
                name=name,
                is_device_oriented=is_device_oriented,
                source_type=source_type,
                data_type=data_type
        )

class GetAlertHistoryModel(PayloadBase):
    def __init__(self, page=None, pagesize=None, is_acked=None, alert_ids=None, company_ids=None, device_ids=None):
        super().__init__(
                page=page,
                pagesize=pagesize,
                is_acked=is_acked,
                alert_ids=alert_ids,
                company_ids=company_ids,
                device_ids=device_ids
        )

class CreateAlertModel(PayloadBase):
    def __init__(self, name=None, company_id = None, mac_address = None, is_enabled = None, cooldown = None, period = None,
                 critical_device_count = None, critical_repetitions = None, alertable_field_id = None, alert_operator_id = None,
                 operand1=None, operand2=None):
        super().__init__(
            name = name,
            company_id = company_id,
            mac_address = mac_address,
            is_enabled = is_enabled,
            cooldown = cooldown,
            period = period,
            critical_device_count = critical_device_count,
            critical_repetitions = critical_repetitions,
            alertable_field_id = alertable_field_id,
            alert_operator_id = alert_operator_id,
            operand1 = operand1,
            operand2 = operand2
        )

class UpdateAlertModel(PayloadBase):
    def __init__(self, id=None, name=None, company_id = None, mac_address = None, is_enabled = None, cooldown = None, period = None,
                 critical_device_count = None, critical_repetitions = None, alertable_field_id = None, alert_operator_id = None,
                 operand1=None, operand2=None):
        super().__init__(
            id = id,
            name = name,
            company_id = company_id,
            mac_address = mac_address,
            is_enabled = is_enabled,
            cooldown = cooldown,
            period = period,
            critical_device_count = critical_device_count,
            critical_repetitions = critical_repetitions,
            alertable_field_id = alertable_field_id,
            alert_operator_id = alert_operator_id,
            operand1 = operand1,
            operand2 = operand2
        )

class DetailFileModel(PayloadBase):
    def __init__(self, id=None, file_name=None, file_size=None, url=None):
        super().__init__(
            id = id,
            file_name = file_name,
            file_size = file_size,
            url = url
        )

class DeviceModel:
    def __init__(self, data):
        apps = data['applications']
        self.type           = data['device_type']
        self.mac            = data['mac_address']
        self.imei           = data['imei']
        self.alias          = data['alias']

        self.is_synced      = data['in_sync']
        self.is_online      = data['is_online']
        self.is_playbook_running = data['is_playbook_running']

        self.firmware       = list(filter(lambda x: x['application_version']['application']['is_firmware'] == True and\
                                                    x['state'] == "Installed", apps))
        self.firmware       = self.firmware[0] if self.firmware else []
        self.pending_fw     = list(filter(lambda x: x['application_version']['application']['is_firmware'] == True and\
                                                    x['state'] != "Installed", apps))
        self.pending_fw     = self.pending_fw[0] if self.pending_fw else []
        self.apps_installed = list(filter(lambda x: x['application_version']['application']['is_firmware'] == False and \
                                                    x['application_version']['application']['name'] != 'wadmp_client' and \
                                                    x['state'] == "Installed", apps))
        self.apps_pending   = list(filter(lambda x: x['application_version']['application']['is_firmware'] == False and \
                                                    x['application_version']['application']['name'] != 'wadmp_client' and \
                                                    x['state'] != "Installed", apps))
        self.wadmp_app      = list(filter(lambda x: x['application_version']['application']['is_firmware'] == False and \
                                                    x['application_version']['application']['name'] == 'wadmp_client', apps))
        self.wadmp_app      = self.wadmp_app[0] if self.wadmp_app else []

"""Web-objects"""

class LoginObj(dict):
    def __init__(self, email, pwd):
        self['email'] = email
        self['pwd'] = pwd

class LoginErrorObj(dict):
    def __init__(self, email_error=None, pwd_error=None):
        self['email_error'] = email_error
        self['pwd_error'] = pwd_error

class LoginSignUpObj(dict):
    def __init__(self, first_name, surname, email, pwd, confirm_pwd):
        self['first_name'] = first_name
        self['surname'] = surname
        self['email'] = email
        self['pwd'] = pwd
        self['confirm_pwd'] = confirm_pwd

class LoginSignUpErrorObj(dict):
    def __init__(self, first_name_error=None, surname_error=None, email_error=None, pwd_error=None, confirm_pwd_error=None):
        self['first_name_error'] = first_name_error
        self['surname_error'] = surname_error
        self['email_error'] = email_error
        self['pwd_error'] = pwd_error
        self['confirm_pwd_error'] = confirm_pwd_error

class LoginSignUpCompanyObj(dict):
    def __init__(self, company_name, company_address, country, contact_number):
        self['company_name'] = company_name
        self['company_address'] = company_address
        self['country'] = country
        self['contact_number'] = contact_number

class LoginSignUpCompanyErrorObj(dict):
    def __init__(self, company_name_error=None, company_address_error=None, country_error=None, contact_number_error=None):
        self['company_name_error'] = company_name_error
        self['company_address_error'] = company_address_error
        self['country_error'] = country_error
        self['contact_number_error'] = contact_number_error

class LoginSignUpPrimaryObj(dict):
    def __init__(self, company_contact_name, company_contact_email):
        self['company_contact_name'] = company_contact_name
        self['company_contact_email'] = company_contact_email

class LoginSignUpPrimaryErrorObj(dict):
    def __init__(self, company_contact_name_error=None, company_contact_email_error=None):
        self['company_contact_name_error'] = company_contact_name_error
        self['company_contact_email_error'] = company_contact_email_error

class SetPasswordObj(dict):
    def __init__(self, pwd, confirm_pwd):
        self['pwd'] = pwd
        self['confirm_pwd'] = confirm_pwd

class SetPasswordErrorObj(dict):
    def __init__(self, pwd_error=None, confirm_pwd_error=None):
        self['pwd_error'] = pwd_error
        self['confirm_pwd_error'] = confirm_pwd_error

class ChangePasswordObj(dict):
    def __init__(self, current_pwd, new_pwd, confirm_pwd):
        self['current_pwd'] = current_pwd
        self['new_pwd'] = new_pwd
        self['confirm_pwd'] = confirm_pwd

class ChangePasswordErrorObj(dict):
    def __init__(self,  current_pwd_error=None, new_pwd_error=None, confirm_pwd_error=None):
        self['current_pwd_error'] = current_pwd_error
        self['new_pwd_error'] = new_pwd_error
        self['confirm_pwd_error'] = confirm_pwd_error