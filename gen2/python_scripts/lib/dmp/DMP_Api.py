import requests
import time
import re

from .ApiConsumer import ApiConsumer
from .Models      import DeviceModel, GetCompanyModel

######################################################
# This is a high level API built on top of APIConsumer
# API Consumer. It adds methods that combine multiple
# API calls into achieving a goal.
class DMP_Api(ApiConsumer):

    def get_apps_by_mac(self, mac):
        """ Return dict of all apps installed on device.

        :param mac: Mac address of device we work with.
        :return: Name of app and version.
        :rtype: dict
        
        """
        resp = self.device.get_apps(mac)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f'Cannot get apps: {resp.json()["message"]}')        
        apps_dict = {}
        resp = resp.json()["data"]
        for app in resp:
            name = app['application_version']['application']['name']
            apps_dict[name] = app['application_version']['version']            
        if not apps_dict:
            raise Exception(f"Applications for device: {mac} was not found.")             
        return apps_dict

    def get_pinned_apps_by_mac(self, mac):
        """ Return dict of all apps installed and pinned on device.

        :param mac: Mac address of device we work with.
        :return: Name of app and version.
        :rtype: dict
        
        """
        resp = self.device.get_apps(mac)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f'Cannot get apps: {resp.json()["message"]}')        
        apps_dict = {}
        resp = resp.json()["data"]
        for app in resp:
            name = app['application_version']['application']['name']
            if app['is_pinned'] == True:
                apps_dict[name] = app['application_version']['version']
        if not apps_dict:
            raise Exception(f"Applications for device: {mac} was not found.")             
        return apps_dict    

    #########################################################
    # Get device type id by mac.
    def get_device_type_id(self, mac):
        resp = self.device.get_by_mac(mac)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f'Cannot get device: {resp.json()["message"]}')        
        device_type_id = resp.json()["data"]["device_type"]["type_id"]
        if not device_type_id:
            raise Exception(f"Device with mac: {mac} was not found.")        
        return [device_type_id]

    #########################################################
    # Returns the dict of app names and latest app version 
    # available for device except wadmp_client and FW.
    def get_apps_by_device_type_id(self, device_type_id):
        resp = self.application.get_apps(device_type_ids=device_type_id)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f'Cannot get apps: {resp.json()["message"]}')        
        dict_of_apps = {}
        for i in resp.json()["data"]:
            if 'wadmp_client' in i["name"] or 'SPECTRE-v3-LTE' in i["name"]:
                continue
            else:
                dict_of_apps.update( {i["name"] : i["versions"][-1]["version"]} )                
        if not dict_of_apps:
            raise Exception(f"Applications for device: {device_type_id} was not found.")             
        return dict_of_apps          

    #####################################################
    # Returns application as json.
    def get_app_by_name(self, app_name):
        resp = self.application.get_apps()
        if resp.status_code != requests.codes['ok']:
            raise Exception(f'Cannot get applications: {resp.json()["message"]}')
        apps = resp.json()['data']
        result = list(filter(lambda x: x['name'] == app_name, apps))
        if not result:
            raise Exception(f"Application {app_name} was not found.")
        return result[0]

    #####################################################
    # Returns company as json.
    def get_company_by_name(self, company_name):
        resp = self.company.get_companies(GetCompanyModel())
        if resp.status_code != requests.codes['ok']:
            raise Exception(f'Cannot get companies: {resp}')
        companies = resp.json()['data']
        result = list(filter(lambda x: x['name'] == company_name, companies))
        if not result:
            raise Exception(f"Company {company_name} was not found.")
        return result[0]

    #####################################################
    # Returns information about the given device parsed
    # into an easy to use object. 
    def get_device_datamodel(self, mac):
        resp = self.device.get_by_mac(mac)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f"Couldn't get device [{mac}]: {str(resp.json())}")
        return DeviceModel(resp.json()['data'])

    #####################################################
    # Returns version_id of the currently installed 
    # wadmp_client.
    def get_client_version_id_current(self, mac):
        device = self.get_device_datamodel(mac)
        return device.wadmp_app['application_version']['id']

    #####################################################
    # Returns version of the currently installed 
    # wadmp_client.
    def get_client_version_current(self, mac):
        device = self.get_device_datamodel(mac)
        return device.wadmp_app['application_version']['version']

    #####################################################
    # Example input: 
    # app_name='wadmp_client', app_version_str='2.1.1'
    def get_appversion_id(self, app_name, app_version_str):
        resp = self.application.get_apps(app_name)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f"Couldn't get applications: {str(resp.json())}")
        if not resp.json()['data']: #
            raise Exception(f"Application {app_name} not found.")
        correct_app = []
        for data in resp.json()['data']:
            if data['name'] == app_name:
                correct_app.append(data)
        for version in correct_app[0]['versions']:
            if version['version'] == app_version_str:
                return version['id']
        raise Exception(f"Version {app_version_str} for app {app_name} was not found.")

    #####################################################
    # Returns currently installed firmware of a device.
    def get_firmware_by_mac(self, mac):
        return self.get_device_datamodel(mac).firmware

    #####################################################
    # Returns currently installed firmware of a device.
    def get_firmware_version_id_by_mac(self, mac):
        return self.get_device_datamodel(mac).firmware['application_version']['id']

    #####################################################
    # Returns currently installed firmware of a device.
    def get_firmware_version_by_mac(self, mac):
        return self.get_device_datamodel(mac).firmware['application_version']['version']

    #####################################################
    # Retrieves last X commands that were created for
    # the given device.
    def get_last_x_commands_created(self, mac, count):
        resp = self.device.get_commands(mac)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f"Couldn't get commands: {str(resp.json())}")
        return resp.json()['data'][:count]

    #####################################################
    # Returns company ID.
    def get_company_id_by_name(self, company_name):
        company = self.get_company_by_name(company_name) # Raises exception if company does not exist.
        return company['id']

    #####################################################
    # Returns value of the given setting.
    def get_device_setting(self, mac, app_version_id, setting_name):
        resp = self.device.get_settings(mac, app_version_id)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f"Getting settings of device {mac} failed [response code: {resp.status_code}]")
        for section in resp.json()['data']:
            m = re.search(f'{setting_name}=(.+?)\n', section['reported_configuration'])
            if m:
                return m.group(1)
        raise Exception(f'Setting {setting_name} was not found.')

    #########################################################
    # Returns section_id where the given setting belongs.
    def get_section_id_by_settingname(self, mac, app_version_id, setting_name):
        section = self.get_section_by_settingname(mac, app_version_id, setting_name)
        return section['section_id']

    #########################################################
    # Returns settings section where the given setting belongs.
    def get_section_by_settingname(self, mac, app_version_id, setting_name):
        resp = self.device.get_settings(mac, app_version_id)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f"Getting settings of device {mac} failed [response code: {resp.status_code}]")
        for section in resp.json()['data']:
            if setting_name+'=' in section['reported_configuration']:
                return section
        raise Exception(f'Section not found for {setting_name}.')

    #####################################################
    # 
    def get_installation_result(self, mac, app_version_id):
        resp = self.device.get_by_mac(mac)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f'Cannot get device details for {mac}: {resp}')
        apps = list(filter(lambda x: x['application_version']['id'] == app_version_id, resp.json()['data']['applications']))
        if not apps:
            raise Exception(f'App_version_id {app_version_id} for device {mac} was not found.')
        return apps[0]['result']

    #####################################################
    # Returns a list of IDs of playbooks whose state 
    # matches one of those in 'states' and are related 
    # to the given device.
    # Valid states = ['Draft', 'Scheduled', 'Running', 'Completed', 
    #                'Failed', 'Incomplete', 'Aborted', 'CompletedWithFailure', 
    #                'Deleted']
    def get_playbooks_by_device(self, mac, states):
        resp = self.playbook.get_playbooks(states) # This gets all playbooks
        if resp.status_code != requests.codes['ok']:
            raise Exception(f'Cannot get playbooks: {resp}')
        # We want only playbooks that the given Device is part of.
        result_ids = []
        result_playbooks = []
        for playbook in resp.json()['data']:
            id = playbook['id']
            resp = self.playbook.get_playbook_by_id(id)
            if resp.status_code != requests.codes['ok']:
                raise Exception(f'Cannot get playbook for device {mac}: {resp}')
            for device in resp.json()['data']['devices']:
                if mac == device['device']['mac_address'] and id not in result_ids:
                    result_ids.append(id)
                    result_playbooks.append(playbook)
        return result_playbooks

    #####################################################
    # Returns ID of the device tag that has the given 
    # name. If such tag does not exist, empty list is 
    # returned.
    def get_tag_id_by_name(self, tag_name): 
        resp = self.devicetags.get_tags(tag_name)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f'Cannot get device tags: {resp}')
        for tag in resp.json()['data']:
            if tag['name'] == tag_name:
                return tag['id']
        return None

    #####################################################
    # Returns name of the firmware that would match the
    # given device (firmwares for different device types
    # have different names)
    def get_fw_name_by_mac(self, mac):
        d = self.get_device_datamodel(mac)
        return d.firmware['application_version']['application']['name']

    #####################################################
    # Change value of a single setting.
    def set_device_setting(self, mac, app_version_id, setting_name, value):
        section = self.get_section_by_settingname(mac, app_version_id, setting_name)
        # Remove the given setting from the section
        set_config = re.sub(f'{setting_name}=.*?\n', '', section['desired_configuration'])
        # Add the setting into the section again with the new value
        set_config += f'{setting_name}={value}\n'
        resp = self.device.update_settings_section(mac, app_version_id, section['section_id'], set_config)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f"Updating settings of device {mac} failed [response code: {resp.status_code}]")

    #####################################################
    # Makes the given setting un-managed.
    def unset_device_setting(self, mac, app_version_id, setting_name):
        section = self.get_section_by_settingname(mac, app_version_id, setting_name)
        # Remove the given setting from the section
        edited_section = re.sub(f'{setting_name}=.*?\n', '', section['desired_configuration'])
        # Upload the edited section
        resp = self.device.update_settings_section(mac, app_version_id, section['section_id'], edited_section)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f"Updating settings of device {mac} failed [response code: {resp.status_code}]")

    #####################################################
    # Makes all settings of the device that are related 
    # to the given appversion un-managed.
    def unset_device_settings_by_appversion(self, mac, app_version_id):
        resp = self.device.get_settings(mac, app_version_id)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f"Getting settings of device {mac} failed [response code: {resp.status_code}]")
        sections = []
        for section in resp.json()['data']:
            sections.append({'section_id': section['section_id'], 'set_config': ''})
        # Upload the edited sections
        resp = self.device.update_settings(mac, app_version_id, data=sections)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f"Updating settings of device {mac} failed [response code: {resp.status_code}]")

    #####################################################
    # Makes ALL settings of the device un-managed.
    def unset_device_settings_all(self, mac):
        d = self.get_device_datamodel(mac)
        for app in d.apps_installed:
            self.unset_device_settings_by_appversion(mac, app['application_version']['id'])
        self.unset_device_settings_by_appversion(mac, d.firmware['application_version']['id'])
        
    #####################################################
    # Waits untill 'result' field of the application 
    # installation gets into one of the states in
    # 'end_when' list.
    def wait_installation_result(self, mac, app_version_id, end_when=['Installed'], timeout=120, interval=2):
        time_spent = 0
        while not self.get_installation_result(mac, app_version_id) in end_when:
            time.sleep(interval)
            time_spent += interval
            if time_spent > timeout:
                raise Exception(f"Timeout reached while waiting for {app_version_id} installation on {mac}.")

    #####################################################
    # Waits at most 'timeout' seconds until the function
    # passed as 'is_ready_f' starts returning true for at 
    # least 'persist' seconds. 'interval' controls how often
    # the timeout check and persist check should be done
    # (check once every 'interval' seconds).
    def _wait_device(self, is_ready_f, mac, timeout=99999, interval=2, persist=0, err_str='Timeout reached.'):
        time_spent = 0
        sec_in_final_state = 0
        prev_ready = False
        while time_spent < timeout:
            if is_ready_f(mac):
                if prev_ready:
                    sec_in_final_state += interval
                if sec_in_final_state >= persist:
                    return
                prev_ready = True
            else:
                sec_in_final_state = 0
                prev_ready = False
            time.sleep(interval)
            time_spent += interval      
        raise Exception(err_str)

    #####################################################
    # Waits at most 'timeout' seconds until the given
    # device becomes online and stays online for at least
    # 'persist' seconds. 'interval' controls how often
    # the timeout check and persist check should be done
    # (check once every 'interval' seconds).
    def wait_device_online(self, mac, timeout=99999, interval=2, persist=0):
        # We define lambda function that will be used in _wait_device() method
        # to check whether it should continue waiting or not.
        f = lambda dev_mac: self.is_device_online(dev_mac)
        err_str = f"Timeout reached while waiting for device {mac} to become online."

        self._wait_device(f, mac, timeout, interval, persist, err_str)

    #####################################################
    # Waits at most 'timeout' seconds until the given
    # device becomes synced and stays synced for at least
    # 'persist' seconds. 'interval' controls how often
    # the timeout check and persist check should be done
    # (check once every 'interval' seconds).
    def wait_device_synced(self, mac, timeout=99999, interval=2, persist=0):
        # We define lambda function that will be used in _wait_device() method
        # to check whether it should continue waiting or not.
        f = lambda dev_mac: self.is_device_synced(dev_mac)
        err_str = f"Timeout reached while waiting for device {mac} to become synced."

        self._wait_device(f, mac, timeout, interval, persist, err_str)

    #####################################################
    # Waits at most 'timeout' seconds until the given
    # device becomes online+synced and stays in that state
    # for 'persist' seconds. 'interval' controls how often
    # the timeout check and persist check should be done
    # (check once every 'interval' seconds).
    def wait_device_ready(self, mac, timeout=99999, interval=2, persist=0):   
        # We define lambda function that will be used in _wait_device() method
        # to check whether it should continue waiting or not.
        f = lambda dev_mac: self.is_device_synced(dev_mac) and self.is_device_online(dev_mac)
        err_str = f"Timeout reached while waiting for device {mac} to become online & synced."

        self._wait_device(f, mac, timeout, interval, persist, err_str)
        
    #####################################################
    # Installs the given version of DMP Client into the
    # router. Version_str = '2.1.1' or '2.0.10', etc.
    def install_client(self, mac, version_str):
        app_version_id = self.get_appversion_id('wadmp_client', version_str)
        resp = self.device.install_app(mac, app_version_id)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f"Couldn't install app_version_id={app_version_id} to {mac}: {str(resp.json())}")

    #####################################################
    # Returns true if the given device is online.
    def is_device_online(self, mac):
        resp = self.device.get_by_mac(mac)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f'Cannot get device details for {mac}: {resp}')
        return resp.json()['data']['is_online']

    #####################################################
    # Returns true if the given device is online.
    def is_device_synced(self, mac):
        resp = self.device.get_by_mac(mac)
        if resp.status_code != requests.codes['ok']:
            raise Exception(f'Cannot get device details for {mac}: {resp}')
        return resp.json()['data']['in_sync']

    #####################################################
    # Returns true if device exists. False otherwise.
    def device_exists(self, mac):
        resp = self.device.get_by_mac(mac)
        if resp.status_code == requests.codes['ok']:
            return True
        if resp.status_code == 404:
            return False
        raise Exception(f'Cannot get device information for {mac}: {resp}')

    #####################################################
    # Return true if the given device has reported its 
    # configuration at least once in its lifetime.
    # TODO: remove app_version_id parameter (this method 
    # should find version ID on its own).
    def has_reported_configuration(self, mac, app_version_id):
        sections = self.device.get_sections(mac, app_version_id)
        if sections.status_code != requests.codes['ok']:
            raise Exception(f'Failed to get settings sections: {sections.json()}')

        return sections.json()['data'] and \
            'reported_configuration' in sections.json()['data'][0]