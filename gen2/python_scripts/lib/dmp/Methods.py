import re
import csv

#########################################
# Prints and exits
def die(msg):
    print(msg)
    exit(1)

#########################################################
# Looks for column named <col_name> and returns list 
# of values from this column.
def csv_get_column(csv_file, col_name, delimiter=';'):
    with open(csv_file, encoding="UTF-8", newline="") as csvfile:
        csvreader = csv.reader(csvfile, delimiter=delimiter)
        index = None
        result = []
        for row in csvreader:
            if index == None:
                try:
                    index = row.index(col_name)
                except Exception:
                    pass
                continue
            result.append(row[index])
        return result

#########################################################
# Returns true if the given mac is valid MAC address. 
# Otherwise returns false.
def is_mac_address(mac):
    return mac and re.match("[0-9a-f]{2}([-:]?)[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", mac.lower())

#########################################################
# Return base_url for the given cloud environment.
# 'cloud_env' can be: 'dev', 'prod', 'elenia', 'local'
def get_base_url_by_cloud(cloud_env):
    if cloud_env == "dev":
        return "https://gateway.dev.wadmp.com"
    elif cloud_env == "prod":
        return "https://gateway.wadmp.com"
    elif cloud_env == 'elenia':
        return "https://gateway.wadmp.smartgrid.fi"
    elif cloud_env == 'local':
        return "http://localhost:8082"
    else:
        return None 

#########################################################
# Returns id of version that is lower than the one that was 
# given.
def get_lower_version(api, name, version):
    lower_version_id = get_list_of_lower_versions(version)
    version_id_new = get_app_version_id(api, name, lower_version_id)
    return version_id_new

# Returns a list of versions that are lower 
# than the one that was given.
def get_list_of_lower_versions(version):
    req_major, req_minor, req_build = version.split('.')
    versions = []    
    minor = int(req_minor)
    build = int(req_build)
    req_build_new = int(req_build)
    for i in range(10):
      if minor > 0:
        if build > 0:
            versions.append(f'{req_major}.{req_minor}.{req_build_new-1}')
        else:
            versions.append(f'{req_major}.{int(req_minor)-1}.{(req_build_new+10)-1}')
      elif minor >= 0 and build > 0:
            versions.append(f'{req_major}.{req_minor}.{req_build_new-1}')
      else:
        exit
      build -= 1
      req_build_new -= 1    
    return versions 

# This will go through the list of versions and 
# select (latest) first one valid version.
def get_app_version_id(api, name, versions):
    version_id_new = ""
    for i in versions:
        try:
            version_id_new = api.get_appversion_id(name, i)
        finally:
            if isinstance(version_id_new, int):
                return version_id_new

#########################################################
# Returns true if the device has currently installed 
# higher version of wadmp client than 'version'. 
# 'Version' is in format e.g. '2.0.10'. 
# 'Device' is initialized object of 'DeviceModel' class.
def has_wadmp_client_atleast(device, version):
    if not device.wadmp_app:
        return False
    major, minor, build = device.wadmp_app['application_version']['version'].split('.')
    req_major, req_minor, req_build = version.split('.')
    if int(major) > int(req_major):
        return True
    if int(major) < int(req_major):
        return False
    if int(minor) > int(req_minor):
        return True
    if int(minor) < int(req_minor):
        return False
    if int(build) > int(req_build):
        return True
    if int(build) < int(req_build):
        return False
    return True # The version is identical