from dmp.DMP_Api     import DMP_Api
from dmp.Methods     import die
import requests
import sys

base_url = 'http://gateway.wadmp.com'
username = 'editme'
password = 'editme'

if len(sys.argv) < 2:
    die(f'Missing/Invalid arguments. Correct usage: {sys.argv[0]} <mac>')

mac = sys.argv[1]

api = DMP_Api(base_url)
api.login(username, password)

d = api.get_device_datamodel(mac)

print('*** Individual apps: ***\n')
sync_array = []
running_installations_array = []
failed_installations_array = []

r = api.device.get_by_mac(mac)
if r.status_code != requests.codes['ok']:
    raise Exception(f"Couldn't get device [{mac}]: {str(r.json())}")

for app in r.json()['data']['applications']:
    resp = api.device.get_settings(mac, app['application_version']['id'])
    if resp.status_code != requests.codes['ok']:
        raise Exception(f'Could not get setting sections: {resp}')
    
    print(f'Appversion_id: {app["application_version"]["id"]}')
    print(f'Name: {app["application_version"]["application"]["name"]}')
    print(f'Version: {app["application_version"]["version"]}')
    print(f'State: {app["state"]}')
    print(f'Result: {app["result"]}')
    print(f'Is Running: {app["is_running"]}')

    for section in resp.json()['data']:
        # Print the reported/desired settings only if the given section is out of sync.
        if not section['in_sync']:
            print(f'{section}')
        sync_array.append(section['in_sync'])

    if app["is_running"]:
        running_installations_array.append(f'{app["application_version"]["application"]["name"]} {app["application_version"]["version"]}')
    if app["has_failed"]:
        failed_installations_array.append(f'{app["application_version"]["application"]["name"]} {app["application_version"]["version"]}')
    print() # Add empty line

cmds =  api.get_last_x_commands_created(mac, 4)
response_codes_array = []
for cmd in cmds:
    print(f'"id": {cmd["id"]}')
    print(f'"type": {cmd["type"]}')
    print(f'"creation_date": {cmd["creation_date"]}')
    print(f'"date_completed": {cmd["date_completed"]}')
    print(f'"arguments": {cmd["arguments"]}')
    print(f'"response": {cmd["response"]}')
    print(f'"response_code": {cmd["response_code"]}')
    response_codes_array.append(cmd["response_code"])

print(f'\n\n*** Summary: ***\n')
print(f'Is device synced: {"true" if d.is_synced else "false"}')
print(f'Should it be synced: {"true" if False not in sync_array else "false"}')
print(f'Is playbook running: {"true" if d.is_playbook_running else "false"}')
print(f'Individual sync statuses: {str(sync_array)}')
print(f'Last cmd response codes: {str(response_codes_array)}')
print(f'Running installations: {str(running_installations_array)}')
print(f'Failed installations: {str(failed_installations_array)}')