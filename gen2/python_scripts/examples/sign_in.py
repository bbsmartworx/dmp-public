from dmp.DMP_Api import DMP_Api
import requests

base_url = 'http://gateway.wadmp.com'
username = 'editme'
password = 'editme'

api = DMP_Api(base_url)
response = api._auth.authorize(username, password)

if response.status_code != requests.codes['ok']:
    if response.status_code == 400: # Invalid username/password
        raise Exception("Invalid username or password.")
    elif response.status_code == 405: # 'Not Allowed'
        # Note: response.json() throws an exception when err code is 405. So we must not use it here.
        raise Exception(f"Invalid URL.")
    else:
        raise Exception(f"Authorization failed: '{str(response.status_code)} -> {str(response.json())}")             
print(response.json()["access_token"])