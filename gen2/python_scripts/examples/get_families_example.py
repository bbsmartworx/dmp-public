from dmp.ApiConsumer import ApiConsumer
import requests

base_url = 'http://localhost:8082'
username = 'editMe@something.com'
password = 'editMe'

api = ApiConsumer(base_url)
api.login(username, password)

resp = api.family.get_families()
if resp.status_code != requests.codes['ok']:
    # More specific message is in resp.json(), but it is not always present
    # and when it is not and you attempt to access it, an exception is thrown.
    # So for now lets not access it and later I will add some abstraction method
    # for accessing that in safe way.
    raise Exception(f'Could not get families: {resp}') 

print(resp.json())