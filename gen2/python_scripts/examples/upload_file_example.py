from dmp.ApiConsumer import ApiConsumer
import requests

base_url = 'http://gateway.wadmp.com' 
username = 'editme'
password = 'editme'

api = ApiConsumer(base_url)
api.login(username, password)

resp = api.file.update_by_id('359', 'wadmp_client-2.1.1_md5version.v3.tgz', 'D:\\wadmp_client-2.1.1_md5version.v3.tgz')
if resp.status_code != requests.codes['ok']:
    # More specific message is in resp.json(), but it is not always present
    # and when it is not and you attempt to access it, an exception is thrown.
    # So for now lets not access it and later I will add some abstraction method
    # for accessing that in safe way.
    raise Exception(f'Could not update_file: {resp}') 

print(resp.json())