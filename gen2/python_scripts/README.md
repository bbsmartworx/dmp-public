# WebAccess/DMP Scripts
This is a publicly available repository that contains scripts, which use DMP API to manage and monitor devices.

Before using any example scripts, installation of requirements.txt is required.
In command line move to python_scripts directory and than
you can install requirements.txt by
>**pip3 install -r requirements.txt**
command.
Part of this requirements is DMP package.
This package can also be installed by following command, from python_scripts directory:

>**pip3 install ./lib/dist/dmp-1.0.0.tar.gz**

After installing DMP package you can use any part of package under namespace 'dmp'.
Usages are available inside 'example' directory.
