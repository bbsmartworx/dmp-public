# WebAccess/DMP Scripts
This is a publicly available repository that contains scripts, which use DMP API to manage and monitor devices.

Before using any example scripts, installation of requirements.txt is required.
In command line move to python_scripts/csv_utilities directory and than
you can install requirements.txt by command
>**pip3 install -r requirements.txt**

Part of this requirements is DMP package.
This package can also be installed by following command, from python_scripts directory:

>**pip3 install ./lib/dist/dmp-3.1.2.tar.gz**

After installing DMP package you can use any part of package under namespace 'dmp'.
Usages are available inside 'example' directory.
