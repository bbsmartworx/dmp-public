# Building DMP package
PS. Official package is already build and located in python_scripts/lib/dist directory.
1) In command line move to python_scripts/lib directory.
2) pip3 install build (if already not installed)
3) python3 -m build
4) DMP package is build inside python_scripts/lib/dist directory

# Installing DMP package
1) In command line move to python_scripts/lib/dist directory.
2) Install .tar version of dmp package. For example: pip install dmp-3.1.1.tar.gz

# After installation
1) DMP package is installed into active environment.