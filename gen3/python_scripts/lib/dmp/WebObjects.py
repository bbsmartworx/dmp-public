from enum import Enum

##############################################################################################################
# WEB OBJECTS #
##############################################################################################################
class LoginObj(dict):
    def __init__(self, email, pwd):
        self['email'] = email
        self['pwd'] = pwd

class LoginErrorObj(dict):
    def __init__(self, email_error=None, pwd_error=None):
        self['email_error'] = email_error
        self['pwd_error'] = pwd_error

class LoginSignUpObj(dict):
    def __init__(self, first_name, surname, email, pwd, confirm_pwd):
        self['first_name'] = first_name
        self['surname'] = surname
        self['email'] = email
        self['pwd'] = pwd
        self['confirm_pwd'] = confirm_pwd

class LoginSignUpErrorObj(dict):
    def __init__(self, first_name_error=None, surname_error=None, email_error=None, pwd_error=None, confirm_pwd_error=None):
        self['first_name_error'] = first_name_error
        self['surname_error'] = surname_error
        self['email_error'] = email_error
        self['pwd_error'] = pwd_error
        self['confirm_pwd_error'] = confirm_pwd_error

class LoginSignUpCompanyObj(dict):
    def __init__(self, company_name, company_address, country, contact_number):
        self['company_name'] = company_name
        self['company_address'] = company_address
        self['country'] = country
        self['contact_number'] = contact_number

class LoginSignUpCompanyErrorObj(dict):
    def __init__(self, company_name_error=None, company_address_error=None, country_error=None, contact_number_error=None):
        self['company_name_error'] = company_name_error
        self['company_address_error'] = company_address_error
        self['country_error'] = country_error
        self['contact_number_error'] = contact_number_error

class LoginSignUpPrimaryObj(dict):
    def __init__(self, company_contact_name, company_contact_email):
        self['company_contact_name'] = company_contact_name
        self['company_contact_email'] = company_contact_email

class LoginSignUpPrimaryErrorObj(dict):
    def __init__(self, company_contact_name_error=None, company_contact_email_error=None):
        self['company_contact_name_error'] = company_contact_name_error
        self['company_contact_email_error'] = company_contact_email_error