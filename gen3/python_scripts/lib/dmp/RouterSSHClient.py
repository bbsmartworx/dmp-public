import paramiko
import time
import os
from paramiko import ChannelFile

#################################################
# Exception class used for any errors that occur
# on the router.
class RemoteError(Exception):
    pass

#################################################
# Class for manipulating routers via ssh.
class RouterSSHClient:
    def __init__(self, ip, port=22, username='root', password='root'):
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ip = ip
        self.port = port
        self.username = username
        self.password = password

        # Connect
        self.connect()

    def __del__(self):
        if self.is_connected():
            self.ssh.close()

    def is_reachable(self):
        """Pings the device once to see if it's reachable.

        :return: If the device answered to the ping
        """
        return os.system(f'ping -c 1 {self.ip}') == 0

    def wait_until_reachable(self, timeout=60):
        """Pings the device until it's reachable or timeout seconds passed.

        :param int timeout: (Optional) Max time to wait until the device becomes reachable.
        """
        count = 0
        while count < timeout:
            if self.is_reachable():
                break
            time.sleep(5)
            count += 5

    def wait_until_unreachable(self, timeout=60):
        """Pings the device until it's unreachable or timeout seconds passed.

        :param ip: IP of the device.
        :param int timeout: (Optional) Max time to wait until the device becomes unreachable.
        """
        count = 0
        while count < timeout:
            if not self.is_reachable():
                break
            time.sleep(5)
            count += 5

    def connect(self, timeout=8):
        """ Connects to specific device.

        :param timeout: (Optional) Max time to wait .
        """
        self.ssh.connect(self.ip, self.port, self.username, self.password, timeout=timeout)

    def is_connected(self):
        """ Return status of connection.
        """
        if self.ssh.get_transport() is None:
            return False

        return self.ssh.get_transport().is_active()

    def exec(self, cmd, timeout=None):
        """Executes a command on the host and returns the command output.

        :param str command: command to execute on the host.
        :param float timeout: Maximum time (in seconds) to wait for the command to complete.
            If None, the function will wait indefinitely.
        :return: Command response.
        """
        _,stdout,stderr = self.ssh.exec_command(cmd, timeout=timeout)
        return stdout.read().decode('utf-8', 'ignore'), stderr.read().decode(encoding='utf-8')

    def exec_with_retval_check(self, cmd, retval=0, timeout=None) -> ChannelFile:
        """Executes a command on the host with expection of return code.

        :param str command: command to execute on the host.
        :param retval: Expecter return code of command.
        """
        _,stdout,_ = self.ssh.exec_command(cmd, timeout=timeout)
        recv = stdout.channel.recv_exit_status()
        if recv != retval:
            raise RemoteError(f'Remote command "{cmd}" failed (expecting retval "{retval}", but got "{recv}" instead).')
        return stdout

    def exec_without_check(self, cmd, timeout=None):
        """Executes a command on the host and returns the stdin, stdout, and stderr of the executing command, as a 3-tuple.

        :param str command: command to execute on the host.
        :param float timeout: Maximum time (in seconds) to wait for the command to complete.
            If None, the function will wait indefinitely.
        """
        return self.ssh.exec_command(cmd, timeout=timeout)