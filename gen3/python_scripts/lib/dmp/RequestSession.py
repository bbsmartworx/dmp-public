import requests

class RequestSession:

    def __init__(self, base_url_api, ca_path=True):
        self.base_url_api = base_url_api
        self.verify       = ca_path
        self.headers_data = {}
        # When API is under heavy load timeout is 30sec
        self.timeout = 30

    @property
    def headers(self):
        return self.headers_data

    def set_header(self, headers):
        if type(headers) is not type({}):
            raise Exception("Require headers to be provided in dictionary format!")

        for key in headers:
            self.headers_data[key] = headers[key]

    def clear_headers(self):
        self.headers_data = {}

    @staticmethod
    def make_query(args):
        query = {}
        for key in args:
            if args[key] is not None:
                query[key] = args[key]
        return query

    def post(self, url, json = None, params = None, data = None, files = None):
        return requests.post(url, json=json, headers=self.headers, timeout=self.timeout, params=params, data=data, files=files, verify=self.verify)

    def put(self, url, json = None, params = None, data = None, files = None):
        return requests.put(url, json=json, headers=self.headers, timeout=self.timeout, params=params, data=data, files=files, verify=self.verify)

    def get(self, url, json = None, params = None, data = None):
        return requests.get(url, json=json, headers=self.headers, timeout=self.timeout, params=params, data=data, verify=self.verify)

    def delete(self, url, json = None, params = None, data = None):
        return requests.delete(url, json=json, headers=self.headers, timeout=self.timeout, params=params, data=data, verify=self.verify)
