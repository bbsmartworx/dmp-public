import os
from typing import Union
from .RouterSSHClient import RouterSSHClient
import time

class RouterSSH:
    def __init__(self, ip, port=22, username='root', password='root'):
        self.client = RouterSSHClient(ip, port, username, password)

    def _setup_packet_loss(self, lose_rate, src_chain, dst_chain):
        chain = dst_chain
        if 0 > lose_rate > 1:
            raise Exception(f'Invalid lose rate (must be between 0 and 1, but is "{lose_rate}").')
        self.client.exec(f'iptables -F {chain}')                       # Flush contents of the chain
        self.client.exec(f'iptables -D {src_chain} -j {chain}')        # Delete the jump leading into the chain from INPUT
        self.client.exec(f'iptables -X {chain}')                       # Delete the chain
        if lose_rate > 0:
            self.client.exec(f'iptables -N {chain}')                   # Create a new chain
            self.client.exec(f'iptables -A {src_chain} -j {chain}')    # Add jump leading into the chain from INPUT
            # Enable access from local networks, so that we dont cut ourselfs out of the router.
            direction = "-s" if chain=='INPUT' else '-d' # -s = source, -d = destination
            self.client.exec_with_retval_check(f'iptables -A {chain} {direction} 10.0.0.0/8 -j ACCEPT')
            self.client.exec_with_retval_check(f'iptables -A {chain} {direction} 172.16.0.0/12 -j ACCEPT')
            self.client.exec_with_retval_check(f'iptables -A {chain} {direction} 192.168.0.0/16 -j ACCEPT')
            # Set up packet drop
            self.client.exec_with_retval_check(f'iptables -A {chain} -m statistic --mode random --probability {lose_rate} -j DROP')

    def setup_packet_loss_in(self, lose_rate=0.95):
        """Setup packet loss of incoming packets to device.

        :param lose_rate: Lose rate in value between 0 and 1 (1=drop all).
        """
        self._setup_packet_loss(lose_rate, 'INPUT', 'pkt_lose_in')

    def setup_packet_loss_out(self, lose_rate=0.95):
        """Setup packet loss of outgoing packets to device.

        :param lose_rate: Lose rate in value between 0 and 1 (1=drop all).
        """

        self._setup_packet_loss(lose_rate, 'OUTPUT', 'pkt_lose_out')

    def reboot(self):
        """Reboots a device and waits for it to be available again.

        Wait up to 5 minutes for the device to be ready.

        :param device: Device name to locate in the environment or the device itself. Contains the 'ip',
            'ssh_user' and 'ssh_password' of the device to configure.
        """
        try:
            self.client.exec('reboot', 5)
        except TimeoutError:
            # skip this exception, was raisen due to server cut of socket
            pass
        except:
            raise Exception("Error executing reboot command.")
        self.client.wait_until_unreachable(timeout=300)
        self.client.wait_until_reachable(timeout=300)
        # Wait for device to stabilize after reboot
        time.sleep(5)
        # Reconnect ssh
        self.client.connect()

    def update_setting(self, fpath, setting_name, new_value):
        """Update specific setting on device.

        :param fpath: Path to setting file.
        :param setting_name: Name of setting.
        :param new_value: Value of setting to be updated to.
        """
        cmd = f"sed -i '/^{setting_name}=/s/=.*/={new_value}/' {fpath}"
        self.client.exec(cmd)

    def read_setting(self, fpath, setting_name):
        """Return value of setting on device.

        :param fpath: Path to setting file.
        :param setting_name: Name of setting.
        """
        cmd = f"cat {fpath} | grep '{setting_name}=' | " + "awk -F= '{print $2}'"
        return self.client.exec(cmd)[0][:-1]

    def wait_setting_has_value(self, fpath, setting_name, expected_value, timeout=4, interval=2):
        """Awaits setting value change.


        :param fpath: Path to setting file.
        :param setting_name: Name of setting.
        :param expected_value: Value of setting to look for.
        :param timeout: (Optional) Max time to wait .
        :param interval: (Optional) Time to wait between atempts.
        """
        time_spent = 0
        while time_spent < timeout:
            v = self.read_setting(fpath, setting_name)
            if v == expected_value:
                return
            time.sleep(interval)
            time_spent += interval
        raise Exception(f"Timeout reached. Setting {setting_name} was not set to {expected_value} [value: {v}].")

    def backup_configuration(self, filter_by=None):
        """Executes the backup command on the device and returns the result as a dict, possibly filtered.

        The filter works as an optimization of the result returned only, as the command executed on the device is the same.

        :param filter_by: (Optional) text the configuration key or value must match.
        :return: All device configurations as a dict.
        :rtype: dict
        """
        command = "backup" if not filter_by else f"backup | grep -w '{filter_by}'"
        backup, _ = self.client.exec(command)
        configuration = {}

        for line in backup.strip().splitlines():
            line = line.split('=')
            if len(line) > 1:
                configuration[line[0]] = line[1]

        return configuration


    def restore_configuration(self, configuration):
        """Restores the given configuration to the device.

        It uses the restore command on the device to apply the given configuration.

        :param dict configuration: Configuration to restore. The key must be the variable, e.g. {'VARIABLE_NAME': 'VALUE',
            'VARIABLE_NAME2': 'VALUE2'}
        :return: If the given config changed something on the device
        :rtype: bool
        """
        lines = []
        for key, val in configuration.items():
            lines.append('{}={}\n'.format(key, val))

        command = "echo '{}' > /tmp/config_from_test;restore /tmp/config_from_test".format(''.join(lines))

        result, _ = self.client.exec(command)
        return 'successful' in result


    def restore_original(self):
        """Executes the restore command on the device to restore /opt/orig_backup file.

        :return: true/false.
        :rtype: bool
        """
        command = "restore /opt/orig_backup"
        restore, _ = self.client.exec(command)

        return 'successful' in restore or 'unchanged' in restore

    def get_firmware_version(self):
        """Return Firmware version.

        :return: Firmware version.
        """
        stdout,_ = self.client.exec('cat /etc/version')
        return stdout.strip()

    def get_client_version(self):
        """Return Client version.

        :return: Client version.
        """
        stdout,_ = self.client.exec('cat /opt/wadmp_client/etc/version')
        return stdout.strip()

    def get_product_name(self):
        """Get the product name.

        :return: Product name of the device.
        :rtype: str
        """
        return self.get_status_param('sys', 'Product Name')

    def get_serial_number(self):
        """Get the serial number.

        :return: Device's serial number.
        :rtype: str
        """
        return self.get_status_param('sys', 'Serial Number')

    def get_time(self):
        """Get the current time on the device. Not necessarily the correct one.

        :return: Time reported by the device.
        :rtype: str
        """
        return self.get_status_param('sys', 'Time')

    def get_uptime(self):
        """Get the current uptime on the device.

        :return: Uptime reported by the device.
        :rtype: str
        """
        return self.get_status_param('sys', 'Uptime')

    def get_temperature(self):
        """Get the current temperature on the device.

        :return: temperature reported by the device.
        :rtype: str
        """
        return self.get_status_param('sys', 'Temperature')

    def get_voltage(self):
        """Get the current voltage on the device.

        :return: voltage reported by the device.
        :rtype: str
        """
        return self.get_status_param('sys', 'Supply Voltage')

    def get_cpu_usage(self):
        """Get the current CPU usage on the device.

        :return: CPU usage reported by the device.
        :rtype: str
        """
        return self.get_status_param('sys', 'CPU Usage')

    def get_memory_usage(self):
        """Get the current memory on the device.

        :return: Memory reported by the device.
        :rtype: str
        """
        return self.get_status_param('sys', 'Memory Usage')

    def get_module_manufacturer(self, module=1):
        """Get the module manufacturer. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module manufacturer.
        :rtype: str
        """
        return self.get_status_param('module %s' % module, 'Manufacturer')

    def get_module_model(self, module=1):
        """Get the model of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module model.
        :rtype: str
        """
        return self.get_status_param('module %s' % module, 'Model')


    def get_module_revision(self, module=1):
        """Get the revision of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module revision.
        :rtype: str
        """
        return self.get_status_param('module %s' % module, 'Revision')


    def get_module_imei(self, module=1):
        """Get the IMEI of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module IMEI.
        :rtype: str
        """
        return self.get_status_param('module %s' % module, 'IMEI')


    def get_module_iccid(self, module=1):
        """Get the ICCID of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module ICCID.
        :rtype: str
        """
        return self.get_status_param('module %s' % module, 'ICCID')


    def get_module_esn(self, module=1):
        """Get the ESN of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module ESN.
        :rtype: str
        """
        return self.get_status_param('module %s' % module, 'ESN')


    def get_module_meid(self, module=1):
        """Get the MEID of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module MEID.
        :rtype: str
        """
        return self.get_status_param('module %s' % module, 'MEID')


    def get_mobile_registration(self, module=1):
        """Get the registration state of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module registration state.
        :rtype: str
        """
        return self.get_status_param('mobile %s' % module, 'Registration')


    def get_mobile_operator(self, module=1):
        """Get the operator of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module operator.
        :rtype: str
        """
        return self.get_status_param('mobile %s' % module, 'Operator')


    def get_mobile_technology(self, module=1):
        """Get the technology of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module technology.
        :rtype: str
        """
        return self.get_status_param('mobile %s' % module, 'Technology')


    def get_mobile_plmn(self, module=1):
        """
        Get the PLMN of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module PLMN.
        :rtype: str
        """
        return self.get_status_param('mobile %s' % module, 'PLMN')


    def get_mobile_cell(self, module=1):
        """Get the cell of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module cell.
        :rtype: str
        """
        return self.get_status_param('mobile %s' % module, 'Cell')


    def get_mobile_lac(self, module=1):
        """Get the LAC of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module LAC.
        :rtype: str
        """
        return self.get_status_param('mobile %s' % module, 'LAC')


    def get_mobile_channel(self, module=1):
        """Get the channel of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module channel.
        :rtype: str
        """
        return self.get_status_param('mobile %s' % module, 'Channel')


    def get_mobile_strength(self, module=1):
        """Get the signal strength of the given module in dBm. Devices can have 1 or more additional modules. By
        default this function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module signal strenght in dBm.
        :rtype: str
        """
        return self.get_status_param('mobile %s' % module, 'Signal Strength').split()[0]


    def get_mobile_quality(self, module=1):
        """Get the signal quality of the given module in dB. Devices can have 1 or more additional modules. By
        default this function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module signal quality in dB.
        :rtype: str
        """
        return self.get_status_param('mobile %s' % module, 'Signal Quality').split()[0]


    def get_mobile_csq(self, module=1):
        """Get the CSQ of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module CSQ.
        :rtype: str
        """
        return self.get_status_param('mobile %s' % module, 'CSQ')


    def get_mobile_iface(self, module=1):
        """Get the interface of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module interface.
        :rtype: str
        """
        return self.get_status_param('mwan %s' % module, 'Interface')


    def get_mobile_ip(self, module=1):
        """Get the IP address of the given module. Devices can have 1 or more additional modules. By default this
        function will request about the first module.

        :param module: (Optional) Module number.
        :return: Module IP address.
        :rtype: str
        """
        return self.get_status_param('mwan %s' % module, 'IP Address')

    #################################################
    #   Device methods

    def get_status_category(self, category):
        """Get the status of one category.

        status [ -h ] [ -v ] [ lan | mobile | module | ports | ppp | sys | wifi ]
        Options:
        -h:     Generates html output (used when called by web interface)
        -v:     Verbose - writes out more detailed information
        lan:    Status of primary LAN. Can be lan 1, lan 2, etc. if available
        mobile: Status of mobile WAN
        module: Status of mobile module. Can be module 1, module 2, etc. if available
        ports:  Status of available peripheral ports
        ppp:    Status of mobile connection
        sys:    System information
        wifi:   Status of wlan interface

        :param str category: one of the status categories
        :return: Category values as a dict.
        :rtype: dict
        """
        command = f"status -v {category}"

        status_text, _ = self.client.exec(command)

        return status_text

    def get_status_param(self, category, param):
        """Get the value of the given status parameter.

        Example:
        # status -v sys
        Product Name     : UR5i-v2
        Firmware Version : 6.1.3 (2017-08-10)
        Serial Number    : 5341548
        Profile          : Standard
        Supply Voltage   : 24.5 V
        Temperature      : 40 ?C
        CPU Usage        : 0%
        Memory Usage     : 18208 KB / 59368 KB
        Time             : 2018-11-01 16:48:10
        Uptime           : 6 days, 7 hours, 40 minutes

        :param str category: one of the status categories
        :param str param: Status parameter to be retrieved
        :return: Parameter value.
        :rtype: str
        """
        category_status = self.get_status_category(category)
        lines = category_status.strip().splitlines()

        for line in lines:
            if param in line:
                line = line.split(':', 1)
                param_value = line[1].split(',')[0][1:]
                return param_value

        return None

    def is_user_created(self, username: str) -> bool:
        resp = self.client.exec(f"grep -c '{username}' /etc/passwd")[0]
        return resp.strip() == "1"

    def create_user(self, username: str, password: str, group: str = "users") -> Union[Exception, None]:
        """group: [users, root]"""
        self.client.exec_with_retval_check(f'adduser -D -G {group} {username}')
        self.client.exec_with_retval_check(f'echo "{username}:{password}" | chpasswd')

    def delete_user(self, username: str) -> Union[Exception, None]:
        self.client.exec_with_retval_check(f"deluser {username}")

    def make_custom_metrics_dir(self) -> Union[Exception, None]:
        self.client.exec_with_retval_check("mkdir /var/data/wadmp_client/custom_metrics")

    def remove_custom_metrics_dir(self) -> Union[Exception, None]:
        self.client.exec_with_retval_check('rm -rf /var/data/wadmp_client/custom_metrics')

    def write_to_custom_metrics(self, input: str, file: str) -> Union[Exception, None]:
        """will omit echoing trailing newline"""
        self.client.exec_with_retval_check(f"echo -n '{input}' > /var/data/wadmp_client/custom_metrics/{file}")

    def read_config_profile(self) -> str:
        return self.client.exec('cat /var/data/wadmp_client/config_profile.conf')[0]

    def kill_process(self, pid: int) -> Union[Exception, None]:
        self.client.exec_with_retval_check(f"kill {pid}")

    #################################################
    #   Firmware methods

    def firmware_update(self, binary_file_url, version):
        """Performs a firmware update on the device

        It will try to upgrade the firmware without checking the new binary to install. The device will not update if the
        new file is not a firmware binary, but it will go ahead even if the firmware to install was not made for it.
        It waits for the device to reboot and checks if the firmware version is the expected before returning success.

        :param str binary_file_url: The URL of the firmware file to apply to the device
        :param str version: The firmware version, only numbers. e.g. 617
        :return: If the device has the expected firmware version after rebooting
        :rtype: bool
        """
        new_firmware = f'firmware-{version}.bin'
        self.client.exec('mkdir -p /tmp/firmware/')
        self.client.exec(f'curl -k -1 -o /tmp/firmware/{new_firmware} {binary_file_url}')
        try:
            self.client.exec(f'fwupdate -i /tmp/firmware/{new_firmware}', 50)
        except TimeoutError:
            # skip this exception, was raisen due to server cut of socket
            print("Post upgrade reboot.")
        except Exception as e:
            print(e)

        # Wait for the device to (1) update, (2) reboot and (3) stabilize
        self.client.wait_until_reachable(timeout=300)
        time.sleep(5)
        # Reconnect ssh
        self.client.connect()

        # Check for the new firmware version
        new_firmware_version = self.get_firmware_version_from_file()
        return new_firmware_version.startswith('.'.join(version))


    def factory_reset_safe(self):
        """Resets the device to factory defaults but keeping the connection settings.

        The factory_reset() function is not safe as it will reset also the network connection, most probably causing
        the device to be unreachable.
        This function saves the current critical connection settings (eth, ppp, nat and client), performs a factory
        reset on the device and restores the connection settings. It also checks that the reset was done properly
        before returning success.

        :return: If the factory reset was successful.
        :rtype: bool
        """
        # Set a random, non critical setting
        self.restore_configuration({'VRRP_SERVER_ID': '192.168.0.1'})

        # Save critical connection settings, reset to factory and restore saved settings
        self.client.exec('cp /etc/settings.eth /etc/settings.ppp /etc/settings.nat /etc/settings.backrt /tmp')
        self.client.exec('/usr/sbin/defaults')
        self.client.exec('cp /tmp/settings.eth /tmp/settings.ppp /tmp/settings.nat /tmp/settings.backrt /etc')

        # Save client settings, reset user modules to factory and restore saved client settings
        self.client.exec('cp /opt/wadmp_client/etc/settings /tmp')
        self.client.exec('/etc/rc.d/init.d/module defaults')
        self.client.exec('cp /tmp/settings /opt/wadmp_client/etc')

        self.reboot()

        # Check the non-critical setting has been reset
        return self.backup_configuration()['VRRP_SERVER_ID'] == ''


    def factory_reset(self):
        """Performs a factory reset on the device: Note this will cause the device to try to connect over cellular and we
        will lose ssh connection to it. The tester will have to manually restore the configuration. Call at your peril!

        """
        self.client.exec('/usr/sbin/defaults')
        self.client.exec('/etc/rc.d/init.d/module defaults')
        self.reboot()


    def get_firmware_version_from_file(self):
        """Returns the version of the firmware on the device taken from the /etc/version file.

        :return: Firmware version
        :rtype: str
        """
        version, _ = self.client.exec('cat /etc/version')
        return version

    #################################################
    #   Application methods

    def path_exist(self, path: str, check_arg: str = 'e') -> bool:
        """This function verifies whether the provided path exists.
        However, by default it does not differentiate between whether the entity at the given path is a file or a directory.
        :param path: The path to check for existence.
        :param check_arg:
            - b	Checks if file is a block special file.
            - c	Checks if file is a character special file.
            - d	Checks if file is a directory.
            - e	Checks if file exists.
            - f	Checks if file is a regular file.
            - g	Checks if file has set-group-id bit set.
            - h	Checks if file is a symbolic link.
            - n	Checks if the length of string is nonzero.
            - r	Checks if file is readable.
            - s	Checks if file size is greater than zero.
            - u	Checks if file has set-user-id bit set.
            - w	Checks if file is writable.
            - x	Checks if file is executable.
            - z	Checks if the length of string is zero.
        """
        stdout, _ = self.client.exec(f'[ -{check_arg} "{path}" ] && echo 1 || echo 0')

        return int(stdout) == 1

    def install_app(self, binary_file_url: str) -> None:
        """Performs a application install on the device.
        It will try to install the application without checking the new binary to install.
        """
        tmp_new_app = os.path.basename(binary_file_url)
        opt_new_app = tmp_new_app.split('.')[0]

        # Download and unpack app
        self.client.exec(f'mkdir -p /tmp/applications')
        self.client.exec(f'curl -k -1 -o /tmp/applications/{tmp_new_app} {binary_file_url}')
        self.client.exec(f'tar -xzvf /tmp/applications/{tmp_new_app} -C /opt/')
        self.client.exec(f'rm -rf /tmp/applications')

        # Create symlink to web pages
        self.client.exec(f'ln -s /opt/{opt_new_app}/www /var/www/{opt_new_app}')

        # Install and/or init app
        if self.path_exist(f'/opt/{opt_new_app}/etc/install'):
            self.client.exec(f'/opt/{opt_new_app}/etc/install')
        if self.path_exist(f'/opt/{opt_new_app}/etc/init'):
            self.client.exec(f'/opt/{opt_new_app}/etc/init defaults')
            self.client.exec_with_retval_check(f'/opt/{opt_new_app}/etc/init start')

    def restart_app(self, app_name: str) -> None:
        if self.path_exist(f'/opt/{app_name}/etc/init'):
            self.client.exec_with_retval_check(f'/opt/{app_name}/etc/init restart')

    def start_app(self, app_name: str) -> None:
        if self.path_exist(f'/opt/{app_name}/etc/init'):
            self.client.exec_with_retval_check(f'/opt/{app_name}/etc/init start')

    def stop_app(self, app_name: str) -> None:
        if self.path_exist(f'/opt/{app_name}/etc/init'):
            self.client.exec(f'/opt/{app_name}/etc/init stop')

    def delete_app(self, app_name: str) -> None:
        """Delete application.

        :param app_name: Name of application without version. Example: 'pinger'.
        """
        self.client.exec(f'rm -rf /opt/{app_name}')
        if app_name == "wadmp_client":
            self.client.exec(f"rm -rf /var/data/wadmp_client")

    def get_app_version(self, app_name: str) -> str:
        """Return application version.

        :param str app_name: Name of application without version. Example 'pinger'.
        :return: Application version. Example: '2.6.1 (2022-05-10)'
        """
        stdout,_ = self.client.exec(f'cat /opt/{app_name}/etc/version')
        return stdout.strip()