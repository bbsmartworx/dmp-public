import json
import requests
import time
import re
from .utils.wrappers import *

from dmp.ApiConsumer import ApiConsumer
from dmp.Enums import ConnectionStatus, SyncStatus
from dmp.models.Shared import DeviceModel
from dmp.models.Companies import FindCompanyModel
from dmp.models.Types import FindTypesModel
from dmp.models.Applications import FindApplicationModel

######################################################
# This is a high level API built on top of ApiConsumer.
# It adds methods that combine multiple API calls into achieving a goal.

class DMP_Api(ApiConsumer):
    def get_device_type_id(self, mac) -> int:
        """ Returns device type id by given mac. """
        resp = check(self.device_monitoring.get_by_mac(mac))
        device_type_name = get_device_type(get_data(resp))
        device_types = self.type.find_types(FindTypesModel(type_name=device_type_name))
        device_type_id = [get_id(x) for x in get_data(device_types) if get_name(x) == device_type_name]
        if not device_type_id:
            raise Exception(f"Device type {device_type_name} was not found.")
        return device_type_id[0]

    #####################################################
    def get_app_by_name(self, app_name) -> json:
        """
        Returns application with exact name you provided.
        example:   {"id": 61,
                    "name": "noderedopcua",
                    "display_name": "Node-RED OPC UA Node",
                    "description": "Provides nodes for using OPC UA protocol in your Node-RED projects.",
                    "is_global": true,
                    "is_firmware": false}
        """
        resp = check(self.application.find_apps(FindApplicationModel(name=app_name)))
        result = [app for app in get_data(resp) if get_name(app) == app_name]
        if not result:
            raise Exception(f"Application {app_name} was not found.")
        return result[0]

    #####################################################
    def get_app_version(self, app_name, app_version_str) -> json:
        """
        example input: app_name='wadmp_client', app_version_str='2.1.1'
        return: application version as json.
        """
        desired_app_id = get_id(self.get_app_by_name(app_name))
        desired_app_versions = get_data(self.application.find_versions_of_app(desired_app_id))
        desired_app_version = [version for version in desired_app_versions if get_version(version) == app_version_str]
        if not desired_app_version:
            raise Exception(f"Version {app_version_str} for app {app_name} was not found.")
        return desired_app_version[0]

    #####################################################
    def get_appversion_id(self, app_name, app_version_str) -> int:
        """
        example input: app_name='wadmp_client', app_version_str='2.1.1'
        return: application version id.
        """
        return get_id(self.get_app_version(app_name, app_version_str))

    #####################################################
    def get_company_by_name(self, company_name) -> json:
        """
        Returns company with exact name you provided.
        return type: json
        """
        resp = check(self.company.find_companies(FindCompanyModel(name=company_name)))
        result = [comp for comp in get_data(resp) if get_name(comp) == company_name]
        if not result:
            raise Exception(f"Company {company_name} was not found.")
        return result[0]

    #####################################################
    def get_company_id_by_name(self, company_name) -> int:
        """
        Returns company ID.
        Raises exception if company does not exist.
        """
        return get_id(self.get_company_by_name(company_name))

    #####################################################
    def get_device_datamodel(self, mac) -> DeviceModel:
        """
        Returns information about the given device parsed
        into an easy to use object.
        """
        resp = check(self.device_monitoring.get_by_mac(mac))
        resp_2 = check(self.config_profile.get_config_profile_base_by_mac(mac))
        return DeviceModel(get_data(resp), get_data(resp_2))

    #####################################################
    def get_apps_by_mac(self, mac) -> list:
        """ Returns list of all apps installed on device.
        If there is no config profile on the device, no apps (type: None) will be returned,
        even if there are some apps on the device.

        :param mac: Mac address of device we work with.
        :return: [(app_name, version, url), ...]
        :rtype: list

        """
        return self.get_device_datamodel(mac).apps_installed

    #####################################################
    def get_client_version_id_current(self, mac) -> int:
        """
        Returns client version id of the currently installed wadmp_client.
        If there is no config profile on the device returns None.
        """
        client_version = self.get_device_datamodel(mac).wadmp_app_version
        if client_version:
            return self.get_appversion_id(Constants.wadmp_client.value, client_version)
        print(f"Couldn't get client version id, because empty config profile. Return: None")

    #####################################################
    # Returns version of the currently installed wadmp_client.
    def get_client_version_current(self, mac) -> str:
        """
        Returns version of the currently installed wadmp_client.
        If there is not config profile on the device returns None.
        """
        return self.get_device_datamodel(mac).wadmp_app_version

    #####################################################
    def get_firmware_url_by_mac(self, mac) -> str:
        """
        Returns currently installed firmware of a device.
        If there is not config profile on the device returns None.
        """
        return self.get_device_datamodel(mac).firmware_url

    #####################################################
    def get_firmware_version_by_mac(self, mac) -> str:
        """
        Returns currently installed firmware version of a device.
        If there is not config profile on the device returns None.
        """
        return self.get_device_datamodel(mac).firmware_version

    #####################################################
    # Returns id of firmware currently installed on a device.
    def get_firmware_version_id_by_mac(self, mac) -> int:
        """
        Returns id of firmware currently installed on a device.
        If there is not config profile on the device or device type field is missing returns None.
        """
        device = self.get_device_datamodel(mac)
        firmware_version = device.firmware_version
        app_name = device.device_type
        if firmware_version and app_name:
            return self.get_appversion_id(app_name, firmware_version)
        print(f"Couldn't get firmware version id, because empty config profile or missing device type field. Return: None")

    #####################################################
    def get_last_x_commands_created(self, mac, count=None) -> list:
        """ Retrieves last X commands that were created for the given device. """
        commands = get_data(check(self.command.get_history_of_commands_by_mac(mac)))
        if count and len(commands) > count:
            return commands[:count]
        return commands

    #####################################################
    def get_device_setting(self, mac, setting_name) -> dict:
        """
        Returns value of the given setting from config profile.
        If settings in config profile is null, returns None.
        """
        config_profile = get_data(check(self.config_profile.get_config_profile_base_by_mac(mac)))
        settings = get_settings(json.loads(config_profile))
        if settings:
            return settings.get(setting_name)

    #####################################################
    # Returns name of the firmware that would match the
    # given device (firmwares for different device types
    # have different names)
    def get_fw_name_by_mac(self, mac) -> str:
        """
        Returns name of the firmware that would match the
        given device (firmware_name match the name of device_type)
        """
        return self.get_device_datamodel(mac).device_type

    #####################################################
    def _wait_device(self, is_ready_f, mac, timeout=99999, interval=2, persist=0, err_str='Timeout reached.') -> None:
        """
        Waits at most 'timeout' seconds until the function
        passed as 'is_ready_f' starts returning true for at
        least 'persist' seconds. 'interval' controls how often
        the timeout check and persist check should be done
        (check once every 'interval' seconds).
        """
        time_spent = 0
        sec_in_final_state = 0
        prev_ready = False
        while time_spent < timeout:
            if is_ready_f(mac):
                if prev_ready:
                    sec_in_final_state += interval
                if sec_in_final_state >= persist:
                    return
                prev_ready = True
            else:
                sec_in_final_state = 0
                prev_ready = False
            time.sleep(interval)
            time_spent += interval      
        raise Exception(err_str)

    #####################################################
    def wait_device_online(self, mac, timeout=99999, interval=2, persist=0) -> None:
        """
        Waits at most 'timeout' seconds until the given
        device becomes online and stays online for at least
        'persist' seconds. 'interval' controls how often
        the timeout check and persist check should be done
        (check once every 'interval' seconds).
        """
        # We define lambda function that will be used in _wait_device() method
        # to check whether it should continue waiting or not.
        is_ready_f = lambda dev_mac: self.is_device_online(dev_mac)
        err_str = f"Timeout reached while waiting for device {mac} to become online."

        self._wait_device(is_ready_f, mac, timeout, interval, persist, err_str)

    #####################################################
    def wait_device_synced(self, mac, timeout=99999, interval=2, persist=0) -> None:
        """
        Waits at most 'timeout' seconds until the given
        device becomes synced and stays synced for at least
        'persist' seconds. 'interval' controls how often
        the timeout check and persist check should be done
        (check once every 'interval' seconds).
        """
        # We define lambda function that will be used in _wait_device() method
        # to check whether it should continue waiting or not.
        is_ready_f = lambda dev_mac: self.is_device_synced(dev_mac)
        err_str = f"Timeout reached while waiting for device {mac} to become synced."

        self._wait_device(is_ready_f, mac, timeout, interval, persist, err_str)

    #####################################################
    def wait_device_ready(self, mac, timeout=99999, interval=2, persist=0) -> None:
        """
        Waits at most 'timeout' seconds until the given
        device becomes online+synced and stays in that state
        for 'persist' seconds. 'interval' controls how often
        the timeout check and persist check should be done
        (check once every 'interval' seconds).
        """
        # We define lambda function that will be used in _wait_device() method
        # to check whether it should continue waiting or not.
        is_ready_f = lambda dev_mac: self.is_device_synced(dev_mac) and self.is_device_online(dev_mac)
        err_str = f"Timeout reached while waiting for device {mac} to become online & synced."

        self._wait_device(is_ready_f, mac, timeout, interval, persist, err_str)

    #####################################################
    def is_device_online(self, mac) -> bool:
        """ Returns true if the given device is online. """
        resp = check(self.device_monitoring.get_by_mac(mac))
        connection_status = get_connection_status(get_data(resp))
        return connection_status == ConnectionStatus.Online.value

    #####################################################
    def is_device_synced(self, mac) -> bool:
        """ Returns true if the given device is synced. """
        resp = check(self.device_monitoring.get_by_mac(mac))
        sync_status = get_sync_status(get_data(resp))
        return sync_status == SyncStatus.Synced.value

    #####################################################
    def device_exists(self, mac) -> bool:
        """ Returns true if device exists. False otherwise. """
        resp = self.device_monitoring.get_by_mac(mac)
        if resp.status_code == requests.codes['ok']:
            return True
        if resp.status_code == 404:
            return False
        raise Exception(f'Cannot get device information for {mac}: {resp}')
