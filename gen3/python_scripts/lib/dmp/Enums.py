from enum import Enum

##############################################################################################################
# ENUMS #
##############################################################################################################
class DeviceUserRole(Enum):
    User    = "User"
    Admin   = "Admin"
    Deleted = "Deleted"

class FieldDisplayCategory(Enum):
    StaticInformation    = "StaticInformation"
    MonitoringMomentary  = "MonitoringMomentary"
    MonitoringCumulative = "MonitoringCumulative"
    Configuration        = "Configuration"

class FieldReportMode(Enum):
    Never    = "Never"
    Always   = "Always"
    OnChange = "OnChange"

class FieldResetPeriodUnit(Enum):
    Hours  = "Hours"
    Days   = "Days"
    Months = "Months"

class BillingStatus(Enum):
    Created = "Created"
    Sent    = "Sent"
    Failed  = "Failed"
    Deleted = "Deleted"

class BillingMethod(Enum):
    SAP = "SAP"
    MKT = "MKT"

class BillingSortBy(Enum):
    Id              = "Id"
    CompanyName     = "CompanyName"
    SentDate        = "SentDate"
    CreatedDate     = "CreatedDate"
    MonthAndYear    = "MonthAndYear"
    Status          = "Status"
    TotalPrice      = "TotalPrice"
    BillingMethod   = "BillingMethod"

class LongOperationState(Enum):
    Created             = "Created"
    Finished            = "Finished"
    FinishedWithErrors  = "FinishedWithErrors"
    Queued              = "Ready"
    Running             = "Running"
    Aborted             = "Aborted"

class LongOperationType(Enum):
    CreateRebootCommands        = "CreateRebootCommands"
    CreateRebootstrapCommands   = "CreateRebootstrapCommands"
    UpdateDevices               = "UpdateDevices"
    ClaimDevices                = "ClaimDevices"
    UnclaimDevices              = "UnclaimDevices"
    OperationSequence           = "OperationSequence"

class Months(Enum):
    January     = "January"
    February    = "February"
    March       = "March"
    April       = "April"
    May         = "May"
    June        = "June"
    July        = "July"
    August      = "August"
    September   = "September"
    October     = "October"
    November    = "November"
    December    = "December"

class ConfigUnlistedBehavior(Enum):
    Ignore  = "Ignore"
    Monitor = "Monitor"
    Purge   = "Purge"

class NextHopServer(Enum):
    develop         = "bootstrap.dev.wadmp.com"
    staging         = "bootstrap.staging.wadmp.com"
    onprem          = "bootstrap.onprem.blade.link"
    fresh_onprem    = "bootstrap.fresh-onprem.wadmp-infrastructure.com"
    updated_onprem  = "bootstrap.updated-onprem.wadmp-infrastructure.com"

class Permissions(Enum):
    none            = "None"
    ViewUsers       = "ViewUsers"
    AddUsers        = "AddUsers"
    EditUsers       = "EditUsers"
    RemoveUsers     = "RemoveUsers"
    AddCompany      = "AddCompany"
    EditCompany     = "EditCompany"
    ViewDevices     = "ViewDevices"
    AddDevices      = "AddDevices"
    EditDevices     = "EditDevices"
    RemoveDevices   = "RemoveDevices"
    ViewAlerts      = "ViewAlerts"
    AddAlerts       = "AddAlerts"
    EditAlerts      = "EditAlerts"
    RemoveAlerts    = "RemoveAlerts"
    ViewAuditing    = "ViewAuditing"
    ManageFields    = "ManageFields"
    ManageViews     = "ManageViews"
    ManageAppStore  = "ManageAppStore"
    ManageBilling   = "ManageBilling"

class PlotType(Enum):
    Line    = "Line"
    Points  = "Points"

class WidgetType(Enum):
    Plot        = "Plot"
    Table       = "Table"
    Map         = "Map"
    CompanyStat = "CompanyStat"
    Pie         = "Pie"

class TrackingMode(Enum):
    Auto   = "Auto"
    Manual = "Manual"

class NextHopServerType(Enum):
    BootstrapServer     = "BootstrapServer"
    ManagementServer    = "ManagementServer"

class EndpointTypes(Enum):
    Email   = "Email"

class GrantType(Enum):
    Implicit                                    = "Implicit"
    ImplicitAndClientCredentials                = "ImplicitAndClientCredentials"
    Code                                        = "Code"
    CodeAndClientCredentials                    = "CodeAndClientCredentials"
    Hybrid                                      = "Hybrid"
    HybridAndClientCredentials                  = "HybridAndClientCredentials"
    ClientCredentials                           = "ClientCredentials"
    ResourceOwnerPassword                       = "ResourceOwnerPassword"
    ResourceOwnerPasswordAndClientCredentials   = "ResourceOwnerPasswordAndClientCredentials"
    DeviceFlow                                  = "DeviceFlow"

class ConnectionStatus(Enum):
    Online          = 1
    Offline         = 0
    Never_connected = -1

class SyncStatus(Enum):
    Synced      = 1
    Pending     = 0
    Not_synced  = -1

class SamplingOperator(Enum):
    Random = 'Random'

class CompanyType(Enum):
    Free                = 'Free'
    Trial               = 'Trial'
    Premium             = 'Premium'
    PremiumMarketplace  = 'Premium marketplace'

class SourcesSearchType(Enum):
    AuditingAggregation = "AuditingAggregation"
    FieldAggregation    = "FieldAggregation"
    CompanyInfo         = "CompanyInfo"
    Field               = "Field"

class ViewType(Enum):
    DevicePage  = "DevicePage"
    Dashboard   = "Dashboard"

class DataType(Enum):
    String      = "String"
    Integer     = "Integer"
    Bool        = "Bool"
    Float       = "Float"
    DateTime    = "DateTime"

class SortDirection(Enum):
    ASC   = "ASC"
    DESC  = "DESC"

class SortUsers(Enum):
    FirstName      = 'FirstName'
    LastName       = 'LastName'
    Email          = 'Email'
    DateOfCreation = 'DateOfCreation'

class SortAlerts(Enum):
    Id        = "Id"
    Name      = "Name"
    CompanyId = "CompanyId"

class SortAlertsHistory(Enum):
    Id             = "Id"
    AlertName      = "AlertName"
    CompanyId      = "CompanyId"
    Device         = "Device"
    ActivationDate = "ActivationDate"
    Message        = "Message"
    IsAcked        = "IsAcked"

class SortCompanies(Enum):
    Name         = 'Name'
    ContactName  = 'ContactName'
    ContactEmail = 'ContactEmail'

class SortFieldTemplates(Enum):
    Id           = "Id"
    Name         = "name"
    DisplayName  = "DisplayName"
    DataType     = "DataType"
    ReportMode   = "ReportMode"
    Units        = "Units"

class SortFields(Enum):
    Id              = "Id"
    CompanyId       = "CompanyId"
    Name            = "name"
    DisplayName     = "DisplayName"
    ReportMode      = "ReportMode"
    FieldTemplateId = "FieldTemplateId"
    Units           = "Units"

class FilteringOperatorId(Enum):
    Equals              = "Equals"
    NotEquals           = "NotEquals"
    NotBetween          = "NotBetween"
    Between             = "Between"
    GreaterThan         = "GreaterThan"
    LessThan            = "LessThan"
    GreaterThanOrEqual  = "GreaterThanOrEqual"
    LessThanOrEqual     = "LessThanOrEqual"
    ContainsSubstring   = "ContainsSubstring"
    In                  = "In"
    NotIn               = "NotIn"

class FieldNames(Enum):
    CompanyId           = "CompanyId"
    DataDownR           = "DataDownR"
    DataUpR             = "DataUpR"
    DataMixedR          = "DataMixedR"
    DataMixedC          = "DataMixedC"
    MoSim               = "MoSim"
    MoOperator          = "MoOperator"
    MoTechnology        = "MoTechnology"
    Online              = "Online"
    OnlineOfflineSince  = "OnlineOfflineSince"
    ReconnectsCount     = "ReconnectsCount"
    Id                  = "Id"
    MacAddress          = "MacAddress"
    Name                = "Name"
    Description         = "Description"
    Serial              = "Serial"
    Imei                = "Imei"
    CreationDate        = "CreationDate"
    ConfigProfileBaseId = "ConfigProfileBaseId"
    SyncStatus          = "SyncStatus"
    DeviceType          = "DeviceType"
    ClaimedDate         = "ClaimedDate"
    DeviceUptime        = "DeviceUptime"
    SuppVoltage         = "SuppVoltage"
    Temperature         = "Temperature"
    WanIfaceIp          = "WanIfaceIp"
    MemUsage            = "MemUsage"
    CpuUsage            = "CpuUsage"
    RtcBattery          = "RtcBattery"
    ProdRevision        = "ProdRevision"
    MoBand              = "MoBand"
    MoLastConnect       = "MoLastConnect"
    MoPlmn              = "MoPlmn"
    MoSigQuality        = "MoSigQuality"
    MoSigStrength       = "MoSigStrength"
    MoTodayCells        = "MoTodayCells"
    MoLac               = "MoLac"
    MoUptime            = "MoUptime"
    GpsAlt              = "GpsAlt"
    DataDownC           = "DataDownC"
    DataUpC             = "DataUpC"
    MoCell              = "MoCell"
    MoLastDisconnect    = "MoLastDisconnect"
    MoChannel           = "MoChannel"
    Iccid               = "Iccid"
    GpsLat              = "GpsLat"
    GpsLon              = "GpsLon"
    CustNum             = "CustNum"
    CustStr             = "CustStr"
    TagBool             = "TagBool"
    TagString           = "TagString"
    SettingUpdate       = "SettingUpdate"
    UserPassword        = "UserPassword"
    PingLat             = "PingLat"
    PingStat            = "PingStat"
    NextHopServerId     = "NextHopServerId"

class Platforms(Enum):
    v2 = 'v2'
    v3 = 'v3'
    v4 = 'v4'

class DeviceTypes(Enum):
    LR77_v2             = "LR77-v2"
    LR77_v2L            = "LR77-v2L"
    UR5i_v2             = "UR5i-v2"
    UR5i_v2L            = "UR5i-v2L"
    XR5i_v2             = "XR5i-v2"
    XR5i_v2e            = "XR5i-v2e"
    ICR_320x            = "ICR-320x"
    ICR_321x            = "ICR-321x"
    ICR_323x            = "ICR-323x"
    ICR_324x            = "ICR-324x"
    ICR_324x_1N         = "ICR-324x-1N"
    SPECTRE_v3_ERT      = "SPECTRE-v3-ERT"
    SPECTRE_v3_LTE      = "SPECTRE-v3-LTE"
    SPECTRE_v3_LTE_US   = "SPECTRE-v3-LTE-US"
    SPECTRE_v3L_LTE     = "SPECTRE-v3L-LTE"
    SPECTRE_v3L_LTE_US  = "SPECTRE-v3L-LTE-US"
    SPECTRE_v3T_LTE     = "SPECTRE-v3T-LTE"
    ICR_1601G           = "ICR-1601G"
    ICR_1601W           = "ICR-1601W"
    WISE_6610_AS        = "WISE-6610-AS"
    WISE_6610_EU        = "WISE-6610-EU"
    WISE_6610_US        = "WISE-6610-US"
    WISE_6610_AS_Eth    = "WISE-6610-AS-Eth"
    WISE_6610_EU_Eth    = "WISE-6610-EU-Eth"
    WISE_6610_US_Eth    = "WISE-6610-US-Eth"
    ICR_203x            = "ICR-203x"
    ICR_204x            = "ICR-204x"
    ICR_241x            = "ICR-241x"
    ICR_243x            = "ICR-243x"
    ICR_244x            = "ICR-244x"
    ICR_253x            = "ICR-253x"
    ICR_263x            = "ICR-263x"
    ICR_270x            = "ICR-270x"
    ICR_273x            = "ICR-273x"
    ICR_283x            = "ICR-283x"
    ICR_440x            = "ICR-440x"
    ICR_443x            = "ICR-443x"
    ICR_445x            = "ICR-445x"
    ICR_446x            = "ICR-446x"

class AuditingActionTypes(Enum):
    USER_SIGNED_IN                              = "USER_SIGNED_IN"
    USER_SIGNED_OUT                             = "USER_SIGNED_OUT"
    USER_CREATED                                = "USER_CREATED"
    USER_PROFILE_UPDATED                        = "USER_PROFILE_UPDATED"
    USER_PASSWORD_UPDATED                       = "USER_PASSWORD_UPDATED"
    USER_INVITE_CREATED                         = "USER_INVITE_CREATED"
    COMPANY_CREATED                             = "COMPANY_CREATED"
    COMPANY_UPDATED                             = "COMPANY_UPDATED"
    COMPANY_DELETED                             = "COMPANY_DELETED"
    DEVICE_REGISTERED                           = "DEVICE_REGISTERED"
    DEVICE_UPDATED                              = "DEVICE_UPDATED"
    DEVICE_UNREGISTERED                         = "DEVICE_UNREGISTERED"
    DEVICE_ADDED                                = "DEVICE_ADDED"
    DEVICE_REMOVED                              = "DEVICE_REMOVED"
    REQUESTED_RESEND_OF_CONFIG                  = "REQUESTED_RESEND_OF_CONFIG"
    DEVICE_CONNECTED                            = "DEVICE_CONNECTED"
    DEVICE_DISCONNECTED                         = "DEVICE_DISCONNECTED"
    FILE_MAPPING_UPDATED                        = "FILE_MAPPING_UPDATED"
    APPVERSION_DEFINITION_CREATED               = "APPVERSION_DEFINITION_CREATED"
    APPVERSION_DEFINITION_UPDATED               = "APPVERSION_DEFINITION_UPDATED"
    APPVERSION_DEFINITION_DELETED               = "APPVERSION_DEFINITION_DELETED"
    APP_DEFINITION_CREATED                      = "APP_DEFINITION_CREATED"
    APP_DEFINITION_UPDATED                      = "APP_DEFINITION_UPDATED"
    APP_DEFINITION_DELETED                      = "APP_DEFINITION_DELETED"
    BOOTSTRAP_NEXTHOP_SERVER_DEFINITION_CREATED = "BOOTSTRAP_NEXTHOP_SERVER_DEFINITION_CREATED"
    BOOTSTRAP_NEXTHOP_SERVER_DEFINITION_UPDATED = "BOOTSTRAP_NEXTHOP_SERVER_DEFINITION_UPDATED"
    BOOTSTRAP_NEXTHOP_SERVER_DEFINITION_DELETED = "BOOTSTRAP_NEXTHOP_SERVER_DEFINITION_DELETED"
    BOOTSTRAP_DEVICE_NEXTHOP_SERVER_CHANGED     = "BOOTSTRAP_DEVICE_NEXTHOP_SERVER_CHANGED"
    FILE_UPLOADED                               = "FILE_UPLOADED"
    FILE_DELETED                                = "FILE_DELETED"
    INVOICE_SENT                                = "INVOICE_SENT"
    INVOICE_REMOVED                             = "INVOICE_REMOVED"
    INVOICE_CREATED                             = "INVOICE_CREATED"
    INVOICE_CREATION_FAILED                     = "INVOICE_CREATION_FAILED"
    INVOICE_SENDING_FAILED                      = "INVOICE_SENDING_FAILED"
    USER_LOGIN_FAILED                           = "USER_LOGIN_FAILED"
    COMPANY_TYPE_UPGRADED                       = "COMPANY_TYPE_UPGRADED"
    REQUESTED_DEVICE_REBOOT                     = "REQUESTED_DEVICE_REBOOT"
    REQUESTED_DEVICE_BOOTSTRAP                  = "REQUESTED_DEVICE_BOOTSTRAP"
    ALERT_ENDPOINT_CREATED                      = "ALERT_ENDPOINT_CREATED"
    ALERT_ENDPOINT_UPDATED                      = "ALERT_ENDPOINT_UPDATED"
    ALERT_ENDPOINT_DELETED                      = "ALERT_ENDPOINT_DELETED"
    ALERTS_EVENT_ACKED                          = "ALERTS_EVENT_ACKED"
    ALERT_ENDPOINT_ASSIGNMENT_UPDATED           = "ALERT_ENDPOINT_ASSIGNMENT_UPDATED"
    ALERT_CREATED                               = "ALERT_CREATED"
    ALERT_UPDATED                               = "ALERT_UPDATED"
    ALERT_DELETED                               = "ALERT_DELETED"
    FIELD_CREATED                               = "FIELD_CREATED"
    FIELD_DELETED                               = "FIELD_DELETED"
    FIELD_UPDATED                               = "FIELD_UPDATED"
    CONFIG_PROFILE_BASE_UPDATED                 = "CONFIG_PROFILE_BASE_UPDATED"
    DEVICE_LOCAL_CHANGES_DETECTED               = "DEVICE_LOCAL_CHANGES_DETECTED"
    REQUESTED_DEVICE_CONFIG                     = "REQUESTED_DEVICE_CONFIG"
    VIEW_CREATED                                = "VIEW_CREATED"
    VIEW_DELETED                                = "VIEW_DELETED"
    VIEW_UPDATED                                = "VIEW_UPDATED"
    USER_DELETED                                = "USER_DELETED"
    API_CLIENT_CREATED                          = "API_CLIENT_CREATED"
    API_CLIENT_DELETED                          = "API_CLIENT_DELETED"
    API_CLIENT_UPDATED                          = "API_CLIENT_UPDATED"
    USER_PERMISSIONS_UPDATED                    = "USER_PERMISSIONS_UPDATED"
    CONFIG_PROFILE_BASE_CREATED                 = "CONFIG_PROFILE_BASE_CREATED"
    LONG_OPERATION_CREATED                      = "LONG_OPERATION_CREATED"
    LONG_OPERATION_ABORTED                      = "LONG_OPERATION_ABORTED"
    DEVICE_TYPE_CREATED                         = "DEVICE_TYPE_CREATED"
    DEVICE_TYPE_DELETED                         = "DEVICE_TYPE_DELETED"
    CONFIG_PROFILE_BASE_DEFAULT_UPDATED         = "CONFIG_PROFILE_BASE_DEFAULT_UPDATED"