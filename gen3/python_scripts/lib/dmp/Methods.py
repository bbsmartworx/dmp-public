import re
import pandas
from dmp.models.Shared import DeviceModel


#########################################
def die(message):
    """Prints message and exits with exit code 1."""
    print(message)
    exit(1)

#########################################################
def csv_get_column(csv_file, column_name):
    """ Looks for column named <col_name> and returns list
        of values from this column.
    """
    # Read the CSV file into a DataFrame
    df = pandas.read_csv(csv_file)

    # Extract the values from the specified column and return them like list
    return df[column_name].tolist()

#########################################################
def is_mac_address(mac) -> bool:
    """ Returns True if the given mac is valid MAC address.
        Otherwise returns False.
    """
    pattern = r'^([0-9a-f]{2}[:-]){5}[0-9a-f]{2}$'
    normalized_mac = mac.lower()
    return bool(re.match(pattern, normalized_mac))

#########################################################
def has_wadmp_client_atleast(device: DeviceModel, version) -> bool:
    """ Returns true if the device has currently installed
        higher or equal to version of wadmp client than 'version'.
        'Version' is in format e.g. '2.0.10'.
    """
    if not device.wadmp_app:
        return False

    app_version = device.wadmp_app[1]
    req_version = version

    return app_version >= req_version
