from dmp.RequestSession import RequestSession
from dmp.models.Types import *


class ApiTypes:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_by_id(self, type_id):
        url = f"{self.base_url_api}/device-types/{type_id}"
        response = self.session.get(url)
        return response

    def delete_by_id(self, type_id):
        url = f"{self.base_url_api}/device-types/{type_id}"
        response = self.session.delete(url)
        return response

    def find_types(self, model: FindTypesModel):
        url = f"{self.base_url_api}/device-types"
        query = RequestSession.make_query({
            'TypeName': model.type_name,
            'FamilyId': model.family_id
        })
        response = self.session.get(url, params=query)
        return response

    def create(self, model: CreateTypeModel):
        url = f"{self.base_url_api}/device-types"
        response = self.session.post(url, json=model)
        return response