from dmp.RequestSession import RequestSession
from dmp.models.Alerts import *


class ApiAlerts:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def find_alerts(self, model: FindAlertModel):
        url = f"{self.base_url_api}/alerts"
        query = RequestSession.make_query({
            'Name': model.name,
            'CompanyIds': model.company_ids,
            'DeviceMacAddresses': model.device_mac_addresses,
            'AlertSortingType': model.alert_sorting_type,
            'SortDirection': model.sort_direction,
            'Page': model.page,
            'PageSize': model.pagesize
        })
        response = self.session.get(url, params=query)
        return response

    def get_by_id(self, alert_id):
        url = f"{self.base_url_api}/alerts/{alert_id}"
        response = self.session.get(url)
        return response

    def delete_by_id(self, alert_id):
        url = f"{self.base_url_api}/alerts/{alert_id}"
        response = self.session.delete(url)
        return response

    def create(self, model: CreateAlertModel):
        url = f"{self.base_url_api}/alerts"
        response = self.session.post(url, json=model)
        return response

    def update_by_id(self, alert_id, model: UpdateAlertModel):
        url = f"{self.base_url_api}/alerts/{alert_id}"
        response = self.session.put(url, json=model)
        return response