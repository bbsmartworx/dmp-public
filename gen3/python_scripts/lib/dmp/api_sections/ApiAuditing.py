from dmp.RequestSession import RequestSession
from dmp.models.Auditing import *


class ApiAuditing:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def find_audit_logs(self, model: FindAuditLogsModel):
        url = f"{self.base_url_api}/auditing"
        query = RequestSession.make_query({
            'MacAddress':           model.mac_address,
            'UserId':               model.user_id,
            'Companies':            model.companies,
            'IncludeEmptyCompany':  model.include_empty_company,
            'Start':                model.start,
            'End':                  model.end,
            'ActionTypes':          model.action_types,
            'Page':                 model.page,
            'PageSize':             model.page_size
        })
        response = self.session.get(url, params=query)
        return response

    def get_all_action_types(self):
        url = f"{self.base_url_api}/auditing/action-types"
        response = self.session.get(url)
        return response