from dmp.RequestSession import RequestSession
from dmp.models.Companies import *


class ApiCompanies:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_by_id(self, company_id):
        url = f"{self.base_url_api}/companies/{company_id}"
        response = self.session.get(url)
        return response

    def update_by_id(self, company_id, model: UpdateCompanyModel):
        url = f"{self.base_url_api}/companies/{company_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_by_id(self, company_id):
        url = f"{self.base_url_api}/companies/{company_id}"
        response = self.session.delete(url)
        return response

    def find_companies(self, model: FindCompanyModel):
        url = f"{self.base_url_api}/companies"
        query = RequestSession.make_query({
            'name': model.name,
            'sort': model.sort,
            'sortOrder': model.sort_order
        })
        response = self.session.get(url, params=query)
        return response

    def create(self, model: CreateCompanyModel):
        url = f"{self.base_url_api}/companies"
        response = self.session.post(url, json=model)
        return response

    def get_status_of_import(self, import_id):
        url = f"{self.base_url_api}/companies/import/{import_id}"
        response = self.session.get(url)
        return response

    def send_req_to_upgrade_to_premium(self, company_id, erp_id=None):
        url = f"{self.base_url_api}/companies/{company_id}/upgrade/request"
        query = RequestSession.make_query({
            'erpid': erp_id
        })
        response = self.session.post(url, params=query)
        return response

    def import_data_from_gen2(self, company_id, file_path):
        url = f"{self.base_url_api}/companies/{company_id}/import"
        file = open(file_path, 'rb')
        files = {'file': file}
        response = self.session.post(url, files=files)
        return response

    def set_to_trial_by_id(self, company_id):
        url = f"{self.base_url_api}/companies/{company_id}/upgrade/trial"
        response = self.session.put(url)
        return response

    def set_to_free_by_id(self, company_id):
        url = f"{self.base_url_api}/companies/{company_id}/upgrade/free"
        response = self.session.put(url)
        return response

    def set_to_premium_by_id(self, company_id, model: UpgradeCompanyPremium):
        url = f"{self.base_url_api}/companies/{company_id}/upgrade/premium"
        query = RequestSession.make_query({
            'BillingMethod': model.billing_method,
            'CrmId': model.crmid,
            'ErpId': model.erpid,
            'OrgId': model.orgid,
            'Currency': model.currency,
            'PoNumber': model.po_number,
            'OrderType': model.order_type,
            'DistributionChannel': model.distribution_channel,
            'Division': model.division,
            'PartNumber': model.part_number,
            'BillingDay': model.billing_day
        })
        response = self.session.put(url, params=query)
        return response

    def set_company_limits(self, company_id, max_alert_count):
        url = f"{self.base_url_api}/companies/{company_id}/limits"
        query = RequestSession.make_query({
            'MaxAlertCount': max_alert_count
        })
        response = self.session.put(url, params=query)
        return response