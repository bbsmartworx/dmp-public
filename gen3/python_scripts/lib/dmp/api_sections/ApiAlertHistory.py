from dmp.RequestSession import RequestSession
from dmp.models.AlertHistory import *


class ApiAlertHistory:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def delete_by_id(self, id):
        url = f"{self.base_url_api}/alert-history/{id}"
        response = self.session.delete(url)
        return response

    def find_history(self, model: FindAlertHistoryModel):
        url = f"{self.base_url_api}/alert-history"
        query = RequestSession.make_query({
            'IsAcked': model.is_acked,
            'AlertIds': model.alert_ids,
            'CompanyIds': model.company_ids,
            'DeviceIds': model.device_ids,
            'SortType': model.sort_type,
            'SortDirection': model.sort_direction,
            'Page': model.page,
            'PageSize': model.pagesize
        })
        response = self.session.get(url, params=query)
        return response

    def ack_records(self, ids):
        url = f"{self.base_url_api}/alert-history/ack"
        query = RequestSession.make_query({
            'ids': ids
        })
        response = self.session.put(url, params=query)
        return response

    def unack_records(self, ids):
        url = f"{self.base_url_api}/alert-history/unack"
        query = RequestSession.make_query({
            'ids': ids
        })
        response = self.session.put(url, params=query)
        return response