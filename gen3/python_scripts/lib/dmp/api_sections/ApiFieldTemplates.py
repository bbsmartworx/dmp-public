from dmp.RequestSession import RequestSession
from dmp.models.FieldTemplates import *


class ApiFieldTemplates:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def find_field_templates(self, model: FindFieldTemplatesModel):
        url = f"{self.base_url_api}/fields/templates"
        query = RequestSession.make_query({
            'IsReportable': model.is_reportable,
            'Name': model.name,
            'SortBy': model.sort_by,
            'SortDirection': model.sort_direction,
            'Page': model.page,
            'PageSize': model.page_size,
        })
        response = self.session.get(url, params=query)
        return response

    def get_field_template_by_id(self, id):
        url = f"{self.base_url_api}/fields/templates/{id}"
        response = self.session.get(url)
        return response