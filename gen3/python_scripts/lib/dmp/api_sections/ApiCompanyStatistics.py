from dmp.models.CompanyStatistics import *


class ApiCompanyStatistics:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_company_statistics(self, company_id, model: GetCompanyStatsModel):
        url = f"{self.base_url_api}/monitoring/companies/{company_id}/sources"
        response = self.session.post(url, json=model)
        return response

    def get_company_statistics_history(self, company_id, model: GetCompanyStatisticsHistoryModel):
        url = f"{self.base_url_api}/monitoring/companies/{company_id}/sources/history"
        response = self.session.post(url, json=model)
        return response