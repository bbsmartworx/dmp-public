from dmp.RequestSession import RequestSession
from dmp.models.DeviceIdentity import *
from dmp.models.Shared import *


class ApiDeviceIdentity:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def delete_by_mac(self, mac):
        url = f"{self.base_url_api}/identity/devices/{mac}"
        response = self.session.delete(url)
        return response

    def create(self, model: CreateDeviceModel):
        url = f"{self.base_url_api}/identity/devices"
        response = self.session.post(url, json=model)
        return response

    def claim(self, model: ClaimDeviceModel):
        url = f"{self.base_url_api}/identity/devices/claim"
        response = self.session.post(url, json=model)
        return response

    def release(self, mac_address):
        url = f"{self.base_url_api}/identity/devices/{mac_address}/release"
        response = self.session.post(url)
        return response

    def claim_devices_from_csv(self, company_id, source_file_path):
        url = f"{self.base_url_api}/identity/devices/long-operations/claim/csv"

        files = {'File': open(source_file_path, 'rb')}
        body = RequestSession.make_query({'CompanyId': company_id})

        response = self.session.post(url, files=files, data=body)
        return response

    def release_devices_from_company(self, model: FilteredRequestModel):
        url = f"{self.base_url_api}/identity/devices/long-operations/release"
        response = self.session.post(url, json=model)
        return response