from dmp.RequestSession import RequestSession
from dmp.models.Bootstrap import *


class ApiBootstrap:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_management_server_by_id(self, id):
        url = f"{self.base_url_api}/bootstrap/{id}"
        response = self.session.get(url)
        return response

    def update_management_server(self, id, model: UpdateNextHopServerModel):
        url = f"{self.base_url_api}/bootstrap/{id}"
        response = self.session.put(url, json=model)
        return response

    def delete_management_server_by_id(self, id):
        url = f"{self.base_url_api}/bootstrap/{id}"
        response = self.session.delete(url)
        return response

    def find_management_servers(self, model: GetNextHopServerModel):
        url = f"{self.base_url_api}/bootstrap"
        query = RequestSession.make_query({
            'Name': model.name,
            'Address': model.address,
            'CompanyId': model.company_id
        })
        response = self.session.get(url, params=query)
        return response

    def create_management_servers(self, model: CreateNextHopServerModel):
        url = f"{self.base_url_api}/bootstrap"
        response = self.session.post(url, json=model)
        return response