from dmp.RequestSession import RequestSession
from dmp.Enums import DataType
from dmp.models.Fields import *


class ApiFields:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_field_by_id(self, id):
        url = f"{self.base_url_api}/fields/{id}"
        response = self.session.get(url)
        return response

    def update_field(self, id, model: UpdateFieldModel):
        url = f"{self.base_url_api}/fields/{id}"
        response = self.session.put(url, json=model)
        return response

    def delete_field_by_id(self, id):
        url = f"{self.base_url_api}/fields/{id}"
        response = self.session.delete(url)
        return response

    def find_fields(self, model: FindFieldsModel):
        url = f"{self.base_url_api}/fields"
        query = RequestSession.make_query({
            'CompanyId': model.company_id,
            'IsReportable': model.is_reportable,
            'Name': model.name,
            'SortBy': model.sort_by,
            'SortDirection': model.sort_direction,
            'Page': model.page,
            'PageSize': model.page_size
        })
        response = self.session.get(url, params=query)
        return response

    def create_field(self, model: CreateFieldModel):
        url = f"{self.base_url_api}/fields"
        response = self.session.post(url, json=model)
        return response

    def get_input_options(self, field_ids):
        url = f"{self.base_url_api}/fields/input-options"
        query = RequestSession.make_query({
            'ids': field_ids
        })
        response = self.session.get(url, params=query)
        return response

    def get_operators_for_type(self, search_type: DataType):
        url = f"{self.base_url_api}/fields/operators-for-types"
        query = RequestSession.make_query({
            'searchType': search_type
        })
        response = self.session.get(url, params=query)
        return response