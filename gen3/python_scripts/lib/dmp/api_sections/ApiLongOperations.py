from dmp.RequestSession import RequestSession
from dmp.models.LongOperations import FindLongOperations


class ApiLongOperations:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def find_long_operations(self, model: FindLongOperations):
        url = f"{self.base_url_api}/long-operations"
        response = self.session.get(url, params=model)
        return response

    def get_by_id(self, operation_id):
        url = f"{self.base_url_api}/long-operations/{operation_id}"
        response = self.session.get(url)
        return response

    def cancel_by_id(self, operation_id):
        url = f"{self.base_url_api}/long-operations/{operation_id}/cancel"
        response = self.session.put(url)
        return response