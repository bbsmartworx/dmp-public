from dmp.RequestSession import RequestSession
from dmp.models.Users import *


class ApiUsers:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_by_id(self, user_id):
        url = f"{self.base_url_api}/users/{user_id}"
        response = self.session.get(url)
        return response

    def update_user_companies(self, user_id, model: CreateOrUpdateUserCompaniesModel):
        url = f"{self.base_url_api}/users/{user_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_by_id(self, user_id):
        url = f"{self.base_url_api}/users/{user_id}"
        response = self.session.delete(url)
        return response

    def remove_user_from_companies(self, user_id, company_ids):
        url = f"{self.base_url_api}/users/{user_id}/companies"
        query = RequestSession.make_query({
            'CompanyIds' : company_ids
        })
        response = self.session.delete(url, params=query)
        return response

    def find_users(self, model: FindUsersModel):
        url = f"{self.base_url_api}/users"
        query = RequestSession.make_query({
            'CompanyId' : model.company_id,
            'FirstName' : model.first_name,
            'LastName' : model.last_name,
            'Email' : model.email,
            'DateOfCreationAfter' : model.date_of_creation_after,
            'DateOfCreationBefore' : model.date_of_creation_before,
            'Sort' : model.sort,
            'SortDirection' : model.sort_direction,
            'Page' : model.page,
            'PageSize' : model.page_size
        })
        response = self.session.get(url, params=query)
        return response

    def create(self, model: CreateUserModel):
        url = f"{self.base_url_api}/users"
        response = self.session.post(url, json=model)
        return response

    def resend_user_confirmation_email_by_id(self, user_id):
        url = f"{self.base_url_api}/users/{user_id}/resend-confirmation-email"
        response = self.session.post(url)
        return response

    def invite_user_to_companies(self, model: CreateUserInviteModel):
        url = f"{self.base_url_api}/users/invite"
        response = self.session.post(url, json=model)
        return response

    def update_user_profile_by_id(self, user_id, model: UpdateUserProfileModel):
        url = f"{self.base_url_api}/users/{user_id}/profile"
        response = self.session.put(url, json=model)
        return response

    def update_user_eula_confirmation(self, user_id):
        url = f"{self.base_url_api}/users/{user_id}/eula-confirmation"
        response = self.session.put(url)
        return response

    def set_system_flags_for_user(self, user_id, is_sys_admin, is_manufacturer):
        url = f"{self.base_url_api}/users/{user_id}/system-flags"
        query = RequestSession.make_query({
            'IsSysAdmin': is_sys_admin,
            'IsManufacturer': is_manufacturer
        })
        response = self.session.put(url, params=query)
        return response