from dmp.models.ApiClients import *


class ApiClients:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_api_client_by_id(self, api_client_id):
        url = f"{self.base_url_api}/api-clients/{api_client_id}"
        response = self.session.get(url)
        return response

    def edit_client(self, api_client_id, model: UpdateAPIClientModel):
        url = f"{self.base_url_api}/api-clients/{api_client_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_by_id(self, api_client_id):
        url = f"{self.base_url_api}/api-clients/{api_client_id}"
        response = self.session.delete(url)
        return response

    def find_clients(self):
        url = f"{self.base_url_api}/api-clients"
        response = self.session.get(url)
        return response

    def create(self, model: CreateAPIClientModel):
        url = f"{self.base_url_api}/api-clients"
        response = self.session.post(url, json=model)
        return response