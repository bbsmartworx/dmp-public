

class ApiCommands:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_history_of_commands_by_mac(self, mac_address):
        url = f"{self.base_url_api}/commands/devices/{mac_address}"
        response = self.session.get(url)
        return response

    def get_command_by_id(self, command_id):
        url = f"{self.base_url_api}/commands/{command_id}"
        response = self.session.get(url)
        return response

    def abort_command_by_id(self, command_id):
        url = f"{self.base_url_api}/commands/{command_id}/abort"
        response = self.session.put(url)
        return response
