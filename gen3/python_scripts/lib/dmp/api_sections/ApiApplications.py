from dmp.RequestSession import RequestSession
from dmp.models.Applications import *


class ApiApplications:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_app_by_id(self, app_id):
        url = f"{self.base_url_api}/applications/{app_id}"
        response = self.session.get(url)
        return response

    def edit_app(self, app_id, model: UpdateApplicationModel):
        url = f"{self.base_url_api}/applications/{app_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_by_id(self, app_id):
        url = f"{self.base_url_api}/applications/{app_id}"
        response = self.session.delete(url)
        return response

    def get_app_version(self, app_version_id, device_type_id=None):
        url = f"{self.base_url_api}/applications/versions/{app_version_id}"
        query = RequestSession.make_query({
            'deviceTypeId': device_type_id
        })
        response = self.session.get(url, params=query)
        return response

    def edit_app_version(self, app_version_id, model: UpdateApplicationVersionModel):
        url = f"{self.base_url_api}/applications/versions/{app_version_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_app_version(self, app_version_id):
        url = f"{self.base_url_api}/applications/versions/{app_version_id}"
        response = self.session.delete(url)
        return response

    def find_apps(self, model: FindApplicationModel):
        url = f"{self.base_url_api}/applications"
        query = RequestSession.make_query({
            'Name': model.name,
            'DeviceTypeId': model.device_type_id,
            'IncludeVersions': model.include_versions,
            'IncludeHidden': model.include_hidden,
            'Page': model.page,
            'PageSize': model.page_size
        })
        response = self.session.get(url, params=query)
        return response

    def create_new_app(self, model: CreateApplicationModel):
        url = f"{self.base_url_api}/applications"
        response = self.session.post(url, json=model)
        return response

    def find_distinct_apps(self, model: FindDistinctApplicationsModel):
        url = f"{self.base_url_api}/applications/distinct"
        response = self.session.get(url, params=model)
        return response

    def find_versions_of_app(self, app_id, device_type_id=None, include_hidden=True):
        url = f"{self.base_url_api}/applications/{app_id}/versions"
        query = RequestSession.make_query({
            'deviceTypeId': device_type_id,
            'includeHidden': include_hidden
        })
        response = self.session.get(url, params=query)
        return response

    def create_new_app_version(self, app_id, model: CreateApplicationVersionModel):
        url = f"{self.base_url_api}/applications/{app_id}/versions"
        response = self.session.post(url, json=model)
        return response