from typing import List
from dmp.RequestSession import RequestSession
from dmp.models.DeviceMonitoring import *


class ApiDeviceMonitoring:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_device_history_fields(self, mac, model: GetDeviceFieldsHistoryModel):
        url = f"{self.base_url_api}/monitoring/devices/{mac}/history"
        response = self.session.post(url, json=model)
        return response

    def get_by_mac(self, mac, fields: List[str] = None):
        url = f"{self.base_url_api}/monitoring/devices/{mac}"
        query = RequestSession.make_query({
            'fields': fields
        })
        response = self.session.get(url, params=query)
        return response

    def find_devices(self, model: GetDevicesModel):
        url = f"{self.base_url_api}/monitoring/devices"
        query = RequestSession.make_query({
            'CompanyId': model.company_id,
            'MacAddress': model.mac_address,
            'Imei': model.imei,
            'Serial': model.serial,
            'DeviceType': model.device_type,
            'Page': model.page,
            'PageSize': model.page_size
        })
        response = self.session.get(url, params=query)
        return response

    def get_claimed_devices(self, company_id, model: FindDevicesModel):
        url = f"{self.base_url_api}/monitoring/devices/companies/{company_id}"
        response = self.session.post(url, json=model)
        return response

    def get_distinct_values(self, company_id, model: GetDistinctValuesModel):
        url = f"{self.base_url_api}/monitoring/devices/companies/{company_id}/distinct-values"
        response = self.session.post(url, json=model)
        return response