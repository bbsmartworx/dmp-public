from requests import Response
from dmp.RequestSession import RequestSession
from dmp.models.ConfigProfile import *


class ApiConfigProfile:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_config_profile_base_by_id(self, id):
        url = f"{self.base_url_api}/config-profile-bases/{id}"
        response = self.session.get(url)
        return response

    def update_config_profile_base(self, id, model: UpdateConfigProfileBaseModel):
        url = f"{self.base_url_api}/config-profile-bases/{id}"
        response = self.session.put(url, json=model)
        return response

    def delete_by_id(self, id):
        url = f"{self.base_url_api}/config-profile-bases/{id}"
        response = self.session.delete(url)
        return response

    def find_config_profile_bases(self, company_id, name=None):
        url = f"{self.base_url_api}/config-profile-bases"
        query = RequestSession.make_query({
            'companyId': company_id,
            'name': name
        })
        response = self.session.get(url, params=query)
        return response

    def create_config_profile(self, model: CreateConfigProfileBaseModel):
        url = f"{self.base_url_api}/config-profile-bases"
        response = self.session.post(url, json=model)
        return response

    def get_config_profile_base_by_mac(self, mac):
        url = f"{self.base_url_api}/devices/{mac}/config-profile"
        response = self.session.get(url)
        return response

    def update_default_config_profile_by_mac(self, mac: str, model: UpsertConfigProfileRequestModel) -> Response:
        url = f"{self.base_url_api}/devices/{mac}/default-config-profile-base"
        response = self.session.post(url, json=model)
        return response