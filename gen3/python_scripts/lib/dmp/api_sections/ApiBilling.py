from dmp.RequestSession import RequestSession
from dmp.models.Billing import *


class ApiBilling:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_invoice_by_id(self, invoice_id):
        url = f"{self.base_url_api}/billing/invoices/{invoice_id}"
        response = self.session.get(url)
        return response

    def delete_invoice_by_id(self, invoice_id, remove_sent_invoice=False):
        url = f"{self.base_url_api}/billing/invoices/{invoice_id}"
        query = RequestSession.make_query({
            'removeSentInvoice': remove_sent_invoice
        })
        response = self.session.delete(url, params=query)
        return response

    def find_list_of_invoices(self, model: FindInvoicesModel):
        url = f"{self.base_url_api}/billing/invoices"
        response = self.session.get(url, params=model)
        return response

    def generate_new_invoice(self, model: CreateInvoiceModel):
        url = f"{self.base_url_api}/billing/invoices"
        response = self.session.post(url, json=model)
        return response

    def send_invoice_to_external_provider_by_id(self, invoice_id):
        url = f"{self.base_url_api}/billing/invoices/send/{invoice_id}"
        response = self.session.put(url)
        return response

    def send_invoices_to_external_provider(self, model: SendToExternalProviderModel):
        url = f"{self.base_url_api}/billing/invoices/send"
        response = self.session.put(url, params=model)
        return response
