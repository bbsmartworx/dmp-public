from dmp.RequestSession import RequestSession
from dmp.models.Views import *


class ApiViews:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_view_by_id(self, id):
        url = f"{self.base_url_api}/views/{id}"
        response = self.session.get(url)
        return response

    def update_view_by_id(self, id, model: UpdateViewModel):
        url = f"{self.base_url_api}/views/{id}"
        response = self.session.put(url, json=model)
        return response

    def delete_view_by_id(self, id):
        url = f"{self.base_url_api}/views/{id}"
        response = self.session.delete(url)
        return response

    def find_views(self, model: FindViewsModel):
        url = f"{self.base_url_api}/views"
        query = RequestSession.make_query({
            'CompanyId': model.company_id,
            'SearchName': model.search_name,
            'SearchType': model.search_type,
            'Page': model.page,
            'PageSize': model.page_size,
        })
        response = self.session.get(url, params=query)
        return response

    def create_view(self, model: CreateViewModel):
        url = f"{self.base_url_api}/views"
        response = self.session.post(url, json=model)
        return response

    def get_aggregation_types(self):
        url = f"{self.base_url_api}/views/aggregation-types"
        response = self.session.get(url)
        return response