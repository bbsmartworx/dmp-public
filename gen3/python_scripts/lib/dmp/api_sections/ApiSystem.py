

class ApiSystem:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_system_statistics(self):
        url = f"{self.base_url_api}/system/monitoring"
        response = self.session.get(url)
        return response

    def get_version_of_dmp_app(self):
        url = f"{self.base_url_api}/system/version"
        response = self.session.get(url)
        return response