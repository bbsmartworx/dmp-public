from dmp.RequestSession import RequestSession
from dmp.models.DeviceManagement import *
from dmp.models.Shared import *


class ApiDeviceManagement:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def reboot_by_mac(self, mac_address):
        url = f"{self.base_url_api}/management/devices/{mac_address}/reboot"
        response = self.session.post(url)
        return response

    def rebootstrap_device_by_mac(self, mac_address):
        url = f"{self.base_url_api}/management/devices/{mac_address}/rebootstrap"
        response = self.session.post(url)
        return response

    def create_snapshot(self, mac_address, name, is_single_use = False):
        url = f"{self.base_url_api}/management/devices/{mac_address}/snapshot"
        query = RequestSession.make_query({
            'Name': name,
            'IsSingleUse': is_single_use
        })
        response = self.session.post(url, params=query)
        return response

    def create_single_use_config_profile(self, mac_address):
        url = f"{self.base_url_api}/management/devices/{mac_address}/single-use-snapshot"
        response = self.session.post(url)
        return response

    def resend_config_profile(self, mac_address):
        url = f"{self.base_url_api}/management/devices/{mac_address}/resend-config"
        response = self.session.post(url)
        return response

    def update_device_fields_by_mac(self, mac_address, model: UpdateDeviceModel):
        url = f"{self.base_url_api}/management/devices/{mac_address}"
        response = self.session.post(url, json=model)
        return response

    def update_next_hop_server_and_rebootstrap(self, model: SetNextHopServerModel):
        url = f"{self.base_url_api}/management/devices/next-hop-server"
        response = self.session.post(url, json=model)
        return response

    def update_device_fields(self, model: UpdateDevicesRequestModel):
        url = f"{self.base_url_api}/management/devices/long-operations/fields"
        response = self.session.post(url, json=model)
        return response

    def update_devices_from_csv(self, source_file_path, company_id=None):
        url = f"{self.base_url_api}/management/devices/long-operations/fields/csv"

        body = RequestSession.make_query({'CompanyId': company_id})
        files = {'File': open(source_file_path, 'rb')}

        response = self.session.post(url, files=files, data=body)
        return response

    def reboot_devices(self, model: FilteredRequestModel):
        url = f"{self.base_url_api}/management/devices/long-operations/reboot"
        response = self.session.post(url, json=model)
        return response

    def rebootstrap_devices(self, model: FilteredRequestModel):
        url = f"{self.base_url_api}/management/devices/long-operations/rebootstrap"
        response = self.session.post(url, json=model)
        return response