from dmp.RequestSession import RequestSession
from dmp.models.Files import *


class ApiFiles:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_by_id(self, file_id):
        url = f"{self.base_url_api}/files/{file_id}"
        response = self.session.get(url)
        return response

    def update_by_id(self, file_id, source_file_path, file_name=None, company_id=None):
        url = f"{self.base_url_api}/files/{file_id}"

        query = RequestSession.make_query({'name': file_name})
        body = {'CompanyId': company_id}
        files = {'file': open(source_file_path, 'rb')}

        response = self.session.put(url, params=query, files=files, data=body)
        return response

    def delete_by_id(self, file_id):
        url = f"{self.base_url_api}/files/{file_id}"
        response = self.session.delete(url)
        return response

    def find_files(self, name=None):
        url = f"{self.base_url_api}/files"
        query = RequestSession.make_query({
            'FileName': name,
        })
        response = self.session.get(url, params=query)
        return response

    def upload_file(self, source_file_path, file_name=None, company_id=None):
        url = f"{self.base_url_api}/files"

        query = RequestSession.make_query({'name': file_name})
        body = RequestSession.make_query({'CompanyId': company_id})
        files = {'file': open(source_file_path, 'rb')}

        response = self.session.post(url, params=query, files=files, data=body)
        return response

    def update_device_types_for_application_version(self, file_id, app_version_id, device_types):
        url = f"{self.base_url_api}/files/{file_id}/applications/versions/{app_version_id}/device-types"
        response = self.session.put(url, json=device_types)
        return response
