from dmp.RequestSession import RequestSession
from dmp.models.AlertEndpoints import *


class ApiAlertEndpoints:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def find_endpoints(self, model: FindAlertEndpointModel):
        url = f"{self.base_url_api}/alert-endpoints"
        query = RequestSession.make_query({
            'Name': model.name,
            'CompanyIds': model.company_ids,
            'Page': model.page,
            'PageSize': model.pagesize
        })
        response = self.session.get(url, params=query)
        return response

    def get_by_id(self, endpoint_id):
        url = f"{self.base_url_api}/alert-endpoints/{endpoint_id}"
        response = self.session.get(url)
        return response

    def delete_by_id(self, endpoint_id):
        url = f"{self.base_url_api}/alert-endpoints/{endpoint_id}"
        response = self.session.delete(url)
        return response

    def create(self, model: CreateAlertEndpointModel):
        url = f"{self.base_url_api}/alert-endpoints"
        response = self.session.post(url, json=model)
        return response

    def update_by_id(self, endpoint_id, model: UpdateAlertEndpointModel):
        url = f"{self.base_url_api}/alert-endpoints/{endpoint_id}"
        response = self.session.put(url, json=model)
        return response