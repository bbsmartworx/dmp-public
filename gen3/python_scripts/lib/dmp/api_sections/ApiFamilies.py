from dmp.RequestSession import RequestSession  
from dmp.models.Families import *


class ApiFamilies:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_by_id(self, family_id):
        url = f"{self.base_url_api}/device-families/{family_id}"
        response = self.session.get(url)
        return response

    def delete_by_id(self, family_id):
        url = f"{self.base_url_api}/device-families/{family_id}"
        response = self.session.delete(url)
        return response

    def find_families(self, model: FindFamiliesModel):
        url = f"{self.base_url_api}/device-families"
        query = RequestSession.make_query({
            'name': model.name,
            'includeTypes': model.include_types
        })
        response = self.session.get(url, params=query)
        return response

    def create(self, model: CreateFamilyModel):
        url = f"{self.base_url_api}/device-families"
        response = self.session.post(url, json=model)
        return response