from dmp.RequestSession import RequestSession
from dmp.models.Sources import *


class ApiSources:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_api = base_url_api

    def get_source_by_id(self, source_id):
        url = f"{self.base_url_api}/sources/{source_id}"
        response = self.session.get(url)
        return response

    def update_source_by_id(self, source_id, model: UpdateSourceModel):
        url = f"{self.base_url_api}/sources/{source_id}"
        response = self.session.put(url, json=model)
        return response

    def delete_source_by_id(self, source_id):
        url = f"{self.base_url_api}/sources/{source_id}"
        response = self.session.delete(url)
        return response

    def find_sources(self, model: FindSourceModel):
        url = f"{self.base_url_api}/sources"
        query = RequestSession.make_query({
            'CompanyId': model.company_id,
            'SearchName': model.search_name,
            'SearchTypes': model.search_types,
            'Page': model.page,
            'PageSize': model.pagesize
        })
        response = self.session.get(url, params=query)
        return response

    def create_source(self, model: AddCountingSourceModel):
        url = f"{self.base_url_api}/sources"
        response = self.session.post(url, json=model)
        return response