from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class CreateDeviceModel(PayloadBase):
    def __init__(self, name=None, serial=None, mac_address=None, device_type_id=None,
                 order_code=None, imei=None):
        super().__init__(
            name = name,
            serial = serial,
            mac_address = mac_address,
            device_type_id = device_type_id,
            order_code = order_code,
            imei = imei
        )

class ClaimDeviceModel(PayloadBase):
    def __init__(self, name=None, serial=None, mac_address=None, imei=None, company_id=None):
        super().__init__(
            name = name,
            serial = serial,
            mac_address = mac_address,
            imei = imei,
            company_id = company_id
        )