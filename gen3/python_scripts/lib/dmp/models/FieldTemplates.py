from .Shared import PayloadBase

##############################################################################################
# MODELS #
##############################################################################################
class FindFieldTemplatesModel(PayloadBase):
    def __init__(self, is_reportable=None, name=None, sort_by=None, sort_direction=None,
                 page=None, page_size=None):
        super().__init__(
            is_reportable=is_reportable,
            name=name,
            sort_by=sort_by,
            sort_direction=sort_direction,
            page=page,
            page_size=page_size,
        )