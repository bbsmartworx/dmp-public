from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class CreateUserModel(PayloadBase):
    def __init__(self, first_name=None, last_name=None, email=None, companies=None, password=None):
        super().__init__(
            first_name = first_name,
            last_name = last_name,
            email = email,
            companies = companies,
            password = password
        )

class UpdateUserProfileModel(PayloadBase):
    def __init__(self, first_name=None, last_name=None, email=None):
        super().__init__(
            first_name = first_name,
            last_name = last_name,
            email = email
        )

class CreateOrUpdateUserCompaniesModel(PayloadBase):
    def __init__(self, companies=None):
        super().__init__(
            companies = companies
        )

class CreateOrUpdateUserCompanyModel(PayloadBase):
    def __init__(self, company_id=None, permissions=None, is_company_admin=None, is_service_account=None):
        super().__init__(
            company_id = company_id,
            permissions = permissions,
            is_company_admin = is_company_admin,
            is_service_account = is_service_account
        )

class CreateUserInviteModel(PayloadBase):
    def __init__(self, email=None, companies=None):
        super().__init__(
            email = email,
            companies = companies
        )

class FindUsersModel(PayloadBase):
    def __init__(self, company_id=None, first_name=None, last_name=None, email=None, date_of_creation_after=None,
                 date_of_creation_before=None, sort=None, sort_direction=None, page=None, page_size=None):
        super().__init__(
            company_id = company_id,
            first_name = first_name,
            last_name = last_name,
            email = email,
            date_of_creation_after = date_of_creation_after,
            date_of_creation_before = date_of_creation_before,
            sort = sort,
            sort_direction = sort_direction,
            page = page,
            page_size = page_size
        )