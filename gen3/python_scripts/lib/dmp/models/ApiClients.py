from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class CreateAPIClientModel(PayloadBase):
    def __init__(self, name=None, grant_type=None, redirect_uri=None, post_logout_redirect_uri=None,
                 allowed_cors_uri=None, company_id=None):
        super().__init__(
            name = name,
            grant_type = grant_type,
            redirect_uri = redirect_uri,
            post_logout_redirect_uri = post_logout_redirect_uri,
            allowed_cors_uri = allowed_cors_uri,
            company_id = company_id
        )

class UpdateAPIClientModel(PayloadBase):
    def __init__(self, name=None, grant_type=None, redirect_uri=None, post_logout_redirect_uri=None,
                 allowed_cors_uri=None, company_id=None):
        super().__init__(
            name = name,
            grant_type = grant_type,
            redirect_uri = redirect_uri,
            post_logout_redirect_uri = post_logout_redirect_uri,
            allowed_cors_uri = allowed_cors_uri,
            company_id = company_id
        )