from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class CreateInvoiceModel(PayloadBase):
    def __init__(self, month=None, year=None, rate_per_device_period=None, company_id=None):
        super().__init__(
            month = month,
            year = year,
            rate_per_device_period = rate_per_device_period,
            company_id = company_id
        )

class FindInvoicesModel(PayloadBase):
    def __init__(self, include_request_details=None, company_id=None, company_name=None, status=None,
                 billing_method=None, sort_by=None, sort_direction=None, page=None, page_size=None):
        super().__init__(
            IncludeRequestDetails = include_request_details,
            CompanyId = company_id,
            CompanyName = company_name,
            Status = status,
            BillingMethod = billing_method,
            SortBy = sort_by,
            SortDirection = sort_direction,
            Page = page,
            PageSize = page_size
        )

class SendToExternalProviderModel(PayloadBase):
    def __init__(self, month=None, year=None):
        super().__init__(
            month = month,
            year = year
        )