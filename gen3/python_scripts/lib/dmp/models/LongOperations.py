from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class FindLongOperations(PayloadBase):
    def __init__(self, company_id=None, state=None, type=None):
        super().__init__(
            CompanyId = company_id,
            State = state,
            Type = type
        )