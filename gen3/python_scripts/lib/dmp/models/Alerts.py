from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class CreateAlertModel(PayloadBase):
    def __init__(self, name=None, company_id = None, mac_address = None, is_enabled = None, cooldown = None,
                 period = None, critical_matched_records_count = None, critical_repetitions = None, 
                 source_id = None, filtering_rule = None, endpoints = None):
        super().__init__(
            name = name,
            company_id = company_id,
            mac_address = mac_address,
            is_enabled = is_enabled,
            cooldown = cooldown,
            period = period,
            critical_matched_records_count = critical_matched_records_count,
            critical_repetitions = critical_repetitions,
            source_id = source_id,
            filtering_rule = filtering_rule,
            endpoints = endpoints,
        )

class UpdateAlertModel(PayloadBase):
    def __init__(self, name=None, is_enabled = None, cooldown = None, period = None, 
                 critical_matched_records_count = None, critical_repetitions = None, source_id = None,
                 filtering_rule = None, endpoints = None):
        super().__init__(
            name = name,
            is_enabled = is_enabled,
            cooldown = cooldown,
            period = period,
            critical_matched_records_count = critical_matched_records_count,
            critical_repetitions = critical_repetitions,
            source_id = source_id,
            filtering_rule = filtering_rule,
            endpoints = endpoints,
        )

class FindAlertModel(PayloadBase):
    def __init__(self, name=None, company_ids=None, device_mac_addresses=None, alert_sorting_type=None,
                 sort_direction=None, page=None, pagesize=None):
        super().__init__(
            name=name,
            company_ids=company_ids,
            device_mac_addresses=device_mac_addresses,
            alert_sorting_type=alert_sorting_type,
            sort_direction=sort_direction,
            page=page,
            pagesize=pagesize,
        )