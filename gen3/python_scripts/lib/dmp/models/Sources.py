from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class AddCountingSourceModel(PayloadBase):
    def __init__(self, company_id=None, field_name=None, rule=None):
        super().__init__(
            company_id = company_id,
            field_name = field_name,
            rule = rule
        )

class UpdateSourceModel(PayloadBase):
    def __init__(self, set_tracking_enabled=None, tracking_mode=None, name=None):
        super().__init__(
            set_tracking_enabled = set_tracking_enabled,
            tracking_mode = tracking_mode,
            name = name
        )

class FindSourceModel(PayloadBase):
    def __init__(self, company_id=None, search_name=None, search_types=None, page=None, pagesize=None):
        super().__init__(
            company_id = company_id,
            search_name=search_name,
            search_types=search_types,
            page=page,
            pagesize=pagesize,
        )
