from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class FindAlertHistoryModel(PayloadBase):
    def __init__(self, is_acked=None, alert_ids=None, company_ids=None, device_ids=None, 
                 sort_type=None, sort_direction=None, page=None, pagesize=None):
        super().__init__(
            is_acked=is_acked,
            alert_ids=alert_ids,
            company_ids=company_ids,
            device_ids=device_ids,
            sort_type=sort_type,
            sort_direction=sort_direction,
            page=page,
            pagesize=pagesize,
        )