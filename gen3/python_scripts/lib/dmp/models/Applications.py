from typing import List
from .Shared import PayloadBase
from ..Enums import DataType

##############################################################################################################
# MODELS #
##############################################################################################################
class CreateApplicationModel(PayloadBase):
    def __init__(self, name=None, display_name=None, description=None,
                 is_global=True, is_firmware=False, companies=None):
        super().__init__(
            name = name,
            display_name = display_name,
            description = description,
            is_global = is_global,
            is_firmware = is_firmware,
            companies = companies
        )

class UpdateApplicationModel(PayloadBase):
    def __init__(self, name=None, display_name=None, description=None,
                 is_global=True, is_firmware=False, companies=None):
        super().__init__(
            name = name,
            display_name = display_name,
            description = description,
            is_global = is_global,
            is_firmware = is_firmware,
            companies = companies
        )

class FindApplicationModel(PayloadBase):
    def __init__(self, name=None, device_type_id=None, include_versions=None,
                 include_hidden=None, page=None, page_size=None):
        super().__init__(
            name = name,
            device_type_id = device_type_id,
            include_versions = include_versions,
            include_hidden = include_hidden,
            page = page,
            page_size = page_size
        )

class FindDistinctApplicationsModel(PayloadBase):
    def __init__(self, name=None, include_hidden=None, page=None, page_size=None):
        super().__init__(
            Name = name,
            IncludeHidden = include_hidden,
            Page = page,
            PageSize = page_size
        )

class DetailApplicationCompanyModel(PayloadBase):
    def __init__(self, id=None):
        super().__init__(
            id = id
        )

class DetailSectionSettingOptionModel(PayloadBase):
    def __init__(self, value=None, name=None):
        super().__init__(
            value = value,
            name = name
        )

class DetailSectionSettingModel(PayloadBase):
    def __init__(self, name: str = None, default: str = None, type: DataType = None,
                 options: List[DetailSectionSettingOptionModel] = None):
        super().__init__(
            name = name,
            default = default,
            type = type,
            options = options
        )

class DetailApplicationVersionSection(PayloadBase):
    def __init__(self, name: str = None, settings: List[DetailSectionSettingModel] = None):
        super().__init__(
            name = name,
            settings = settings
        )

class CreateApplicationVersionModel(PayloadBase):
    def __init__(self, version: str = None, is_hidden: bool = None, sections: List[DetailApplicationVersionSection] = None):
        super().__init__(
            version = version,
            is_hidden = is_hidden,
            sections = sections
        )

class UpdateApplicationVersionModel(PayloadBase):
    def __init__(self, version: str = None, is_hidden: bool = None, sections: List[DetailApplicationVersionSection] = None):
        super().__init__(
            version = version,
            is_hidden = is_hidden,
            sections = sections
        )