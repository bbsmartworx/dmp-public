from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class GetCompanyStatsModel(PayloadBase):
    def __init__(self, filters=None, sources=None):
        super().__init__(
            filters=filters,
            sources=sources
        )

class GetCompanyStatisticsHistoryModel(PayloadBase):
    def __init__(self, sources=None, sampling_operator=None, start=None, stop=None, sampling_period = 3600):
        super().__init__(
            start=start,
            stop=stop,
            sampling_period=sampling_period,
            sampling_operator=sampling_operator,
            sources=sources,
        )