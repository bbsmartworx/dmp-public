from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class FindAuditLogsModel(PayloadBase):
    def __init__(self, mac_address=None, user_id=None, companies=None, include_empty_company=None,
                 start=None, end=None, action_types=None, page=None, page_size=None):
        super().__init__(
            mac_address = mac_address,
            user_id = user_id,
            companies = companies,
            include_empty_company = include_empty_company,
            start = start,
            end = end,
            action_types = action_types,
            page = page,
            page_size = page_size
        )