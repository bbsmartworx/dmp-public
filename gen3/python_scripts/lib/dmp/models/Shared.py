import json
from dmp.utils.wrappers import *
from dmp.Enums import FieldNames

##############################################################################################################
# MODEL WRAPPER #
##############################################################################################################
class PayloadBase(dict):
    def __init__(self, **kwargs):
        for arg, value in kwargs.items():
            self[arg] = value

    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def __setattr__(self, name, value):
        self[name] = value

    def json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)
    
##############################################################################################################
# MODELS #
##############################################################################################################
class FieldFilterModel(PayloadBase):
    def __init__(self, rule=None, field_name=None):
        super().__init__(
            rule=rule,
            field_name=field_name
        )

class FilteringRuleModel(PayloadBase):
    def __init__(self, operator_id=None, operands=None):
        super().__init__(
            operator_id=operator_id,
            operands=operands
        )

class FilteredRequestModel(PayloadBase):
    def __init__(self, company_id=None, filters=None):
        super().__init__(
            company_id = company_id,
            filters = filters
        )

class SortingModel(PayloadBase):
    def __init__(self, order=None, direction=None):
        super().__init__(
            order = order,
            direction = direction
        )

class DeviceModel(PayloadBase):
    def __init__(self, data, config_data=None):
        super().__init__(
            mo_sim                 = data.get(FieldNames.MoSim.value),
            mo_operator            = data.get(FieldNames.MoOperator.value),
            mo_technology          = data.get(FieldNames.MoTechnology.value),

            is_online              = data.get(FieldNames.Online.value),
            sync_status            = data.get(FieldNames.SyncStatus.value),
            online_offline_since   = data.get(FieldNames.OnlineOfflineSince.value),
            reconect_count         = data.get(FieldNames.ReconnectsCount.value),

            id                     = data.get(FieldNames.Id.value),
            mac                    = data.get(FieldNames.MacAddress.value),
            name                   = data.get(FieldNames.Name.value),
            description            = data.get(FieldNames.Description.value),
            serial                 = data.get(FieldNames.Serial.value),
            imei                   = data.get(FieldNames.Imei.value),
            device_type            = data.get(FieldNames.DeviceType.value),
            config_profile_base_id = data.get(FieldNames.ConfigProfileBaseId.value),
            creation_date          = data.get(FieldNames.CreationDate.value),
            claimed_date           = data.get(FieldNames.ClaimedDate.value),

            cumul_data_total       = data.get(FieldNames.DataMixedC.value),
            data_down              = data.get(FieldNames.DataDownR.value),
            data_up                = data.get(FieldNames.DataUpR.value),

            company_id             = data.get(FieldNames.CompanyId.value),

            config_profile         = json.loads(config_data) if config_data else None,

            firmware_version       = self.get_firmware(config_data, Constants.version.value),
            firmware_url           = self.get_firmware(config_data, Constants.url.value),

            apps_installed         = self.get_apps(config_data),
            wadmp_app_version      = self.get_wadmp_client(config_data, Constants.version.value),
            wadmp_app_url          = self.get_wadmp_client(config_data, Constants.url.value)
        )

    def get_firmware(self, config_data, key):
        if config_data:
            json_object = json.loads(config_data)
            if firmware := get_firmware(get_system(json_object)):
                return firmware.get(key)

    def get_apps(self, config_data):
        if config_data:
            json_object = json.loads(config_data)

            if get_apps(json_object):
                apps_installed = []
                for k, v in get_apps(json_object).items():
                    if k != Constants.wadmp_client.value:
                        apps_installed.append(ModelWrapper(name=k, version=get_version(v), url=get_url(v)))
                return apps_installed

    def get_wadmp_client(self, config_data, key):
        if config_data:
            json_object = json.loads(config_data)
            if get_apps(json_object):
                for k, v in get_apps(json_object).items():
                    if k == Constants.wadmp_client.value:
                        return v.get(key)