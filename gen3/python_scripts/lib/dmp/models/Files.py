from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class DetailFileModel(PayloadBase):
    def __init__(self, id=None, file_name=None, file_size=None, url=None):
        super().__init__(
            id = id,
            file_name = file_name,
            file_size = file_size,
            url = url
        )