from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class CreateFamilyModel(PayloadBase):
    def __init__(self, name=None, description=None):
        super().__init__(
            name = name,
            description = description
        )

class FindFamiliesModel(PayloadBase):
    def __init__(self, name=None, include_types=None):
        super().__init__(
            name = name,
            include_types = include_types
        )