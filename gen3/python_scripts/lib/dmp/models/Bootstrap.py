from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class CreateNextHopServerModel(PayloadBase):
    def __init__(self, name=None, address=None, port=None, root_ca=None, company_id=None, type=None):
        super().__init__(
            name = name,
            address = address,
            port = port,
            root_ca = root_ca,
            company_id = company_id,
            type = type,
        )

class UpdateNextHopServerModel(PayloadBase):
    def __init__(self, name=None, address=None, port=None, root_ca=None, type=None):
        super().__init__(
            name = name,
            address = address,
            port = port,
            root_ca = root_ca,
            type = type,
        )

class GetNextHopServerModel(PayloadBase):
    def __init__(self, name=None, address=None, company_id=None):
        super().__init__(
            name = name,
            address = address,
            company_id = company_id,
        )