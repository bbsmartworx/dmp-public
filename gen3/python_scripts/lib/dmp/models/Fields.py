from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class CreateFieldModel(PayloadBase):
    def __init__(self, company_id=None, params=None, template_id=None, display_name=None):
        super().__init__(
            company_id = company_id,
            params = params,
            template_id = template_id,
            display_name = display_name,
        )

class UpdateFieldModel(PayloadBase):
    def __init__(self, name=None, description=None, next_reset=None, reset_period=None,
                 reset_period_unit=None, threshold=None, reported=None):
        super().__init__(
            name = name,
            description = description,
            next_reset = next_reset,
            reset_period = reset_period,
            reset_period_unit = reset_period_unit,
            threshold = threshold,
            reported = reported
        )

class FindFieldsModel(PayloadBase):
    def __init__(self, company_id=None, is_reportable=None, name=None, sort_by=None, sort_direction=None,
                 page=None, page_size=None):
        super().__init__(
            company_id=company_id,
            is_reportable=is_reportable,
            name=name,
            sort_by=sort_by,
            sort_direction=sort_direction,
            page=page,
            page_size=page_size
        )