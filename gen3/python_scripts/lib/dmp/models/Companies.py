from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class CreateCompanyModel(PayloadBase):
    def __init__(self, name=None, parent_id=None, address=None, country_iso=None, contact_phone_number=None,
                 contact_name=None, contact_email=None, is2_fa_enabled=None):
        super().__init__(
            name = name,
            parent_id = parent_id,
            address = address,
            country_iso = country_iso,
            contact_phone_number = contact_phone_number,
            contact_name = contact_name,
            contact_email = contact_email,
            is2_fa_enabled = is2_fa_enabled
        )

class UpdateCompanyModel(PayloadBase):
    def __init__(self, name=None, address=None, country_iso=None, contact_phone_number=None,
                 contact_name=None, contact_email=None, is2_fa_enabled=None):
        super().__init__(
            name = name,
            address = address,
            country_iso = country_iso,
            contact_phone_number = contact_phone_number,
            contact_name = contact_name,
            contact_email = contact_email,
            is2_fa_enabled = is2_fa_enabled
        )

class UpgradeCompanyPremium(PayloadBase):
    def __init__(self, erpid=None, orgid=None, crmid=None, billing_method=None, currency=None, po_number=None,
                 order_type=None, distribution_channel=None, division=None, part_number=None, billing_day=None):
        super().__init__(
            erpid = erpid,
            orgid = orgid,
            crmid = crmid,
            billing_method = billing_method,
            currency = currency,
            po_number = po_number,
            order_type = order_type,
            distribution_channel = distribution_channel,
            division = division,
            part_number = part_number,
            billing_day = billing_day
        )
        
class FindCompanyModel(PayloadBase):
    def __init__(self, name=None, sort=None, sort_order=None):
        super().__init__(
            name = name,
            sort = sort,
            sort_order = sort_order
        )