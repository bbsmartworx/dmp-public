from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class UpdateConfigProfileBaseModel(PayloadBase):
    def __init__(self, name=None):
        super().__init__(
            name = name
        )

class CreateConfigProfileBaseModel(PayloadBase):
    def __init__(self, company_id=None, name=None, settings=None, files=None, users=None, apps=None, system=None):
        super().__init__(
            company_id = company_id,
            name = name,
            settings = settings,
            files = files,
            users = users,
            apps = apps,
            system = system
        )

class ConfigProfileSetting(PayloadBase):
    def __init__(self, v=None):
        super().__init__(
            v = v
        )

class ConfigProfileFile(PayloadBase):
    def __init__(self, path=None, data=None):
        super().__init__(
            path = path,
            data = data
        )

class ConfigProfileUser(PayloadBase):
    def __init__(self, password=None, role=None, home_folder=None):
        super().__init__(
            password = password,
            role = role
        )

class CreateConfigProfileAppModel(PayloadBase):
    def __init__(self, version=None):
        super().__init__(
            version = version
        )

class CreateConfigProfileSystemModel(PayloadBase):
    def __init__(self, firmware=None, ub=None):
        super().__init__(
            firmware = firmware,
            ub = ub
        )

class CreateConfigProfileFirmwareModel(PayloadBase):
    def __init__(self, version=None):
        super().__init__(
            version = version
        )

class ConfigUnlistedBehavior(PayloadBase):
    def __init__(self, apps=None, users=None):
        super().__init__(
            apps = apps,
            users = users
        )

class UpsertConfigProfileRequestModel(PayloadBase):
    def __init__(self, settings=None, files=None, users=None, apps=None, system=None):
        super().__init__(
            settings    = settings,
            files       = files,
            users       = users,
            apps        = apps,
            system      = system
        )