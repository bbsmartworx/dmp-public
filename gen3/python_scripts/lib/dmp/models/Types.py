from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class CreateTypeModel(PayloadBase):
    def __init__(self, family_id=None, name=None, description=None):
        super().__init__(
            family_id = family_id,
            name = name,
            description = description
        )

class FindTypesModel(PayloadBase):
    def __init__(self, type_name=None, family_id=None):
        super().__init__(
            type_name = type_name,
            family_id = family_id
        )