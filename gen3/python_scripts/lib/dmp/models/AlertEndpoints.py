from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class CreateAlertEndpointModel(PayloadBase):
    def __init__(self, name=None, endpoint_type=None, recipient=None, company_id=None, is_enabled=None):
        super().__init__(
            name = name,
            endpoint_type = endpoint_type,
            recipient = recipient,
            company_id = company_id,
            is_enabled = is_enabled
        )

class UpdateAlertEndpointModel(PayloadBase):
    def __init__(self, name=None, endpoint_type=None, recipient=None, is_enabled=None):
        super().__init__(
            name = name,
            endpoint_type = endpoint_type,
            recipient = recipient,
            is_enabled = is_enabled
        )

class FindAlertEndpointModel(PayloadBase):
    def __init__(self, page=None, pagesize=None, name=None, company_ids=None):
        super().__init__(
            page=page,
            pagesize=pagesize,
            name=name,
            company_ids=company_ids
        )