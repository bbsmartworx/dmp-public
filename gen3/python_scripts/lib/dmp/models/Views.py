from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class CreateViewModel(PayloadBase):
    def __init__(self, company_id=None, name=None, description=None, widgets=None, table=None):
        super().__init__(
            company_id = company_id,
            name = name,
            description = description,
            widgets = widgets,
            table = table
        )

class UpdateViewModel(PayloadBase):
    def __init__(self, name=None, description=None, widgets=None, table=None):
        super().__init__(
            name = name,
            description = description,
            widgets = widgets,
            table = table
        )

class FindViewsModel(PayloadBase):
    def __init__(self, company_id=None, search_name=None, search_type=None, page=None, page_size=None):
        super().__init__(
            company_id=company_id,
            search_name=search_name,
            search_type=search_type,
            page=page,
            page_size=page_size
        )

class CreateWidgetModel(PayloadBase):
    def __init__(self, name=None, type=None, is_table_linked=None, sources=None,
                 position=None, plot=None, pie=None):
        super().__init__(
            name = name,
            type = type,
            is_table_linked = is_table_linked,
            sources = sources,
            position = position,
            plot = plot,
            pie = pie
        )

class DetailWidgetPositionModel(PayloadBase):
    def __init__(self, row=None, col=None, height=None, width=None):
        super().__init__(
            row = row,
            col = col,
            height = height,
            width = width
        )

class CreateTableWidgetModel(PayloadBase):
    def __init__(self, filters=None, columns=None):
        super().__init__(
            filters = filters,
            columns = columns
        )

class CreateTableWidgetColumnModel(PayloadBase):
    def __init__(self, field_name=None, position=None, sort=None):
        super().__init__(
            field_name = field_name,
            position = position,
            sort = sort
        )

class CreatePlotWidgetModel(PayloadBase):
    def __init__(self, type=None, default_time_span_minutes=None):
        super().__init__(
            type = type,
            default_time_span_minutes = default_time_span_minutes
        )

class DetailPieWidgetModel(PayloadBase):
    def __init__(self, ranges=None):
        super().__init__(
            ranges = ranges
        )

class DetailPieRangeModel(PayloadBase):
    def __init__(self, start=None, end=None, name=None):
        super().__init__(
            start = start,
            end = end,
            name = name
        )