from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class UpdateDeviceModel(PayloadBase):
    def __init__(self, data=None):
        super().__init__(
            data=data
        )

class DeviceFieldUpdateModel(PayloadBase):
    def __init__(self, field_name=None, value=None):
        super().__init__(
            field_name=field_name,
            value=value
        )

class UpdateDevicesRequestModel(PayloadBase):
    def __init__(self, company_id=None, field_name=None, value=None, filters=None):
        super().__init__(
            company_id=company_id,
            field_name=field_name,
            value=value,
            filters=filters
        )

class SetNextHopServerModel(PayloadBase):
    def __init__(self, mac_address=None, imei=None, serial=None, next_hop_server_id=None):
        super().__init__(
            mac_address=mac_address,
            imei=imei,
            serial=serial,
            next_hop_server_id=next_hop_server_id
        )