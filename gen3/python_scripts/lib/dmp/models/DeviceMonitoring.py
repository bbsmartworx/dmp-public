from .Shared import PayloadBase

##############################################################################################################
# MODELS #
##############################################################################################################
class FindDevicesModel(PayloadBase):
    def __init__(self, page=None, page_size=None, filters=None, fields=None):
        super().__init__(
            page=page,
            page_size=page_size,
            filters=filters,
            fields=fields
        )

class GetDevicesModel(PayloadBase):
    def __init__(self, company_id=None, mac_address=None, imei=None, serial=None,
                 device_type=None, page=None, page_size=None):
        super().__init__(
            company_id=company_id,
            mac_address=mac_address,
            imei=imei,
            serial=serial,
            device_type=device_type,
            page=page,
            page_size=page_size
        )

class GetDeviceFieldsHistoryModel(PayloadBase):
    def __init__(self, sampling_operator=None, fields=None, start=None, stop=None, sampling_period = 3600):
        super().__init__(
            fields=fields,
            sampling_period=sampling_period,
            sampling_operator=sampling_operator,
            start=start,
            stop=stop,
        )

class GetDistinctValuesModel(PayloadBase):
    def __init__(self, filters=None, fields=None):
        super().__init__(
                filters=filters,
                fields=fields
        )

class FieldDistinctValuesRequestModel(PayloadBase):
    def __init__(self, name=None, ranges=None):
        super().__init__(
            name=name,
            ranges=ranges
        )

class CustomRangeModel(PayloadBase):
    def __init__(self, start=None, end=None, name=None):
        super().__init__(
            start=start,
            end=end,
            name=name
        )

class RequestedFieldModel(PayloadBase):
    def __init__(self, name=None, sort=None):
        super().__init__(
            name = name,
            sort = sort
        )

class SortingModel(PayloadBase):
    def __init__(self, order=None, direction=None):
        super().__init__(
            order = order,
            direction = direction
        )