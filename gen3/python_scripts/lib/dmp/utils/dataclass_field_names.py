from dataclasses import fields
from dmp.utils.wrappers import ModelWrapper


class DataClassFieldNames:
    @property
    def attr_names(self):
        attr_names_model = ModelWrapper()
        [setattr(attr_names_model, field.name, field.name) for field in fields(self)]
        return attr_names_model

    @attr_names.setter
    def attr_names(self, value):
        raise Exception(f"Value {value}, can't be set for read-only property attr_names.")