from requests import Response
from enum import Enum


class Constants(Enum):
    wadmp_client = "wadmp_client"
    version      = "version"
    url          = "url"

class ModelWrapper(dict):
    
    def __init__(self, **kwargs):
        for arg, value in kwargs.items():
            self[arg] = value

    def __getattr__(self, name):
        return self[name]
    
    def __setattr__(self, name, value):
        self[name] = value

def check(response: Response) -> Response:
    if response.status_code != 200:
        raise Exception(f" Response data: \n  \
                          API call returned non OK code: {response.status_code}. \n  \
                          Response reason: {response.reason}. \n  \
                          Response request type: {response.request}. \n  \
                          Response URL: {response.url}. \n  \
                          Response text: {response.text}. \n  \
                          Response Header: {response.request.headers['Authorization'].replace('Bearer ', '')}. \n ")

    return response

def get_data(response: Response):
    return response.json()["data"]

def get_device_type(data):
    return data["DeviceType"]

def get_id(data):
    return data['id']

def get_name(data):
    return data["name"]

def get_is_firmware(data):
    return data["is_firmware"]

def get_version(data):
    return data["version"]

def get_settings(data):
    return data["settings"]

def get_connection_status(data):
    return data["Online"]

def get_sync_status(data):
    return data["SyncStatus"]

def get_apps(data):
    return data["apps"]

def get_url(data):
    return data["url"]

def get_system(data):
    return data["system"]

def get_firmware(data):
    return data["firmware"]