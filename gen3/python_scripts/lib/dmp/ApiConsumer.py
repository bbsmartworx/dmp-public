import requests
import time
import string as str
from dmp.RequestSession import RequestSession
from .api_sections.api_section_imports import *


#################################################
# Class for obtaining authorization token.
class ApiAuth:
    def __init__(self, base_url_api, session):
        self.session = session
        self.base_url_auth = base_url_api

    # Log into the the system and return authorization token.
    def authorize(self, username, password):
        url = f"{self.base_url_auth}/auth/connect/token"
        credentials = {'username': username,
                       'password': password,
                       'client_id': 'python',
                       'grant_type': 'password'}
        response = self.session.post(url, data=credentials)
        # How to get token from the response object:
        #      response.json()["access_token"]
        return response

#################################################
# This class can be used for sending API requests
# and parsing response.
class ApiConsumer:
    def __init__(self, base_url="https://gateway.dev.wadmp.com", ca_path=True):
        self.session            = RequestSession(base_url, ca_path)   # For sending API requests.
        self.base_url_auth      = f"{base_url}/public" # Where authorization and weird stuff lies.
        self.base_url_api       = f"{base_url}/api"    # Where API lies.
        self.auth_token         = None                 # Will be configured when login() method is called.
        # SubAPIs:
        self._auth              = ApiAuth(self.base_url_auth, self.session)
        self.alert              = ApiAlerts(self.base_url_api, self.session)
        self.alert_endpoint     = ApiAlertEndpoints(self.base_url_api, self.session)
        self.alert_history      = ApiAlertHistory(self.base_url_api, self.session)
        self.api_client         = ApiClients(self.base_url_api, self.session)
        self.application        = ApiApplications(self.base_url_api, self.session)
        self.auditing           = ApiAuditing(self.base_url_api, self.session)
        self.billing            = ApiBilling(self.base_url_api, self.session)
        self.bootstrap          = ApiBootstrap(self.base_url_api, self.session)
        self.command            = ApiCommands(self.base_url_api, self.session)
        self.company            = ApiCompanies(self.base_url_api, self.session)
        self.company_statistics = ApiCompanyStatistics(self.base_url_api, self.session)
        self.config_profile     = ApiConfigProfile(self.base_url_api, self.session)
        self.device_identity    = ApiDeviceIdentity(self.base_url_api, self.session)
        self.device_management  = ApiDeviceManagement(self.base_url_api, self.session)
        self.device_monitoring  = ApiDeviceMonitoring(self.base_url_api, self.session)
        self.family             = ApiFamilies(self.base_url_api, self.session)
        self.field              = ApiFields(self.base_url_api, self.session)
        self.field_template     = ApiFieldTemplates(self.base_url_api, self.session)
        self.file               = ApiFiles(self.base_url_api, self.session)
        self.source             = ApiSources(self.base_url_api, self.session)
        self.system             = ApiSystem(self.base_url_api, self.session)
        self.type               = ApiTypes(self.base_url_api, self.session)
        self.user               = ApiUsers(self.base_url_api, self.session)
        self.view               = ApiViews(self.base_url_api, self.session)
        self.long_operation     = ApiLongOperations(self.base_url_api, self.session)

    def set_header(self, header):
        '''This method replaces header with the one given. It is used by tests
           that sign-in with multiple users and then switch between them for
           particular requests by replacing the header.'''
        self.session.clear_headers()
        self.session.set_header(header)

    def login(self, username, password):
        response = self._auth.authorize(username, password)
        if response.status_code != requests.codes['ok']:
            print(response.json())
            if response.status_code == 400: # Invalid username/password
                raise Exception("Invalid username or password.")
            elif response.status_code == 405: # 'Not Allowed'
                # Note: response.json() throws an exception when err code is 405. So we must not use it here.
                raise Exception(f"Invalid URL '{self._auth.base_url_auth}'.")
            else:
                raise Exception(f"Authorization failed: '{str(response.status_code)} -> {str(response.json())}")
        self.auth_token = response.json()["access_token"]
        self.session.headers.update({'Authorization': f'Bearer {self.auth_token}'})
        # The following sleep is a workaround from GEN2-2332. Its
        # purpose is to avoid Unauthorized error when using this
        # script against an installation running on a localhost.
        # There must always be some delay between creation of JWT
        # and usage of that token. We were unable to configure this
        # delay on the server side, so we are doing it here.
        time.sleep(0.5)