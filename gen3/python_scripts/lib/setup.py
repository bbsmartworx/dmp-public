from setuptools import setup, find_packages

setup(
    name='dmp',
    version='3.1.2',
    description='This package is not distributed wia PyPi.',
    url='https://bitbucket.org/bbsmartworx/dmp-public/python_scripts',
    author='Advantech WA/DMP Team',
    author_email='dmp@advantech.com',
    packages=find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
