from requests import Response


def check(response: Response) -> Response:
    if response.status_code != 200:
        raise Exception(f" Response data: \n  \
                          API call returned non OK code: {response.status_code}. \n  \
                          Response reason: {response.reason}. \n  \
                          Response request type: {response.request}. \n  \
                          Response URL: {response.url}. \n  \
                          Response text: {response.text}. \n  \
                          Response Header: {response.request.headers['Authorization'].replace('Bearer ', '')}. \n ")

    return response