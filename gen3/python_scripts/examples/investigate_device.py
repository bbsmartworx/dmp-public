from dmp.DMP_Api     import DMP_Api
from dmp.Methods     import die
import sys

base_url = 'http://gateway.wadmp.com'
username = 'editme'
password = 'editme'

if len(sys.argv) < 2:
    die(f'Missing/Invalid arguments. Correct usage: {sys.argv[0]} <mac>')

mac = sys.argv[1]

api = DMP_Api(base_url)
api.login(username, password)

d = api.get_device_datamodel(mac)

cmds =  api.get_last_x_commands_created(mac, 4)
result_codes_array = []
with open('investigate_device.txt', 'a') as f:
    for cmd in cmds:
        f.write("------------------------------------------------ \n")
        f.write(f'id: {str(cmd["id"])} \n')
        f.write(f'type: {cmd["type"]} \n')
        f.write(f'creation_date: {cmd["creation_date"]} \n')
        f.write(f'date_completed :{cmd["date_completed"]} \n')
        f.write(f'arguments :{cmd["arguments"]} \n')
        f.write(f'result_code: {cmd["result_code"]} \n')
        f.write(f'next_send_date: {cmd["next_send_date"]} \n')
        f.write("\n")

        result_codes_array.append(cmd["result_code"])

    f.write(f'\n\n*** Summary: ***\n')
    f.write(f'Is device synced: {"true" if d.sync_status else "false"} \n')
    f.write(f'Last cmd response codes: {str(result_codes_array)} \n')

    f.write(f'Installed applications: \n')
    f.writelines(map(lambda i: f"{i} \n", d.apps_installed))

    f.write(f'Firmware version: {str(d.firmware_version)} \n')