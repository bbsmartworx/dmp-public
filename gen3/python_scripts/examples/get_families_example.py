from dmp.ApiConsumer import ApiConsumer
from dmp.models.Families import FindFamiliesModel
from utils import check


base_url = 'http://localhost:8082'
username = 'editMe@something.com'
password = 'editMe'


api = ApiConsumer(base_url)
api.login(username, password)

resp = check(api.family.find_families(FindFamiliesModel()))
print(resp.json())