#!/usr/bin/python3
"""
This script uses the public API to create files with config profile of devices.
For filter your devices and create csv file which you can use like input,
you can use get_devices.py

Required arguments:
    -csv_file or -mac, one of that argument must be provided.
    -username
    -password

Examples of usage:
    python3 ./get_config_from_devices.py -csv_file ./example.csv -username test@advantech.com -password 123456
    python3 ./get_config_from_devices.py -mac AA:BB:CC:DD:EE:FF -username test@advantech.com -password 123456

June 2023
"""

from argparse import ArgumentParser
import json
import os
from os.path import splitext, basename
from csv import reader
from logging import getLogger
from dmp.ApiConsumer import ApiConsumer
from utils.logging_conf import set_log_lvl, configure_logging
from utils.services import log_response
from utils.get import  get_data


def parse_args():
    """Parse command-line arguments"""
    parser = ArgumentParser(
        description="Get a list of devices, save in CSV file."
    )


    # Optional arguments:

    parser.add_argument("-dir_name",
                        help="Name of saved folder. \
                                Default = 'get_devices_config'",
                        type=str,
                        default="get_devices_config")

    parser.add_argument("-csv_file",
                        help="Path to CSV file.",
                        type=str)

    parser.add_argument("-mac",
                        help="MAC Address of an individual device",
                        type=str)

    parser.add_argument("-host",
                        help="URL of the API gateway. \
                                Default = 'https://gateway.wadmp.com'",
                        type=str,
                        default="https://gateway.wadmp.com")

    parser.add_argument("-username",
                        help="Username. \
                                Check the code for the default!",
                        type=str,
                        default="email address")

    parser.add_argument("-password",
                        help="Password. \
                                Check the code for the default!",
                        type=str,
                        default="password")

    parser.add_argument("-ca",
                        help="Path to CA certificate.",
                        type=str,
                        default="./ca_cert_file")

    parser.add_argument("-console_log_level",
                        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                                            Default = info",
                        type=str,
                        choices=["debug", "info", "warning", "error", "critical"],
                        default="info")

    parser.add_argument("-file_log_level",
                        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                                            Default = info",
                        type=str,
                        choices=["debug", "info", "warning", "error", "critical"],
                        default="info")

    args = parser.parse_args()

    return args


def main(args): 
    api = ApiConsumer(args.host, args.ca)
    api.login(args.username, args.password)

    console_log_lvl = set_log_lvl(args.console_log_level)
    file_log_lvl    = set_log_lvl(args.file_log_level)

    script_name = splitext(basename(__file__))[0]
    configure_logging(script_name, console_log_lvl, file_log_lvl)        
    logger = getLogger(script_name)

    if args.mac:
        create_file_with_config_by_mac(api, logger, args)
    if args.csv_file:
        create_files_with_config_from_csv(api, logger, args)


def create_directory(args):
    dir_path = f"{os.getcwd()}/{args.dir_name}"
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
    return dir_path

def get_config_from_device(api, logger, mac):
    logger.info(f"Getting the config profile from {mac}")
    response = api.config_profile.get_config_profile_base_by_mac(mac)
    log_response(logger, response, message=f"Failed to get config profile from {mac}.")
    return get_data(response)

def save_to_file(logger, args, config, device_name):
    dir_path = create_directory(args)
    json_object = json.loads(config)
    json_formatted_str = json.dumps(json_object, indent=4)
    with open(f'{dir_path}/{device_name}.txt', 'w') as f:
        f.write(json_formatted_str)

    logger.info(f"Saved to {device_name}.")

def create_files_with_config_from_csv(api, logger, args):
    with open(args.csv_file, encoding="UTF-8", newline="") as csv_file:
        csvreader = reader(csv_file, delimiter=",")
        next(csvreader)  # Skip the first row
        for row in csvreader:
            logger.debug(row)

            alias, serial_number, order_code, mac, imei, requested_type = row
            logger.info(f"Alias {alias}")
            logger.info(f"MAC {mac}")

            config = get_config_from_device(api, logger, mac)
            device_name = mac
            if alias:
                device_name = alias
            save_to_file(logger, args, config, device_name)

def create_file_with_config_by_mac(api, logger, args):
    config = get_config_from_device(api, logger, args.mac)
    save_to_file(logger, args, config, args.mac)

if __name__ == "__main__":
    args = parse_args()
    main(args)
