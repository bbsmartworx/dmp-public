#!/usr/bin/python3
"""
This script uses the public API to create config profile from one device and
its distribution to another device or devices.

Required arguments:
    Company
    -src_mac, if you want to create new config profile (you can also provide -config_name,
 if you want change config profile name, default is "Default_config_name".
 If you have already created config profile, -src_mac leave empty and provide only -config_name).
    -config_name, (without -src_mac) if you already have created config profile.
    -dest_mac or -dest_csv, one of them is required.
    -username
    -password

Examples of usage:
    python3 ./clone_device.py TestCompany_1 -src_mac 00:AA:AA:AA:AA:AA -dest_mac 00:BB:BB:BB:BB:BB -username test@advantech.com -password 123456
    python3 ./clone_device.py TestCompany_1 -dest_csv ./xxx.csv -config_name my_config_profile -username test@advantech.com -password 123456

Example of the .csv file can be found at:
   ./example.csv

June 2023
"""
import time
import argparse
import re

from dmp.ApiConsumer import ApiConsumer
from os.path import splitext, basename
from csv import reader
from logging import getLogger
from dmp.ApiConsumer import ApiConsumer
from dmp.models.DeviceManagement import UpdateDeviceModel, DeviceFieldUpdateModel
from dmp.models.Companies import FindCompanyModel
from dmp.Enums import FieldNames
from utils.get import get_id_by_name, get_ids
from utils.logging_conf import set_log_lvl, configure_logging
from utils.services import log_response


def parse_args():
    parser = argparse.ArgumentParser(description="Clones a device on WADMP.")

    # Positional arguments:
    parser.add_argument("Company",
                        help="Company name.",
                        type=str)

    parser.add_argument("-src_mac",
                        help="MAC of the source device.",
                        type=str)

    parser.add_argument("-dest_mac",
                        help="MAC of the destination device.",
                        type=str)

    parser.add_argument("-dest_csv",
                        help="CSV file containing MAC addresses of destination devices.",
                        type=str)

    parser.add_argument("-config_name",
                        help="Name of config profile, which should be applied.",
                        type=str,
                        default="Default_config_name")

    parser.add_argument("-host",
                        help="Default: 'https://gateway.wadmp.com'",
                        type=str,
                        default="https://gateway.wadmp.com")

    parser.add_argument("-username",
                        help="Username.",
                        type=str,
                        required=True)

    parser.add_argument("-password",
                        help="Password.",
                        type=str,
                        required=True)

    parser.add_argument(
        "-ca",
        help="Path to CA certificate.",
        type=str,
        default="./ca_cert_file",
    )

    parser.add_argument("-console_log_level",
                        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                                            Default = info",
                        type=str,
                        choices=["debug", "info", "warning", "error", "critical"],
                        default="info",)

    parser.add_argument("-file_log_level",
                        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                                            Default = info",
                        type=str,
                        choices=["debug", "info", "warning", "error", "critical"],
                        default="info",)

    args = parser.parse_args()
    return fix_args(args)


def main(args):
    validate_args(args)
    api = ApiConsumer(args.host, args.ca)
    api.login(args.username, args.password)

    console_log_lvl    = set_log_lvl(args.console_log_level)
    file_log_lvl       = set_log_lvl(args.file_log_level)
    script_name        = splitext(basename(__file__))[0]
    configure_logging(script_name, console_log_lvl, file_log_lvl)

    logger = getLogger(script_name)

    if args.src_mac:
        create_config_from_device(api, logger, args)

    src_device_cofig_id = get_config_id(api, logger, args)
    update_field_model = get_update_device_model(src_device_cofig_id)

    if args.dest_mac:
        apply_config_on_device(api, logger, args, update_field_model)

    if args.dest_csv:
        apply_config_on_csv(api, logger, args, update_field_model)

#########################################################
def wanted_company_id(api, logger, args):
    company_id = None
    response = api.company.find_companies(FindCompanyModel())
    log_response(logger, response, message="Failed to retrieve the list of Companies!")
    company_id = get_id_by_name(response, args.Company)

    if company_id:
        logger.info(f"Company {args.Company} has ID {company_id}")
    else:
        logger.error(f"Company {args.Company} not found!")
        exit()
    return company_id

#########################################################
def create_config_from_device(api, logger, args):
    response  = api.command.create_snapshot(args.src_mac, args.config_name)
    log_response(logger, response, message=f"Command to create config profile from device: {args.src_mac} failed!")
    if response.status_code == 202:
        # needs time to process the command
        time.sleep(35)

#########################################################
def get_config_id(api, logger, args):
    company_id = wanted_company_id(api, logger, args)
    response = api.config_profile.find_config_profile_bases(company_id, args.config_name)
    log_response(logger, response, message=f"Failed to get config profile with name: {args.config_name}!")
    config_id = get_ids(response)
    return config_id

#########################################################
def get_update_device_model(src_device_cofig_id):
    update_field_model = UpdateDeviceModel([DeviceFieldUpdateModel(field_name = FieldNames.ConfigProfileBaseId.value,
                                                                  value      = src_device_cofig_id)])
    return update_field_model

#########################################################
def apply_config_on_device(api, logger, args, update_field_model):
        logger.debug(args.dest_mac)
        response = api.device_management.update_device_fields_by_mac(args.dest_mac, update_field_model)
        log_response(logger, response, message=f"Failed to update config profile on device: {args.dest_mac} !")

#########################################################
def apply_config_on_csv(api, logger, args, update_field_model):
    with open(args.dest_csv, encoding="UTF-8", newline="") as csvfile:
        csvreader = reader(csvfile, delimiter=",")
        next(csvreader)  # Skip the first row
        for row in csvreader:
            logger.debug(row)

            alias, serial_number, order_code, mac, imei, requested_type = row
            logger.info(f"Alias {alias}")
            logger.info(f"Serial Number {serial_number}")
            logger.info(f"Order Code {order_code}")
            logger.info(f"MAC {mac}")
            logger.info(f"IMEI {imei}")
            logger.info(f"Type {requested_type}\n")

            response = api.device_management.update_device_fields_by_mac(mac, update_field_model)
            log_response(logger, response, message=f"Failed to update config profile on device: {mac} !")

#########################################################
def is_mac_address(mac):
    return mac and re.match("[0-9a-f]{2}([-:]?)[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", mac.lower())

#########################################################
def validate_args(args):
    if args.src_mac and not is_mac_address(args.src_mac):
        raise RuntimeError(f"Invalid MAC address: '{args.src_mac}'")
    if args.dest_mac and not is_mac_address(args.dest_mac):
        raise RuntimeError(f"Invalid MAC address: '{args.dest_mac}'")
    if not args.username:
        raise RuntimeError(f"Missing '--username <user>' mandatory parameter.")
    if not args.password:
        raise RuntimeError(f"Missing '--password <pass>' mandatory parameter.")
    if not args.dest_mac and not args.dest_csv:
        raise RuntimeError(f"Missing destination parameter (use '--dest-csv <file>' or '--dest-mac <mac>'")
    if not args.src_mac and not args.config_name:
        raise RuntimeError(f"""Missing config profile name or mac address that config profile should be created from.
use '--src_mac <mac address>' or '--config_name <name of config profile>'.""")

#########################################################
def fix_args(args):
    if not args.host.startswith("http"):
        args.host = f"https://{args.host}"
    return args


#########################################################
#########################################################
if __name__ == "__main__":
    args = parse_args()
    main(args)

