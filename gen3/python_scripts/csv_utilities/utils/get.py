from requests import Response


def check(response: Response) -> Response:
    if response.status_code != 200:
        raise Exception(f"API call returned non OK code:{response.status_code}.")

    return response

def get_id_by_name_generally(data, name, key_name="name"):
    result = list(filter(lambda x: x[key_name] == name, data))
    if not result:
        raise Exception(f"No match with name \"{name}\" was found.")
    return result[0]["id"]    

def get_id_by_name(function_call, name, key_name="name"):
    """ pram function_call: some api endpoint call. 
        param name: wanted name.
        param key_name: wanted key word in payload dict, default is name, but can be file_name and so on.
    """
    data = get_data(function_call)
    if len(data) == 0 or data == None:
        raise Exception(f"No data was found.")
    id = get_id_by_name_generally(data, name, key_name)
    return id

def search(array, value, key="name"):
    """Generic function to find a particular dictionary in a list of dictionaries,
    based on one key:value pair in each dict.
    """
    for item in array:
        if item[key] == value:
            return item
    return None    

def get_data(response: Response):
    return check(response).json()["data"]

def get_total_items(response: Response):
    return check(response).json()["total_items"] 

def get_sections(response: Response):
    return response.json()["data"]["sections"] 

def get_ids(response: Response, index=0):
    return check(response).json()["data"][index]["id"]

def get_state_of_app_on_device(data):
    return data["state"] 

def get_id(data):
    return data["id"]  

# def get_type_id(data):
#     return data["type_id"]

def has_failed(data):
    return data['has_failed']

def get_app_name_from_app_version(data):
    return data['application_version']['application']['name']

def get_name(data):
    return data['name']

def get_version_from_app_version(data):
    return data['application_version']['version']

def is_firmware_from_app_version(data):
    return data["application_version"]["application"]["is_firmware"]

def is_firmware(data):
    return data["is_firmware"]    

def is_pinned(data):
    return data["is_pinned"]

def is_unknown_from_app_version(data):
    return data["application_version"]["is_unknown"]

def is_unknown(data):
    return data["is_unknown"] 

def get_alias(data):
    return data['Name']

def get_serial_number(data):
    return data['Serial']

def get_order_code(data):
    return data['order_code']

def get_mac_address(data):
    return data['MacAddress']

def get_imei(data):
    return data['Imei']

def get_device_type_description(data):
    return data["device_type"]["description"]    

def get_device_type_name(data):
    return data["DeviceType"]

def get_app_id_from_version(data):
    return data["application_version"]["application"]["id"] 

def get_app_version_id(data):
    return data["application_version"]["id"] 

def get_name(data):
    return data["name"] 

def get_desired_conf(data):
    return data["desired_configuration"]

def get_reported_conf(data):
    return data["reported_configuration"] 

def get_in_sync(data):
    return data["in_sync"]

def get_versions(data):
    return data["versions"]

def get_version(data):
    return data["version"] 