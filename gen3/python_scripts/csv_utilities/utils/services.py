import json
from csv import reader
from requests import Response
from utils.get import get_data, is_firmware_from_app_version, get_app_id_from_version, get_app_version_id


def log_response(logger, response: Response, *, message="Failed to retrieve data!"):
    logger.debug(f"Sending request.")
    logger.debug(response.status_code)
    try:
        logger.debug(json.dumps(response.json(), indent=4, sort_keys=True))
    except ValueError:
        logger.debug(response.text)
    if response.status_code == 200:
        try:
            response.json()
        except json.decoder.JSONDecodeError as err:
            logger.error(f"Problem decoding JSON!\n{err}")
        except KeyError as err:
            logger.error(f"Didn't find what we expected in the JSON response!\n{err}")
    elif response.status_code == 202:
        try:
            response.json()
        except json.decoder.JSONDecodeError as err:
            logger.error(f"Problem decoding JSON!\n{err}")
        except KeyError as err:
            logger.error(f"Didn't find what we expected in the JSON response!\n{err}")
    else:
        logger.error(
            f"{message} Response code: {response.status_code}"
        )
        try:
            logger.error(f"{response.json()['message']}")
        except json.decoder.JSONDecodeError as err:
            logger.error(f"Problem decoding JSON!\n{err}")
        except KeyError as err:
            logger.error(response.json())

