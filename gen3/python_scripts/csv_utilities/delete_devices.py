#!/usr/bin/python3
"""
This script uses the public API to delete a number of devices.

Note that you can only delete a device if:
* The device is not claimed by a company.
  Use the "release_devices.py" script.
* The device has never connected to (aka "registered" with) the Management Server.
  A delete request will be rejected with the message "You cannot delete a device that has registered to a server."
  The only option in this case is to ask a System Adinistrator to delete the device manually in the database.

Required arguments:
    CSVfile
    -username
    -password

Examples of usage:
    python3 ./delete_devices.py ./example.csv -username test@advantech.com -password 123456

Example of the .csv file can be found at:
   ./example.csv

June 2023
"""

from argparse import ArgumentParser
from os.path import splitext, basename
from csv import reader
from logging import getLogger
from dmp.ApiConsumer import ApiConsumer
from utils.logging_conf import set_log_lvl, configure_logging
from utils.services import log_response

def parse_args():
    """Parse command-line arguments
    """
    parser = ArgumentParser(description="Delete devices from WA/DMP")

    # Positional arguments:

    parser.add_argument("CSVfile", help="Path to CSV file.", type=str)

    # Optional arguments:

    parser.add_argument(
        "-host",
        help="URL of the WADMP server's API gateway. \
                                Check the code for the default!",
        type=str,
        default="https://gateway.wadmp.com",
    )

    parser.add_argument(
        "-username",
        help="Username. \
                                Check the code for the default!",
        type=str,
        default="email",
    )

    parser.add_argument(
        "-password",
        help="Password. \
                                Check the code for the default!",
        type=str,
        default="password",
    )

    parser.add_argument(
        "-ca",
        help="Path to CA certificate.",
        type=str,
        default="./ca_cert_file",
    )

    parser.add_argument(
        "-console_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    parser.add_argument(
        "-file_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    return parser.parse_args()


def main(args):
    api = ApiConsumer(args.host, args.ca)
    api.login(args.username, args.password)
    
    console_log_lvl    = set_log_lvl(args.console_log_level)
    file_log_lvl       = set_log_lvl(args.file_log_level)
    script_name        = splitext(basename(__file__))[0]
    configure_logging(script_name, console_log_lvl, file_log_lvl)
    
    logger = getLogger(script_name)
    delete_wanted_device(api, logger)

def delete_wanted_device(api, logger):
    with open(args.CSVfile, encoding="UTF-8", newline="") as csvfile:
        csvreader = reader(csvfile, delimiter=",")
        next(csvreader)  # Skip the first row
        for row in csvreader:
            logger.debug(row)

            alias, serial_number, order_code, mac, imei, requested_type = row
            logger.info(f"Alias {alias}")
            logger.info(f"Serial Number {serial_number}")
            logger.info(f"Order Code {order_code}")
            logger.info(f"MAC {mac}")
            logger.info(f"IMEI {imei}")
            logger.info(f"Type {requested_type}\n")

            response = api.device_identity.delete_by_mac(mac)
            log_response(logger, response, message=f"Failed to delete device {mac} !") 

if __name__ == "__main__":
    args = parse_args()
    main(args)
