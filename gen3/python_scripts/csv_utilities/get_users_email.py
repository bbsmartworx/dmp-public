#!/usr/bin/python3
"""
This script uses the public API to acquire emails of all users the user has access to.

Required arguments:
    --output
    --email
    --password

Examples of usage:
    python3 ./get_users_email.py -o ./mails.csv -e test@advantech.com -p 123456
January 2024
"""

import argparse
import csv
from dmp.ApiConsumer import ApiConsumer
from dmp.models.Users import FindUsersModel
from utils.get import get_data

def parse_args():
    parser = argparse.ArgumentParser(description= "Script for obtainin user's email addresses in CSV file.")

    parser.add_argument("-o", "--output",
                        help="Path to CSV file.",
                        type=str,
                        required=True)

    parser.add_argument("-e", "--email",
                        help="WADMP user's email.",
                        type=str,
                        required=True)

    parser.add_argument("-p", "--password",
                        help="WADMP user's password.",
                        type=str,
                        required=True)

    parser.add_argument("-ca",
                        help="Path to CA certificate.",
                        type=str,
                        default="./ca_cert_file")

    parser.add_argument("-host",
                        help="URL of the API gateway.",
                        type=str,
                        default = 'https://gateway.wadmp.com')

    args = parser.parse_args()

    return args
    
def get_users_email(api: ApiConsumer) -> list[str]:
    model = FindUsersModel()
    model.page = 1
    model.page_size = 100
    
    emails = []
    total_items = None
    
    while total_items == None or len(emails) < total_items:
        response = api.user.find_users(model)
        data = get_data(response)
        
        total_items = (response.json())['total_items']
        
        for item in data:
            email = item['email']
            emails.append(email)
            
        model.page = model.page + 1
        
    return emails
    
def save_emails_to_csv(emails: list[str], csvFilePath: str) -> None:
    with open(csvFilePath, 'w', encoding='UTF8', newline='') as csvfile:
        writer = csv.writer(csvfile)
        
        for email in emails:
            writer.writerow([email])

def main(args):
    api = ApiConsumer(args.host, args.ca)
    api.login(args.email, args.password)
    
    emails = get_users_email(api)
    save_emails_to_csv(emails, args.output)
	
if __name__ == "__main__":
    args = parse_args()
    main(args)