## CSV Utilities

This directory contains a set of Python scripts that are designed to read a CSV file.

* get_devices.py
* create_devices.py
* claim_devices.py
* release_devices.py
* delete_devices.py
* get_config_from_devices.py
* clone_devices.py

# General Info.

The following information is relevant to all scripts in this folder.

Later in this document, we provide specific information on each individual script.

## Creating a virtual environment

We recommend that you create a new virtual environment for these scripts.

Example using virtualenv:

| Linux                                | Windows                               |
| ------------------------------------ | ------------------------------------- |
| `$ virtualenv --python=python3 env3` | `> virtualenv --python=python3 env3`  |
| `$ source env3/bin/activate`         | `> .\env3\Scripts\activate`           |
| `$ pip3 install -r requirements.txt` | `> pip3 install -r requirements.txt`  |

> You only have to do this once on any particular machine.  
> For subsequent runs, just run the activate command.

## Usage

Make sure you *activate* the virtualenv before trying to execute any script.

For detailed usage information, run any script with `-h` as an argument.

The scripts take two types of command-line argument:
1. Positional arguments are not optional. If you do not specify them, you will get an error.
2. Optional arguments. These are prefixed with a hyphen "-". If you do not specify an optional argument, it assumes a default value.

You can change the default values for optional arguments by editing the Python file, or specify new values on the command-line.
For example:

```python .\get_devices.py Test_company_1 -username test@advantech.com -password 123456```

(This example shows backslashes because it was run on Windows)

## Changing log verbosity

There are 2 different `loglevel` arguments for each script:
- `console_log_level` determines what messages are printed to the console;
- `file_log_level` determines what messages are printed to a file named `<script name>.log`.

Python's logging module has a set of default loglevels. The higher the level, the fewer messages that will be logged:
```
 CRITICAL
 ERROR
 WARNING
 INFO
 DEBUG
```

Both loglevel arguments default to `info`. If you want more detail, you may change one or both to `debug`.

> NOTE: If you have a lot of devices, this will create a LOT of verbose output!

## Usage
> python .\get_devices.py -h
usage: get_devices.py [-h] [-csv_file CSV_FILE] [-mac MAC]
                      [-serial_nbr SERIAL_NBR]
                      [-alias ALIAS] [-tags [TAGS ...]]
                      [-type TYPE]
                      [-is_online {True,False}]
                      [-in_sync {True,False,Pending}]
                      [-host HOST] [-username USERNAME] [-password PASSWORD]
                      [-console_log_level {debug,info,warning,error,critical}]
                      [-file_log_level {debug,info,warning,error,critical}]

Get a list of devices, save in CSV file.

optional arguments:
  -h, --help                        show this help message and exit
  -csv_file csv_file                Name of saved file. Default = 'get_devices.csv'
  -mac MAC                          MAC Address of an individual device
  -serial_nbr SERIAL_NBR            Serial Number of an individual device
  -alias ALIAS                      Alias (name) of an individual device
  -tags [TAGS ...]                  Names of Tags and its values which have been applied to devices, separated by ' ' (empty space). Example: Tag_name_1-True Tag_name_2-False
  -type TYPE                        Device Type of an individual device
  -is_online {True,False}           isOnline?
  -types [TYPES [TYPES ...]]        List of Device Types
  -is_online {True, False, None}    is_online?
  -in_sync {True, False, None}      in_sync?
  -host HOST                        URL of the API gateway. Default = 'https://gateway.wadmp.com'
  -username USERNAME                Username. Check the code for the default!
  -password PASSWORD                Password. Check the code for the default!
  -console_log_level {debug,info,warning,error,critical}
                                    Log verbosity level. The higher the level, the fewer
                                    messages that will be logged. Default = info
  -file_log_level {debug,info,warning,error,critical}
                                    Log verbosity level. The higher the level, the fewer
                                    messages that will be logged. Default = info

## Output

This script writes to the console and to the file `get_devices.log`.

This script will ALSO create a CSV file called "get_devices.csv", or some other name if you specify `-csv_file` on the command line.
```


```
> python .\create_devices.py -h
usage: create_devices.py [-h] [-host HOST] [-username USERNAME]
                          [-password PASSWORD]
                          [-console_log_level {debug,info,warning,error,critical}]
                          [-file_log_level {debug,info,warning,error,critical}]
                          CSVfile

Create devices on WA/DMP

positional arguments:
  CSVfile               Path to CSV file.

optional arguments:
  -h, --help            show this help message and exit
  -host HOST            URL of the WADMP server's API gateway. Check the code
                        for the default!
  -username USERNAME    Username. Check the code for the default!
  -password PASSWORD    Password. Check the code for the default!
  -console_log_level {debug,info,warning,error,critical}
                        Log verbosity level. The higher the level, the fewer
                        messages that will be logged. Default = info
  -file_log_level {debug,info,warning,error,critical}
                        Log verbosity level. The higher the level, the fewer
                        messages that will be logged. Default = info
```

```
> python .\claim_devices.py -h
usage: claim_devices.py [-h] [-host HOST] [-username USERNAME]
                         [-password PASSWORD]
                         [-console_log_level {debug,info,warning,error,critical}]
                         [-file_log_level {debug,info,warning,error,critical}]
                         CSVfile Company

Claim devices to a company on WA/DMP

positional arguments:
  CSVfile               Path to CSV file.
  Company               Company name. Check the code for the default!

optional arguments:
  -h, --help            show this help message and exit
  -host HOST            URL of the API gateway. Default =
                        'https://gateway.wadmp.com'
  -username USERNAME    Username. Check the code for the default!
  -password PASSWORD    Password. Check the code for the default!
  -console_log_level {debug,info,warning,error,critical}
                        Log verbosity level. The higher the level, the fewer
                        messages that will be logged. Default = info
  -file_log_level {debug,info,warning,error,critical}
                        Log verbosity level. The higher the level, the fewer
                        messages that will be logged. Default = info
```

```
> python .\release_devices.py -h
usage: release_devices.py [-h] [-host HOST] [-username USERNAME]
                           [-password PASSWORD]
                           [-console_log_level {debug,info,warning,error,critical}]
                           [-file_log_level {debug,info,warning,error,critical}]
                           CSVfile

Release devices from a company on WA/DMP

positional arguments:
  CSVfile               Path to CSV file.

optional arguments:
  -h, --help            show this help message and exit
  -host HOST            URL of the API gateway. Default =
                        'https://gateway.wadmp.com'
  -username USERNAME    Username. Check the code for the default!
  -password PASSWORD    Password. Check the code for the default!
  -console_log_level {debug,info,warning,error,critical}
                        Log verbosity level. The higher the level, the fewer
                        messages that will be logged. Default = info
  -file_log_level {debug,info,warning,error,critical}
                        Log verbosity level. The higher the level, the fewer
                        messages that will be logged. Default = info
```

```
> python .\delete_devices.py -h
usage: delete_devices.py [-h] [-host HOST] [-username USERNAME]
                          [-password PASSWORD]
                          [-console_log_level {debug,info,warning,error,critical}]
                          [-file_log_level {debug,info,warning,error,critical}]
                          CSVfile

Delete devices from WA/DMP

positional arguments:
  CSVfile               Path to CSV file.

optional arguments:
  -h, --help            show this help message and exit
  -host HOST            URL of the WADMP server's API gateway. Check the code
                        for the default!
  -username USERNAME    Username. Check the code for the default!
  -password PASSWORD    Password. Check the code for the default!
  -console_log_level {debug,info,warning,error,critical}
                        Log verbosity level. The higher the level, the fewer
                        messages that will be logged. Default = info
  -file_log_level {debug,info,warning,error,critical}
                        Log verbosity level. The higher the level, the fewer
                        messages that will be logged. Default = info
```

# get_config_from_devices.py

This script will read the config profiles from devices and save them to txt file.

## Usage

```
> python .\get_config_from_devices.py -h
usage: get_config_from_devices.py [-h]
                       [-dir_name DIR_NAME]
                       [-csv_file CSV_FILE]
                       [-mac MAC]
                       [-host HOST]
                       [-username USERNAME]
                       [-password PASSWORD]
                       [-console_log_level {debug,info,warning,error,critical}]
                       [-file_log_level {debug,info,warning,error,critical}]
                       devices section

positional arguments:
  -csv_file CSV_FILE    Path to CSV file.
  or
  -mac MAC              MAC Address of an individual device

optional arguments:
  -h, --help            show this help message and exit
  -dir_name DIR_NAME    Name of saved folder. Default = 'get_devices_config'
  -host HOST            URL of the API gateway. Default =
                        'https://gateway.wadmp.com'
  -username USERNAME    Username. Check the code for the default!
  -password PASSWORD    Password. Check the code for the default!
  -console_log_level {debug,info,warning,error,critical}
                        Log verbosity level. The higher the level, the fewer
                        messages that will be logged. Default = info
  -file_log_level {debug,info,warning,error,critical}
                        Log verbosity level. The higher the level, the fewer
                        messages that will be logged. Default = info
```

## Output
This script will create a directory where the config profile files will be stored. Each file will be named as Alias of the device if is provided in csv or as mac address.


> python .\clone_device.py -h
usage: change_settings_on_devices.py [-h] [-src_mac SRC_MAC]
                          [-dest_mac DEST_MAC]
                          [-dest_csv DEST_CSV]
                          [-config_name CONFIG_NAME]
                          [-host HOST]
                          [-username USERNAME]
                          [-password PASSWORD]
                          [-console_log_level {debug,info,warning,error,critical}]
                          [-file_log_level {debug,info,warning,error,critical}]
                          devices section INIfile
                          Company

Copy config file to all devices.

positional arguments:
  Company               Company name.

optional arguments:
  -h, --help                show this help message and exit
  -src_mac SRC_MAC          MAC of the source device.
  -dest_mac DEST_MAC        MAC of the destination device.
  -config_name CONFIG_NAME  Name of config profile, which should be applied.
  -host HOST                URL of the API gateway. Default = 'https://gateway.wadmp.com'
  -username USERNAME        Username. Check the code for the default!
  -password PASSWORD        Password. Check the code for the default!
  -console_log_level {debug,info,warning,error,critical}
                            Log verbosity level. The higher the level, the fewer
                            messages that will be logged. Default = info
  -file_log_level {debug,info,warning,error,critical}
                            Log verbosity level. The higher the level, the fewer
                            messages that will be logged. Default = info
```

## Output

This script writes to the console and to the file `clone_device.log`.


### csv_file

The `example.csv` file is provided to illustrate the expected schema.
- The first row is ignored.
- Edit the rest of the file with your own device information.


#### Alias

Alias is an optional string that will be displayed in the WA/DMP User Interface.

It allows the user to identify a device using a more human-friendly name.

You should only specify an alias when *claiming* a device, not when *creating* it!
An alias only has meaning within the context of the company that claims it.

#### Serial No.

This is printed on the physical label of the device.

#### Order Code

Order Code is an optional string.

#### MAC

This is the MAC address of the *primary ethernet* interface on the device.
It is also printed on the physical label of the device.

MAC is mandatory when creating, claiming, releasing, or deleting a device.

#### IMEI

IMEI stands for "International Mobile Equipment Identity". It is the unique number of the cellular module in the device, and is available on the physical label of the device.

When creating or claiming a device, IMEI is mandatory.

#### Type

In order to avoid any ambiguity with various names that are used in sales and marketing (e.g. product type, product name, order code, etc.), the string in the Type column should be the name of the *firmware* used by the device.

(ICR-1601 devices are an exception, as their firmware cannot be changed via WA/DMP)

| Product Platform | Product Family | Product Name | WA/DMP Device Type |
| ---------------- | -------------- | ------------ | ------------------ |
| lite             | ICR-1601       | ICR-1601G    | ICR-1601G          |
|                  |                | ICR-1601W    | ICR-1601W          |
| v2               | Conel v2       | LR77 v2      | LR77-v2            |
|                  |                | LR77 v2L     | LR77-v2L           |
|                  |                | UR5i v2      | UR5i-v2            |
|                  |                | UR5i v2L     | UR5i-v2L           |
|                  |                | XR5i v2      | XR5i-v2            |
|                  |                | XR5i v2E     | XR5i-v2e           |
| v3               | SmartStart     | SL304, SL305, SL306 | SPECTRE-v3L-LTE |
|                  |                | SL302        | SPECTRE-v3L-LTE-US |
|                  | SmartFlex      | SR300        | SPECTRE-v3-ERT     |
|                  |                | SR303, SR304, SR306, SR307, SR308, SR309 | SPECTRE-v3-LTE |
|                  |                | SR305        | SPECTRE-v3-LTE-US  |
|                  | SmartMotion    | ST353, ST355 | SPECTRE-v3T-LTE    |
|                  | ICR-3200       | ICR-3211     | ICR-321x           |
|                  |                | ICR-3231, ICR-3232 | ICR-323x     |
|                  |                | ICR-3241     | ICR-324x           |

### Company

The `claim_devices.py` file takes an argument which is your company name.
If the name contains a space, then you should enclose it in single-quotes. For example:
```
> .\claim_devices.py .\example.csv 'Krejuv Limited' -username bkinsella@advantech-bb.com -password PASSWORD -console_log_level info -file_log_level debug
```