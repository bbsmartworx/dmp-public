#!/usr/bin/python3
"""
This script uses the public API to create a CSV file with a list of your devices.
Arguments to the script can be used to filter the devices.

Required arguments:
    company
    -username
    -password

Examples of usage:
    python3 ./get_devices.py Test_company_1 -username test@advantech.com -password 123456

June 2023
"""

from argparse import ArgumentParser
from os.path import splitext, basename
from csv import writer
from logging import getLogger
from dmp.ApiConsumer import ApiConsumer
from dmp.models.DeviceMonitoring import GetDevicesModel, RequestedFieldModel
from dmp.models.Companies import FindCompanyModel
from dmp.models.Shared import FieldFilterModel, FilteringRuleModel
from dmp.Enums import FilteringOperatorId, FieldNames
from utils.logging_conf import set_log_lvl, configure_logging
from utils.services import log_response
from utils.get import (
    get_id_by_name, 
    get_data,
    get_total_items, 
    get_alias,
    get_serial_number,
    get_mac_address, 
    get_imei, 
    get_device_type_name
)


def parse_args():
    """Parse command-line arguments"""
    parser = ArgumentParser(
        description="Get a list of devices, save in CSV file."
    )

    # Positional arguments:

    parser.add_argument("company",
                        help="Name of Company to which devices belong.",
                        type=str)


    # Optional arguments:

    parser.add_argument(
        "-csv_file",
        help="Name of saved file. \
                Default = 'get_devices.csv'",
        type=str,
        default="get_devices.csv",
    )

    parser.add_argument(
        "-mac",
        help="MAC Address of an individual device",
        type=str,
    )

    parser.add_argument(
        "-serial_nbr",
        help="Serial Number of an individual device",
        type=str,
    )

    parser.add_argument(
        "-alias",
        help="Alias (name) of an individual device",
        type=str,
    )

    parser.add_argument(
        "-tags",
        help="""Names of Tags and its values which have been applied to devices, separated by ' ' (empty space).
                Example: Tag_name_1-True Tag_name_2-False""",
        type=str,
        nargs="*"
    )

    parser.add_argument(
        "-type",
        help="Device Type of an individual device",
        type=str,
    )

    parser.add_argument(
        "-is_online",
        help="isOnline?",
        choices=["True", "False", "never_connected"],
        type=str
    )

    parser.add_argument(
        "-in_sync",
        help="inSync?",
        choices=["True", "False", "Pending"],
        type=str
    )

    parser.add_argument(
        "-host",
        help="URL of the API gateway. \
                Default = 'https://gateway.wadmp.com'",
        type=str,
        default="https://gateway.wadmp.com",
    )

    parser.add_argument(
        "-username",
        help="Username. \
                Check the code for the default!",
        type=str,
        default="email address",
    )

    parser.add_argument(
        "-password",
        help="Password. \
                Check the code for the default!",
        type=str,
        default="password",
    )

    parser.add_argument(
        "-ca",
        help="Path to CA certificate.",
        type=str,
        default="./ca_cert_file",
    )

    parser.add_argument(
        "-console_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    parser.add_argument(
        "-file_log_level",
        help="Log verbosity level. The higher the level, the fewer messages that will be logged. \
                             Default = info",
        type=str,
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
    )

    args = parser.parse_args()

    return args


def main(args): 
    api = ApiConsumer(args.host, args.ca)
    api.login(args.username, args.password)

    console_log_lvl = set_log_lvl(args.console_log_level)
    file_log_lvl    = set_log_lvl(args.file_log_level)

    script_name = splitext(basename(__file__))[0]
    configure_logging(script_name, console_log_lvl, file_log_lvl)        
    logger = getLogger(script_name)

    my_devices = get_my_devices(api, args, logger)
    save_to_file(args, logger, my_devices)


def create_filter_by_mac(args):
    filter = FieldFilterModel(rule=FilteringRuleModel(operator_id=FilteringOperatorId.Equals.value,
                                                      operands=[args.mac]),
                              field_name=FieldNames.MacAddress.value)
    return filter

def create_filter_by_serial_number(args):
    filter = FieldFilterModel(rule=FilteringRuleModel(operator_id=FilteringOperatorId.Equals.value,
                                                      operands=[args.serial_nbr]),
                              field_name=FieldNames.Serial.value)
    return filter

def create_filter_by_alias(args):
    filter = FieldFilterModel(rule=FilteringRuleModel(operator_id=FilteringOperatorId.Equals.value,
                                                      operands=[args.alias]),
                              field_name=FieldNames.Name.value)
    return filter

def create_filters_by_tags(tag_name, tag_value):
    if tag_value == "True":
        operand = 1
    elif tag_value == "False":
        operand = 0
    else:
        operand = tag_value
    filter = FieldFilterModel(rule=FilteringRuleModel(operator_id=FilteringOperatorId.Equals.value,
                                                    operands=[operand]),
                            field_name=tag_name)
    return filter

def create_filter_by_type(args):
    filter = FieldFilterModel(rule=FilteringRuleModel(operator_id=FilteringOperatorId.Equals.value,
                                                      operands=[args.type]),
                              field_name=FieldNames.DeviceType.value)
    return filter

def create_filter_by_is_online(args):
    if args.is_online not in ["True", "False", "never_connected"]:
        raise Exception("Only True, False or never_connected can be parsed as argument to is_online.")
    operand = -1 # never connected
    if args.is_online == "False":
        operand = 0
    if args.is_online == "True":
        operand = 1
    filter = FieldFilterModel(rule=FilteringRuleModel(operator_id=FilteringOperatorId.Equals.value,
                                                      operands=[operand]),
                              field_name=FieldNames.Online.value)
    return filter

def create_filter_by_in_sync(args):
    if args.in_sync == "True":
        operand = 1
    elif args.in_sync == "False":
        operand = 0
    elif args.in_sync == "Pending":
        operand = -1
    else:
        raise Exception("Only True, False or Pending can be parsed as argument to in_sync.")
    filter = FieldFilterModel(rule=FilteringRuleModel(operator_id=FilteringOperatorId.Equals.value,
                                                      operands=[operand]),
                              field_name=FieldNames.SyncStatus.value)
    return filter

def create_get_devices_model(args):
    filters_list = []
    if args.mac:
        filters_list.append(create_filter_by_mac(args))
    if args.serial_nbr:
        filters_list.append(create_filter_by_serial_number(args))
    if args.alias:
        filters_list.append(create_filter_by_alias(args))
    if args.tags:
        for tag in args.tags:
            tag_name, tag_value = tag.split('-')
            filters_list.append(create_filters_by_tags(tag_name, tag_value))
    if args.type:
        filters_list.append(create_filter_by_type(args))
    if args.is_online:
        filters_list.append(create_filter_by_is_online(args))
    if args.in_sync:
        filters_list.append(create_filter_by_in_sync(args))

    get_devices_model = GetDevicesModel(page      = 1,
                                        page_size = 100,
                                        filters   = filters_list,
                                        fields    = [RequestedFieldModel(name=FieldNames.Name.value),
                                                     RequestedFieldModel(name=FieldNames.Serial.value),
                                                     RequestedFieldModel(name=FieldNames.MacAddress.value),
                                                     RequestedFieldModel(name=FieldNames.Imei.value),
                                                     RequestedFieldModel(name=FieldNames.DeviceType.value)])
    return get_devices_model

def get_company_id(api, args, logger):
    response = api.company.find_companies(FindCompanyModel(name=args.company))
    log_response(logger, response, message="Failed to get companies details.")
    company_id = get_id_by_name(response, args.company)
    return company_id

def get_my_devices(api, args, logger):
    company_id = get_company_id(api, args, logger)
    get_devices_model = create_get_devices_model(args)
    logger.info(f"Getting the list of matching devices ...")
    response = api.device_monitoring.get_claimed_devices(company_id, get_devices_model)
    log_response(logger, response, message="Failed to get list of devices.")
    logger.info(f"Found {get_total_items(response)} devices in total.\n")
    return get_data(response)

def save_to_file(args, logger, my_devices):
    with open(args.csv_file, "w", encoding="UTF-8", newline="") as csvfile:
            csvwriter = writer(csvfile, delimiter=",")
            csvwriter.writerow(["Alias", "Serial No", "Order Code", "MAC", "IMEI", "Type"])
            for device in my_devices:
                csvwriter.writerow(
                    [
                        get_alias(device),
                        get_serial_number(device),
                        '',
                        get_mac_address(device),
                        get_imei(device),
                        get_device_type_name(device)
                    ]
                )

    logger.info(f"Saved to {args.csv_file}.") 

if __name__ == "__main__":
    args = parse_args()
    main(args)
